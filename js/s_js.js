var	sTypeUrl;
var sIDsource;
var csv;
var pict;
var ref;
var results = [];
var sQ;
// var line =[];


	var url = window.location.href;
	var nchar = url.length - 5;
	var sType = url.substr(nchar,2);
	nchar = url.length - 7;
	sIDsource = url.substr(nchar,7);
	
	
function CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    if (!re_valid.test(text)) return null;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

function  test_type() {
	


	
	switch (sType) {
	case "IS":
		sTypeUrl = "ISS"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "_SELECTEDTAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Individual Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/individual-seismogenic-sources" title="What is an Individual Seismogenic Source?" target="_new"><img src="img/Help2.png"/></a>';
			
		break;
		
	case "CS":
		sTypeUrl = "CSSTOP"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "_SELECTEDTAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Composite Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/composite-seismogenic-sources" title="What is a Composite Seismogenic Source?" target="_new"><img src="img/Help2.png"/></a>';
					
		break;
		
	case "DS":
		sTypeUrl = "DSS"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "_SELECTEDTAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Debated Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/debated-seismogenic-sources" title="What is a Debated Seismogenic Source?" target="_new"><img src="img/Help2.png"/></a>';
		
		break;
		
	case "SD":
		sTypeUrl = "SUBD"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "_SELECTEDTAB";
		
		document.getElementById("TITLE").innerHTML = "Subduction Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/subduction-zones" title="What is a Subduction Source?" target="_new"><img src="img/Help2.png"/></a>';
		
		break;
		
	default: 
		document.getElementById("TAB1").className = "_SELECTEDTAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "DISS Data Tables - Version 3.3.0";
		document.getElementById("INFO_ICON").innerHTML = '';
				
		$(document).ready(function() {
			$("#mainDIV").load("html/home.html");
		});		
		
		break;	
	}
	GeneralInfo();
	
	csv = "data/ContComp4web.txt"
	csvparser(csv);
	commimport();
	
	pict = "data/LinkTable.txt";
	pictparser(pict);
	
	ref = "data/AssignReferences4web.txt";
	refparser(ref);
	
	commimport();
	
	var pos = url.indexOf("/diss3")

	var vers = url.substring(pos+5, pos+8)

	var DISSVER = vers.substring(0,1) + "." + vers.substring(1,2) + "." + vers.substring(2,3)
	
	document.getElementById("VERS").innerHTML = "DISS " + DISSVER;
	
}

function GeneralInfo() {
	  var line =[];
	  var i;
	$.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/html; charset=ISO-8859-1');
		},
	}); 
	$.get('data/'  + sTypeUrl + '4web.MID', function(data){
	
		var Splitdata = data.split('\r\n');
		for (var i = 0; i < Splitdata.length; i++) {

			line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
			// alert(line[1]);
				// line[i] = line[i].replace(/"/g,'');
			var sID = line[i][0];
			sID = line[i][0].replace(/"/g,'');
			// alert(sID);
			if (sID == sIDsource) {
				
			document.getElementById("VALUEELEMENT_IDSource").innerHTML = sIDsource;
			document.getElementById("VALUEELEMENT_SourceName").innerHTML = line[i][1].replace(/"/g,'');	
			document.getElementById("VALUEELEMENT_display").innerHTML = '<a href="dissmap.php?ll=' + sIDsource + '" target="_blank"><img src="img/map_l.gif" alt="map" ></a>'
			
			if (sType == 'IS') {
				ASSISS = "data/Assign_ISS.txt"
				Assign_ISS_parser(ASSISS);	
				paramISS(line, i);
				createAFF(sIDsource,sType);
			}
			
			if (sType == 'CS') {
				ASSCSS = "data/Assign_ISS.txt"
				Assign_CSS_parser(ASSCSS);
				paramCSS(line, i);
				createAFF(sIDsource,sType);
			}
			if (sType == 'DS') {
				// ASSCSS = "data/Assign_ISS.txt"
				// Assign_CSS_parser(ASSCSS);
				paramDSS(line, i);
			}
			if (sType == 'SD') {
				ASSCSS = "data/Assign_ISS.txt"
				Assign_CSS_parser(ASSCSS);
				paramSUBD(line, i);
				createAFF(sIDsource,sType);
			}			
			
			
			}
		}
	});
		$.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/html; UTF-8');
		},
	}); 
};

function csvparser(csv){
	$.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/html; charset=ISO-8859-1');
		},
	}); 
	$.get(csv, function(data){
		
		var line =[];
		var CSVList = data.split(/\r?\n/);
		
		for (var i = 0; i < CSVList.length; i++) {
			line[i] = JSON.parse('[' + CSVList[i] + ']');
			if (line[i][0] == sIDsource) {
				document.getElementById("VALUEELEMENT_compilers").innerHTML = line[i][1];
				document.getElementById("VALUEELEMENT_contributors").innerHTML = line[i][2];
				document.getElementById("VALUEELEMENT_affiliations").innerHTML = line[i][3];
			}
		}
	});
		
}

function commimport(){
	$.get("data/com/" + sIDsource + "COM.txt", function(data){
		//sostituzione dei caratteri per avere il BOLD e il ritorno a capo in HTML
		data = data.replace(/\*\*\r/g, '</b><br>');
		
		data = data.replace(/\*\*/g, '');	

		data = data.replace(/\r/g, '<br>');


			document.getElementById("tab2").innerHTML = data;

	});
		
}

function pictparser(pict){
	$.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/plain; charset=utf-8');
		},
	}); 
	var sPict;
	sPict=" ";
	$.get(pict, function(data){
		var line =[];
		data = data.replace(/\,\,/g,',"",""');	//sostituzione dei caratteri per l'attuale formato del "txt" (campi vuoti) che crea problemi al parser JSON
		 // alert(data);
		var pictList = data.split(/\r?\n/);
		// alert(pictList.length)

	$.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/plain; charset=utf-8');
		},
	}); 	
	
		$.get("data/MainTable.txt", function(data1){
			var line1 =[];
			data1 = data1.replace(/\,\,/g,',"",""');	//sostituzione dei caratteri per l'attuale formato del "txt" (campi vuoti) che crea problemi al parser JSON
			 // alert(data1);
			var didaList = data1.split('\r\n');
			// alert(didaList.length)
			// var m = 1
		
			// alert("ecco")
			var n = 1
			for (var i = 0; i < pictList.length; i++) {
				line[i] = pictList[i].split('\t');
				// alert(line[i][0])
				if (line[i][1] == sIDsource) {
					
					//il primo valore è il nome del file
					var s1Pict = line[i][0]
					
					//chiamo la funzione che legge MainTable.csv
					for (var k = 0; k < didaList.length; k++) {
						line1[k] = didaList[k].split('\t'); //split della "," che non considera quelle tra doppi apici

						if (line1[k][0] == s1Pict) {
							// alert(line1[k][0])				
							//il primo valore è il titolo della picture
							var sTIT = line1[k][1]
						}
					}
					
					//procedura per attribuire alla variabile classe il pari o dispari
					var classRow;
					if (n %2 == 1) {classRow = "EVEN"} else {classRow = "ODD"};
					
					sPict = sPict + '<tr class="' + classRow + '" id="images_emb_ROW_0_1"><td class="' + classRow + '">' + sTIT + '</td><td class="stretto"><a href="img.php?' + s1Pict + '" target="_blank"><img src="img/lens.gif" alt="Details"></a></td></tr>' ;
				n = n+1
				};
			}
			document.getElementById("tab3").innerHTML = '<br><table class="MAINTABLE"><tbody>' + sPict + '</tbody></table>';
		});
	})
}

function Mpictparser(s1Pict){
	// alert(s1Pict);



}

function refparser(ref){
	$.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/html; charset=ISO-8859-1');
		},
	}); 
	var sref;
	sref=" ";
	$.get(ref, function(data){
		var line =[];
		var refList = data.split(/\r?\n/);
		var n = 1
		// alert(sIDsource);
		for (var i = 0; i < refList.length; i++) {
			line[i] = refList[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/);
			var sID = line[i][0].replace(/"/g,'');

			if (sID == sIDsource) {
			// alert(line[i][3] )
				sR = line[i][1] + ' (' + line[i][2] + '), ' + line[i][3] + ' ' + line[i][4]
				sR = sR.replace(/"/g,'');
				//procedura per attribuire alla variabile classe il pari o dispari
				var classRow = "EVEN";
				// if (n %2 == 1) {classRow = "EVEN"} else {classRow = "ODD"};
				
				sref = sref + '<tr class="' + classRow + '" id="images_emb_ROW_0_1"><td class="' + classRow + '">' + sR +'<hr></td></tr>' ;
				
				n = n+1
			};
		}
		
		document.getElementById("tab4").innerHTML = '<br><table class="MAINTABLE" id="tabRef"><tbody><tr><th data-sort-default></th>' + sref + '</tbody></table>';
		new Tablesort(document.getElementById('tabRef'), {
			descending: false
		});
	});

}

function Assign_ISS_parser(ASSISS){
	$.get(ASSISS, function(data){
		var line =[];
		var ASSISSList = data.split(/\r?\n/);
		// alert(ASSISSList);
		for (var i = 0; i < ASSISSList.length; i++) {
			line[i] = JSON.parse('[' + ASSISSList[i] + ']');
			if (line[i][0] == sIDsource) {
				if (line[i][1] == "") {
					document.getElementById("VALUEELEMENT_relatedSourcesList").innerHTML = '<a href="sources.php?' + line[i][2] + '" target="_blank">' +  line[i][2] + '</a>';				
				}
				else 
				{
					document.getElementById("VALUEELEMENT_relatedSourcesList").innerHTML = '<a href="sources.php?' + line[i][1] + '" target="_blank">' +  line[i][1] + '</a>';
				}
			}
		}
	});
}

function Assign_CSS_parser(ASSCSS){
	$.get(ASSCSS, function(data){
		var line =[];
		var ASSCSSList = data.split(/\r?\n/);
		var sASSCSS = "";
		for (var i = 0; i < ASSCSSList.length; i++) {
			line[i] = JSON.parse('[' + ASSCSSList[i] + ']');
			if (line[i][1] == sIDsource) {
				sASSCSS = sASSCSS + ' <a href="sources.php?' + line[i][0] + '" target="_blank">' +  line[i][0] + '</a>'
			}
		}
		document.getElementById("VALUEELEMENT_relatedSourcesList").innerHTML = sASSCSS;
	});
}


function paramISS(line, i) {
			sDate1 = line[i][30].substring(6,8) + "/" + line[i][30].substring(4,6) + "/" + line[i][30].substring(0,4)
			sDate2 = line[i][31].substring(6,8) + "/" + line[i][31].substring(4,6) + "/" + line[i][31].substring(0,4)
			document.getElementById("VALUEELEMENT_created").innerHTML = sDate1;	
			document.getElementById("VALUEELEMENT_latestupdate").innerHTML = sDate2;	
					
			document.getElementById("latlon").innerHTML = ((Number(line[i][22]) + Number(line[i][24]) + Number(line[i][26]) + Number(line[i][28]))/4).toFixed(2) + ' / ' +  ((Number(line[i][23]) + Number(line[i][25]) + Number(line[i][27]) + Number(line[i][29]))/4).toFixed(2);
			iQ = line[i][44];
			qual(iQ);
			document.getElementById("latlonQ").innerHTML = sQ
			document.getElementById("latlonN").innerHTML = line[i][59].replace(/"/g,'');
			
			document.getElementById("length").innerHTML = line[i][2];
			iQ = line[i][33];
			qual(iQ);
			document.getElementById("lengthQ").innerHTML = sQ
			document.getElementById("lengthN").innerHTML = line[i][45].replace(/"/g,'');
			
			document.getElementById("width").innerHTML = line[i][3];
			iQ = line[i][34];
			qual(iQ);
			document.getElementById("widthQ").innerHTML = sQ
			document.getElementById("widthN").innerHTML = line[i][46].replace(/"/g,'');			
			
			document.getElementById("minD").innerHTML = line[i][4];
			iQ = line[i][35];
			qual(iQ);
			document.getElementById("minDQ").innerHTML = sQ
			document.getElementById("minDN").innerHTML = line[i][47].replace(/"/g,'');	
			
			document.getElementById("maxD").innerHTML = line[i][5];
			iQ = line[i][36];
			qual(iQ);
			document.getElementById("maxDQ").innerHTML = sQ
			document.getElementById("maxDN").innerHTML = line[i][48].replace(/"/g,'');				
			document.getElementById("strike").innerHTML = line[i][6];
			iQ = line[i][37];
			qual(iQ);
			document.getElementById("strikeQ").innerHTML = sQ
			document.getElementById("strikeN").innerHTML = line[i][49].replace(/"/g,'');

			document.getElementById("dip").innerHTML = line[i][7];
			iQ = line[i][38];
			qual(iQ);
			document.getElementById("dipQ").innerHTML = sQ
			document.getElementById("dipN").innerHTML = line[i][50].replace(/"/g,'');				

			document.getElementById("rake").innerHTML = line[i][8];
			iQ = line[i][39];
			qual(iQ);
			document.getElementById("rakeQ").innerHTML = sQ
			document.getElementById("rakeN").innerHTML = line[i][51].replace(/"/g,'');				

			document.getElementById("AvgDispl").innerHTML = line[i][9];
			iQ = line[i][40];
			qual(iQ);
			document.getElementById("AvgDisplQ").innerHTML = sQ
			document.getElementById("AvgDisplN").innerHTML = line[i][52].replace(/"/g,'');		

			document.getElementById("sr").innerHTML = line[i][10] + "... " + line[i][11];
			iQ = line[i][41] 
			qual(iQ);
			document.getElementById("srQ").innerHTML = sQ
			document.getElementById("srN").innerHTML = line[i][53].replace(/"/g,'');	

			document.getElementById("RecInt").innerHTML = line[i][12] + "... " + line[i][13] 
			iQ = line[i][42];
			qual(iQ);
			document.getElementById("RecIntQ").innerHTML = sQ
			document.getElementById("RecIntN").innerHTML = line[i][57].replace(/"/g,'');	

			document.getElementById("mag").innerHTML = line[i][17];
			iQ = line[i][43];
			qual(iQ);
			document.getElementById("magQ").innerHTML = sQ
			document.getElementById("magN").innerHTML = line[i][58].replace(/"/g,'');	
	
			document.getElementById("UEQ").innerHTML = line[i][14].replace(/"/g,'');
			document.getElementById("UEQs").innerHTML = line[i][54].replace(/"/g,'');	
			
			document.getElementById("PEQ").innerHTML = line[i][16].replace(/"/g,'');
			document.getElementById("PEQs").innerHTML = line[i][56].replace(/"/g,'');	
			
	
	
};

function paramCSS(line, i) {
			sDate1 = line[i][13].substring(6,8) + "/" + line[i][13].substring(4,6) + "/" + line[i][13].substring(0,4)
			sDate2 = line[i][14].substring(6,8) + "/" + line[i][14].substring(4,6) + "/" + line[i][14].substring(0,4)
			document.getElementById("VALUEELEMENT_created").innerHTML = sDate1;	
			document.getElementById("VALUEELEMENT_latestupdate").innerHTML = sDate2;	
	
			document.getElementById("minD").innerHTML = line[i][2];
			iQ = line[i][16];
			qual(iQ);
			document.getElementById("minDQ").innerHTML = sQ
			document.getElementById("minDN").innerHTML = line[i][23].replace(/"/g,'');	
			
			document.getElementById("maxD").innerHTML = line[i][3];
			iQ = line[i][17];
			qual(iQ);
			document.getElementById("maxDQ").innerHTML = sQ
			document.getElementById("maxDN").innerHTML = line[i][24].replace(/"/g,'');				
			
			document.getElementById("strike").innerHTML = line[i][4] + "..."+line[i][5];
			iQ = line[i][18];
			qual(iQ);
			document.getElementById("strikeQ").innerHTML = sQ
			document.getElementById("strikeN").innerHTML = line[i][25].replace(/"/g,'');				

			document.getElementById("dip").innerHTML = line[i][6] + "..."+line[i][7];
			iQ = line[i][19];
			qual(iQ);
			document.getElementById("dipQ").innerHTML = sQ
			document.getElementById("dipN").innerHTML = line[i][26].replace(/"/g,'');				

			document.getElementById("rake").innerHTML = line[i][8] + "..."+line[i][9];
			iQ = line[i][20];
			qual(iQ);
			document.getElementById("rakeQ").innerHTML = sQ
			document.getElementById("rakeN").innerHTML = line[i][27].replace(/"/g,'');		

			document.getElementById("sr").innerHTML = line[i][10] + "... " + line[i][11];
			iQ = line[i][21] 
			qual(iQ);
			document.getElementById("srQ").innerHTML = sQ
			document.getElementById("srN").innerHTML = line[i][28].replace(/"/g,'');				

			document.getElementById("mag").innerHTML = line[i][12];
			iQ = line[i][22];
			qual(iQ);
			document.getElementById("magQ").innerHTML = sQ
			document.getElementById("magN").innerHTML = line[i][29].replace(/"/g,'');	
	
};

function paramDSS(line, i) {
			sDate1 = line[i][3].substring(6,8) + "/" + line[i][3].substring(4,6) + "/" + line[i][3].substring(0,4)
			sDate2 = line[i][4].substring(6,8) + "/" + line[i][4].substring(4,6) + "/" + line[i][4].substring(0,4)
			document.getElementById("VALUEELEMENT_created").innerHTML = sDate1;	
			document.getElementById("VALUEELEMENT_latestupdate").innerHTML = sDate2;	
			
		$.get('data/DSSA4web.MID', function(data){

			
			var Splitdata = data.split(/\r?\n/);
			
			for (var i = 0; i < Splitdata.length; i++) {

				line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/);
				
				// alert(line[1]);
					// line[i] = line[i].replace(/"/g,'');
				var sID = line[i][0];
				sID = line[i][0].replace(/"/g,'');
				// alert(sID);
				if (sID == sIDsource) {
					for (var k = 1; k < 38; k++) {
						// alert(k);
						sk = k.toString();
						document.getElementById(sk).innerHTML = line[i][k].replace(/"/g,'');	
					}
				}

			}
	});		
};

function paramSUBD(line, i) {
			sDate1 = line[i][10].substring(6,8) + "/" + line[i][10].substring(4,6) + "/" + line[i][10].substring(0,4)
			sDate2 = line[i][11].substring(6,8) + "/" + line[i][11].substring(4,6) + "/" + line[i][11].substring(0,4)
			document.getElementById("VALUEELEMENT_created").innerHTML = sDate1;	
			document.getElementById("VALUEELEMENT_latestupdate").innerHTML = sDate2;	
		
			document.getElementById("minD").innerHTML = line[i][2];
			iQ = line[i][12];
			qual(iQ);
			document.getElementById("minDQ").innerHTML = sQ
			document.getElementById("minDN").innerHTML = line[i][18].replace(/"/g,'');	
			
			document.getElementById("maxD").innerHTML = line[i][3];
			iQ = line[i][13];
			qual(iQ);
			document.getElementById("maxDQ").innerHTML = sQ
			document.getElementById("maxDN").innerHTML = line[i][19].replace(/"/g,'');				

			document.getElementById("dip").innerHTML = line[i][4].replace(/"/g,'');	;
			iQ = line[i][14];
			qual(iQ);
			document.getElementById("dipQ").innerHTML = sQ
			document.getElementById("dipN").innerHTML = line[i][20].replace(/"/g,'');				

			document.getElementById("CA").innerHTML = line[i][5] + "..."+line[i][6];
			iQ = line[i][15];
			qual(iQ);
			document.getElementById("CAQ").innerHTML = sQ
			document.getElementById("CAN").innerHTML = line[i][21].replace(/"/g,'');	

			document.getElementById("CR").innerHTML = Number(line[i][7]).toFixed(1) + "..." + Number(line[i][8]).toFixed(1);
			iQ = line[i][16];
			qual(iQ);
			document.getElementById("CRQ").innerHTML = sQ
			document.getElementById("CRN").innerHTML = line[i][22].replace(/"/g,'');	
			
			document.getElementById("mag").innerHTML = line[i][9];
			iQ = line[i][17];
			qual(iQ);
			document.getElementById("magQ").innerHTML = sQ
			document.getElementById("magN").innerHTML = line[i][23].replace(/"/g,'');	
	
};

function qual(iQ) {
	switch (iQ) {
	  case "1":
	   sQ = 'LD';
		break;
	  case "2": 
	  sQ = 'OD';
		break;
	  case "3":
	  sQ = 'ER';
		break;
	  case "4":
	  sQ = 'AR';
		break;
	  case "5":
	  sQ = 'EJ';
		break;
}

// alert (sQ)	

}

/*
Yetii - Yetii - Yet (E)Another Tab Interface Implementation
http://www.kminek.pl/lab/yetii/
(c) 2007 Grzegorz Wojcik
It is up to You to leave or remove this copyright notice
---
Slightly modified for IE5.x support by Alessandro Fulciniti - http://www.html.it
*/

function Yetii(obj,active){
    
    this.active = (active) ? active : 1,
    this.timeout = null,
    this.tabclass = 'tab',
    this.activeclass = 'active',
    
    this.getTabs = function(){
 
        var retnode = [];
        var elem = document.getElementById(obj).childNodes;     //modified for IE 5.x support
        for (var i = 0; i < elem.length; i++) {
        if (elem[i].className==this.tabclass) retnode[retnode.length]=elem[i];
        }
    
        return retnode;
    
    },
        
    this.links = document.getElementById(obj+'-nav').getElementsByTagName('a'),
    this.tabs = this.getTabs();
    
    this.show = function(number){
        
        for (var i = 0; i < this.tabs.length; i++) {
        this.tabs[i].style.display = ((i+1)==number) ? 'block' : 'none';
        this.links[i].className = ((i+1)==number) ? this.activeclass : '';
        }
    
    },
    
    this.rotate = function(interval){
    
        this.show(this.active);
        this.active++;
    
        if(this.active > this.tabs.length) this.active = 1;
    
        var self = this;
        this.timeout = setTimeout(function(){self.rotate(interval);}, interval*1000);
    
    },
    
    this.init = function(interval){
    
        this.show(this.active);
        
        var self = this; 
        for (var i = 0; i < this.links.length; i++) {
        this.links[i].customindex = i+1;
        this.links[i].onclick = function(){ if (self.timeout) clearTimeout(self.timeout); self.show(this.customindex); return false; };
        } 
        
        if (interval) this.rotate(interval);
            
    };

};


function csvToArray(text) {
    let p = '', row = [''], ret = [row], i = 0, r = 0, s = !0, l;
    for (l of text) {
        if ('"' === l) {
            if (s && l === p) row[i] += l;
            s = !s;
        } else if (',' === l && s) l = row[++i] = '';
        else if ('\n' === l && s) {
            if ('\r' === p) row[i] = row[i].slice(0, -1);
            row = ret[++r] = [l = '']; i = 0;
        } else row[i] += l;
        p = l;
    }
	
    return ret;
};

function createAFF(sIDsource,sType) {
	
		// ACTIVE FAULTS
		$.get('data/AFaults.MID', function(data){
		var line = [];	
		var nRow = 0;
		var Splitdata = data.split('\r\n');
		// alert(Splitdata.length);
		for (var i = 0; i < Splitdata.length-1; i++) {

			line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
			// alert(line[1]);
				// line[i] = line[i].replace(/"/g,'');
			
			switch (sType) {
	  			case "IS":	
					var sID = line[i][0].replace(/"/g,'');
					break;
				case "CS": 
					var sID = line[i][1].replace(/"/g,'');
				  	break;
				case "SD":
					var sID = line[i][2].replace(/"/g,'');
					break;
				}
			// alert(sID);
			if (sID == sIDsource) {
				nRow = nRow + 1;
				var sIDA = line[i][3].replace(/"/g,'');
				var sName = line[i][5].replace(/"/g,'');
				var sRef = line[i][28].replace(/"/g,'');
				var sRow=document.getElementById("AFAcont").innerHTML;
				if(nRow & 1)	{
					document.getElementById("AFAcont").innerHTML = sRow + '<tr class="ODD">	<td class="ODD">'+sIDA +'</td>	<td class="ODD">'+sName+'</td><td class="ODD">'+sRef+'</tr>'
				}
				else	{
					document.getElementById("AFAcont").innerHTML = sRow + '<tr class="EVEN">	<td class="EVEN">'+sIDA +'</td>	<td class="EVEN">'+sName+'</td><td class="EVEN">'+sRef+'</tr>'
				}
				
				
			}
		  }
		  if (nRow == 0) {document.getElementById("AFAcont").innerHTML =""}
	   });	
	   
	   
		// ACTIVE FOLDS
		$.get('data/AFolds.MID', function(data){
		var line = [];	
		var nRow = 0;
		var Splitdata = data.split('\r\n');
		// alert(Splitdata.length);
		for (var i = 0; i < Splitdata.length-1; i++) {

			line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
			// alert(line[1]);
				// line[i] = line[i].replace(/"/g,'');
			
			switch (sType) {
	  			case "IS":	
					var sID = line[i][0].replace(/"/g,'');
					break;
				case "CS": 
					var sID = line[i][1].replace(/"/g,'');
				  	break;
				case "SD":
					var sID = line[i][2].replace(/"/g,'');
					break;
				}
			// alert(sID);
			if (sID == sIDsource) {
				nRow = nRow + 1;
				var sIDA = line[i][3].replace(/"/g,'');
				var sName = line[i][5].replace(/"/g,'');
				var sRef = line[i][28].replace(/"/g,'');
				var sRow=document.getElementById("AFOcont").innerHTML;
				if(nRow & 1)	{
					document.getElementById("AFOcont").innerHTML = sRow + '<tr class="ODD">	<td class="ODD">'+sIDA +'</td>	<td class="ODD">'+sName+'</td><td class="ODD">'+sRef+'</tr>'
				}
				else	{
					document.getElementById("AFOcont").innerHTML = sRow + '<tr class="EVEN">	<td class="EVEN">'+sIDA +'</td>	<td class="EVEN">'+sName+'</td><td class="EVEN">'+sRef+'</tr>'
				}
				
				
			}
		  }
		  if (nRow == 0) {document.getElementById("AFOcont").innerHTML =""}
	   });	
	

// Return array of string values, or NULL if CSV string not well formed.


}