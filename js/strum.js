//==================================================    SISMICITA' STRUMENTALE    ==================================================
var vectorLayer = new ol.layer.Vector({});
var vectorLayerP = new ol.layer.Vector({});
// var infowindow2 = new google.maps.InfoWindow();

function getSTRUMeq(date1, date2, lat1, lat2, lon1, lon2, M1, M2, depth1, depth2){

    // cambia il valore della data minima se è prima del 1985 (settala al 1° gennaio 1985 che è il minimo possibile da dati CNT)
    if (date1.substring(0,4)<1985){
        document.getElementById('StartDateSTRUM').value = '1985-01-01';
    }

    // if (rectangleSTRUM){
        // rectangleSTRUM.setMap(null);
        // document.getElementById('selAreaSTRUM').style.display = "block";
        // document.getElementById('NoselAreaSTRUM').style.display = "none";
    // }


  // read text from URL location
  var request = new XMLHttpRequest();
  request.open('GET', 'https://webservices.ingv.it/fdsnws/event/1/query?starttime=' + date1+ 'T00%3A00%3A00&endtime=' + date2 + 'T23%3A59%3A59&minmag=' + M1 + '&maxmag=' + M2 + '&mindepth=' + depth1 + '&maxdepth=' + depth2 + '&minlat=' + lat1 + '&maxlat=' + lat2 + '&minlon=' + lon1 + '&maxlon=' + lon2 + '&minversion=100&orderby=magnitude-asc&format=text&limit=10000', true);
  request.send(null);
  request.onreadystatechange = function () {
      if (request.readyState === 4 && request.status === 200) {
          var type = request.getResponseHeader('Content-Type');
          if (type.indexOf("text") !== 1) {
				parseQuakeFile(request.responseText)
              return request.responseText;
          }
      } else{
          document.getElementById('numSTRUM').innerHTML = "0 EARTHQUAKES";
           $('#loading').hide()
      }
  }
}


function showIS(fetnam, coordinate, evtPixel){


	// var worldWidth = ol.extent.getWidth(view.getProjection().getExtent());
	// console.log(worldWidth);
	// var world = Math.floor((evtPixel[0] + worldWidth / 2) / worldWidth);
	// console.log(world);

	content = document.getElementById('popup-content');
	if (fetnam.values_.name.slice(0, 2) == 'EQ') {
		descr = fetnam.values_.title;
		var containerPop = document.getElementById('popup');
		containerPop.style.backgroundColor = "#CFE1FE";
		StrumQuakeCode = fetnam.values_.id;
		var url = 'https://e.hsit.it/' + StrumQuakeCode + '/' + StrumQuakeCode + '_mcs.txt';
		$.ajax({
			type: "POST",
			url: 'php/load_data_a.php',
			data: { REQurl: url },
			success: function (html) {
				console.log(html)
				// if (html.substring(0, 3) == 'utc') {
				if (html.substring(0, 2) == 'id') {
					HSITlinkEN = '<br><abbr title=""><a href="https://e.hsit.it/' + StrumQuakeCode + '/index.html " target="_blank"> HSIT page </a></abbr>';
					descr = descr + HSITlinkEN;
					content.innerHTML = descr;
					content.style.display = 'block';
					parent = document.getElementsByClassName('ol-overlay-container ol-selectable')
					parent[0].style.display = 'block';
					// overlay.setPosition([coordinate[0] + world * worldWidth, coordinate[1]]);
					overlay.setPosition(coordinate);
				}else{
					content.innerHTML = descr;
					content.style.display = 'block';
					parent = document.getElementsByClassName('ol-overlay-container ol-selectable')
					parent[0].style.display = 'block';
					// overlay.setPosition([coordinate[0] + world * worldWidth, coordinate[1]]);
					overlay.setPosition(coordinate);
				}
			}
		});

		
	}

}



	


function parseQuakeFile(INGVquakes){
    $('#warningSTRUM').hide()

    ClearMapSTRUM();
	// var vectorSource = new ol.source.Vector();

	var iconFeatures=[];

	var scaleSTRUM5 = 0.06;
	var scaleSTRUM4 = 0.045;
	var scaleSTRUM3 = 1;
	var scaleSTRUM2 = 0.027;
	var scaleSTRUM1 = 0.02;
	var urlIcon1 = "./images/strum_quake_icon_blue1.png"
	var urlIcon2 = "./images/strum_quake_icon_blue2.png"
	var urlIcon3 = "./images/strum_quake_icon_blue3.png"
	var urlIcon4 = "./images/strum_quake_icon_blue4.png"
	var urlIcon5 = "./images/strum_quake_icon_blue5.png"

	var lines=[];
	var Star;
	var EPIpathCALC = 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z';
	var colorSTRUM = '#0c6b2f'


	lines = INGVquakes.split(/\n/)

    if (lines.length>1){
		// alert("OK");
        // toglie la prima riga di intestazione e l'ultima riga vuota
        for (var i=1; i<lines.length-1; i++){
    		var line = lines[i].split('|')
    		var StrumQuakeCode = line[0];
    		var StrumQuakeTime = line[1];
    		var StrumQuakeLat = line[2];
    		var StrumQuakeLon = line[3];
    		var StrumQuakeDepth = line[4];
    		var StrumQuakeCatal = line[5];
    		var StrumQuakeMagType = line[9];
    		var StrumQuakeMagValue = line[10];
    		var StrumQuakeLocation = line[12];

    		if (StrumQuakeMagValue>=6) var markerIcon = new ol.style.Style({
			image: new ol.style.Icon(({src: urlIcon5, anchor: [0.5, 0.5]}))  });
    		if (StrumQuakeMagValue<6 && StrumQuakeMagValue>=5) var markerIcon = new ol.style.Style({
			image: new ol.style.Icon(({src: urlIcon4, anchor: [0.5, 0.5]})) });
    		if (StrumQuakeMagValue<5 && StrumQuakeMagValue>=4) var markerIcon = new ol.style.Style({
			image: new ol.style.Icon(({src: urlIcon3, anchor: [0.5, 0.5]}))  });
    		if (StrumQuakeMagValue<4 && StrumQuakeMagValue>=3) var markerIcon = new ol.style.Style({
			image: new ol.style.Icon(({src: urlIcon2, anchor: [0.5, 0.5]}))  });
    		if (StrumQuakeMagValue<3) var markerIcon = new ol.style.Style({
			image: new ol.style.Icon(({src: urlIcon1, anchor: [0.5, 0.5]}))  });

            // controlla se terremoto ha punti in HSIT

			var time = StrumQuakeTime.split('T')[1]
            var date = StrumQuakeTime.split('T')[0]

    		var titleEN = '<b><font color = "red">Earthquake info</font></a><br><br>' + StrumQuakeMagType + ': ' + StrumQuakeMagValue + ' - ' + StrumQuakeLocation +  '</b><br>Time (UTC): <b>'  + date + '</b>  ' + time.substring(0,8) + '<br>Lat: ' + StrumQuakeLat + ', Lon: ' + StrumQuakeLon + ', Depth: ' + StrumQuakeDepth + ' km'

			var OnClickTextEN = titleEN + '<br>' + '<br><a href="https://terremoti.ingv.it/event/' + StrumQuakeCode + '" target="_blank"> INGV-ONT page </a><br>'		
			
			
    		var STRUMMarker = new ol.Feature({
				geometry: new ol.geom.Point(ol.proj.transform([Number(StrumQuakeLon),Number(StrumQuakeLat)], 'EPSG:4326', 'EPSG:3857')),
    			// style: markerIcon,	
    			title: OnClickTextEN,
				name: "EQ " + StrumQuakeMagType + ': ' + StrumQuakeMagValue,
				id: StrumQuakeCode
				
    		});
			
			STRUMMarker.setStyle(markerIcon)
			iconFeatures.push(STRUMMarker);

    	}

			var vectorSource = new ol.source.Vector({
			  features: iconFeatures //add an array of features
			});;
			
			markerIcon = new ol.style.Style({
				image: new ol.style.Icon(({
					src: urlIcon5,
				    scale: 0.02
					})),

			});
			
			vectorLayer = new ol.layer.Vector({
				  source: vectorSource,	
				  title: 'Instrumental seismicity'
			});
			
			//vectorLayer.setZIndex(10);
			map.addLayer(vectorLayer);	
    }

    var abbr = "Remove instrumental seismicity from map";
    var quakeword = ' EARTHQUAKES'

    document.getElementById('numSTRUM').innerHTML = '<div id="ClearMapSTRUM" onclick="ClearMapSTRUM();"><abbr id="ClearMapSTRUMabbr" title="' + abbr + '"><img src="./images/clearMap.png" width = "25px" ></abbr></div> '+ iconFeatures.length + quakeword ;
    
	// non più usato col nuovo controllo dei livelli GT2023
	// document.getElementById("ztop").disabled = false;
	// document.getElementById("zbot").disabled = false;
	
	
	$('#numSTRUM').show();
    // $('#ClearMapSTRUM').show();

    if (iconFeatures.length == 10000) {
		// alert("10000")
        $('#warningSTRUM').show()
    }
	$('#loading').hide()
}




function ClearMapSTRUM(){

	var featureCount = vectorLayer.getSource();
	if (featureCount) {
		var features = vectorLayer.getSource().getFeatures();
		features.forEach((feature) => {
			vectorLayer.getSource().removeFeature(feature);
		});
	};
    $('#numSTRUM').hide();
    $('#warningSTRUM').hide()

// non più usato col nuovo controllo dei livelli GT2023
// document.getElementById("ztop").disabled = true;
// document.getElementById("zbot").disabled = true;

map.removeLayer(vectorLayer);
}


document.addEventListener('DOMContentLoaded', function () {
	$(function () {
		$('#STRUMeqIcon').click(function (event) {
			$('#STRUMclose').show();
			$('#STRUMeqMenu').show();
		});
		$('#STRUMclose').click(function (event) {
			$('#STRUMeqMenu').hide();
		});

		fillSTRUMdiv()

	})
});





function fillSTRUMdiv(){

    // set initial date range (last 7 days)
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    }
    if(mm<10) {
        mm = '0'+mm
    }

    var last = new Date(today.getTime() - (7 * 24 * 60 * 60 * 1000));
    var day7 = last.getDate();
    var month7 = last.getMonth()+1;
    var year7 = last.getFullYear();
    if(day7<10) {
        day7 = '0'+ day7
    }
    if(month7<10) {
        month7 = '0'+ month7
    }

    document.getElementById('StartDateSTRUM').value = year7 + '-' + month7 + '-' + day7;
    document.getElementById('StopDateSTRUM').value = yyyy + '-' + mm + '-' + dd;

    // -----------  DATEPIKER
    $( function() {
        $( ".datepicker" ).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: '1985-01-01',
            maxDate: '0',
            changeMonth: false,
            changeYear: false
        });

    })


    // -----------  MAGNITUDE SLIDER
	var sliderM = document.getElementById('sliderMSTRUM');
	var StartMM = document.getElementById('StartMagSTRUM');
	var StopMM = document.getElementById('StopMagSTRUM');

	noUiSlider.create(sliderM, {
		start: [2, 10],
		step: 0.1,
		behaviour: 'drag',
		connect: true,
		range: {
			'min': -1,
			'max': 10
		}
	});

	sliderM.noUiSlider.on('update', function( values, handle ) {
		var value = values[handle];
		if ( handle ) {
			StopMM.value = Math.round(value*10)/10;
		} else {
			StartMM.value = Math.round(value*10)/10;
		}
	});

	StartMM.addEventListener('change', function(){
		sliderM.noUiSlider.set([this.value, null]);
	});

	StopMM.addEventListener('change', function(){
		sliderM.noUiSlider.set([null, this.value]);
	});



    // -----------  DEPTH SLIDER
    var sliderD = document.getElementById('sliderDSTRUM');
    var StartDD = document.getElementById('StartDepSTRUM');
    var StopDD = document.getElementById('StopDepSTRUM');

    noUiSlider.create(sliderD, {
      start: [0, 1000],
      step: 5,
      behaviour: 'drag',
      connect: true,
      range: {
          'min': -10,
          'max': 1000
      }
    });

    sliderD.noUiSlider.on('update', function( values, handle ) {
      var value = values[handle];
      if ( handle ) {
          StopDD.value = Math.round(value*10)/10;
      } else {
          StartDD.value = Math.round(value*10)/10;
      }
    });

    StartDD.addEventListener('change', function(){
      sliderD.noUiSlider.set([this.value, null]);
    });

    StopDD.addEventListener('change', function(){
      sliderD.noUiSlider.set([null, this.value]);
    });


    document.getElementById('StartLatSTRUM').value = lat1STRUM;
    document.getElementById('StopLatSTRUM').value = lat2STRUM;
    document.getElementById('StartLonSTRUM').value = lon1STRUM;
    document.getElementById('StopLonSTRUM').value = lon2STRUM;



}

var lat1STRUM = 35 - 0.5;
var lat2STRUM = 48 + 0.5;
var lon1STRUM = 6 - 0.5;
var lon2STRUM = 25 + 0.5;

var rectangleSTRUM;
// var latLngBoundsSTRUM = new google.maps.LatLngBounds(
               // new google.maps.LatLng(lat1STRUM, lon1STRUM),new google.maps.LatLng(lat2STRUM, lon2STRUM)
// )

function CreateRectSTRUM() {

	document.getElementById('selAreaSTRUM').style.display = "none";

	document.getElementById('NoselAreaSTRUM').style.display = "block";

	
		var elem_lat1 = document.getElementById("StartLatSTRUM").value;

		var elem_lat2 = document.getElementById("StopLatSTRUM").value;

		var elem_lon1 = document.getElementById("StartLonSTRUM").value;

		var elem_lon2 = document.getElementById("StopLonSTRUM").value;

		var style1 = new ol.style.Style({
			fill: new ol.style.Fill({
			color: 'rgba(255, 255, 255, 0.6)',
			}),
			stroke: new ol.style.Stroke({
			// color: '#ff00ff',
			width: 2,
			}),
		}); 

		var p1 = [Number(elem_lon1), Number(elem_lat1)];
		var p2 = [Number(elem_lon1), Number(elem_lat2)];
		var p3 = [Number(elem_lon2), Number(elem_lat2)];
		var p4 = [Number(elem_lon2), Number(elem_lat1)];
	
var featureP = new ol.Feature({
            geometry: new ol.geom.Polygon([[fromLonLat(p1),fromLonLat(p2),
                fromLonLat(p3),fromLonLat(p4),fromLonLat(p1)]]),
			name: 'box'
        });

		
		vectorSourceP = new ol.source.Vector({
				features: [featureP],
				name: 'box'
			});

		 vectorLayerP = new ol.layer.Vector({
				source: vectorSourceP,
				style: style1,
				 displayInLayerSwitcher: false,
			 	openInLayerSwitcher: false,
				name: 'box'
			});
			
		//vectorLayerP.setZIndex(0);
		
		map.addLayer(vectorLayerP);	
					
	
	// var geometryFunction = ol.interaction.Draw.createBox()

	//   rectangle.setMap(null)
	// map.fitBounds(latLngBoundsSTRUM);


	// Listener che monitora il cambiamento dei bounds del rettangolo in mappa e aggiorna i campi di testo del form
	// google.maps.event.addListener(rectangleSTRUM, 'bounds_changed', function() {

		//acquisisco i vertici del rettangolo
		// bounds = rectangleSTRUM.getBounds();
		// var lat1 = bounds.getSouthWest().lat();
		// var lat2 = bounds.getNorthEast().lat();
		// var lon1 = bounds.getSouthWest().lng();
		// var lon2 = bounds.getNorthEast().lng();

		//arrotondo a 2 decimali
		// lat1 = Math.round(lat1*100)/100;
		// lat2 = Math.round(lat2*100)/100;
		// lon1 = Math.round(lon1*100)/100;
		// lon2 = Math.round(lon2*100)/100;

		// if (lat1 > lat2) {
		   // var lat2a = lat1
		   // lat1 = lat2
		   // lat2 = lat2a
		// }

		// aggiorno i campi del form
		// var elem_lat1 = document.getElementById("StartLatSTRUM");
		// elem_lat1.value = lat1;

		// var elem_lat2 = document.getElementById("StopLatSTRUM");
		// elem_lat2.value = lat2;

		// var elem_lon1 = document.getElementById("StartLonSTRUM");
		// elem_lon1.value = lon1;

		// var elem_lon2 = document.getElementById("StopLonSTRUM");
		// elem_lon2.value = lon2;

		// latLngBoundsSTRUM = new google.maps.LatLngBounds(
			// new google.maps.LatLng(lat1, lon1),
			// new google.maps.LatLng(lat2, lon2)
		// );
	// });
}

function resizeRectSTRUM() {

RemoveBox()
CreateRectSTRUM();
};

// non più usato col nuovo controllo dei livelli GT2023
//function SetZIndexSTR(topbot) {
//	if (topbot=='top') {
//		vectorLayer.setZIndex(10);
//	} else {
//		vectorLayer.setZIndex(0);
//	}
//}

function RemoveBox() {
	// alert("remove")
	// map.getLayers().forEach(layer => {
		// if (layer && layer.get('name') === 'box') { 
		// map.removeLayer(layer);
		// }
	// });
	var featureCount = vectorLayerP.getSource();
		if (featureCount) {
			var features = vectorLayerP.getSource().getFeatures();
			features.forEach((feature) => {
				vectorLayerP.getSource().removeFeature(feature);
			});
		};
	document.getElementById('selAreaSTRUM').style.display = "block";
	document.getElementById('NoselAreaSTRUM').style.display = "none";
}

