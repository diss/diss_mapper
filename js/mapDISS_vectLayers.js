// Orange CSS
var CSS_O = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  source: new ol.source.Vector({
    url: 'kml/CSS.kml',
    format: new ol.format.KML(),
  })
});
var CSSTOP_O = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  source: new ol.source.Vector({
    url: 'kml/CSS_TOP.kml',
    format: new ol.format.KML(),
  })
});


// Color CSS
var CSS_C = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  source: new ol.source.Vector({
    url: 'kml/CSS_gray.kml',
    format: new ol.format.KML()
  })
});
var CSSTOP_C = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  source: new ol.source.Vector({
    url: 'kml/CSS_TOP_gray.kml',
    format: new ol.format.KML()
  })
});


// Color subductions	  	  
var SUBcontour = new ol.layer.Vector({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  // source: new ol.source.Vector({
  //   url: 'kml/SUBD.kml',
  //   format: new ol.format.KML()
  // }),
  source: SUBcontour_wfs_contour,
  style: sdsLines_style
})
var SUBcontour_color = new ol.layer.Vector({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  openInLayerSwitcher: false,
  // source: new ol.source.Vector({
  //   url: 'kml/SUBD_gray.kml',
  //   format: new ol.format.KML()
  // }),
  source: SUBcontour_wfs_contour,
  style: sds_style2
});

// Gray subductions	  
var SUBzone = new ol.layer.Vector({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  openInLayerSwitcher: false,
  // source: new ol.source.Vector({
  //   url: 'kml/SUBD_gray.kml',
  //   format: new ol.format.KML()
  // }),
  source: SUBzone_wfs_source,
  style: sdsZone_style
});
var SUBzone_color = new ol.layer.Vector({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  openInLayerSwitcher: false,
  // source: new ol.source.Vector({
  //   url: 'kml/SUBD_gray.kml',
  //   format: new ol.format.KML()
  // }),
  source: SUBzone_wfs_source,
  style: sdsZone_style
});

var CSS_wfs = new ol.layer.Vector({
  source: CSS_wfs_source,
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  style: css_style
});

var CSSAreaGray = new ol.layer.Vector({
  source: CSS_wfs_source,
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  style: cssPlaneForColored_style
});

var CSS_planeForColored = new ol.layer.Vector({
  source: CSS_wfs_source,
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  style: cssPlaneForColored_style
});

var CSS_contour = new ol.layer.Vector({
  source: CSScontour_wfs,
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  style: sds_style2
});

var CSSTOP_wfs = new ol.layer.Vector({
  source: CSSTOP_wfs_source,
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  style: css_style2
});

var CSS_Colored_wfs = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  source: CSS_planeColored_source,
  style: sds_style2
});
var CSSTOP_Colored_wfs = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayInLayerSwitcher: false,
  source: CSSTOP_wfs_source,
  style: cssTopGray_style
});  