//Instantiate with some options and add the Control
var geocoder = new Geocoder('nominatim', {
  provider: 'osm',
  key:'',
  lang: 'it',
  placeholder: 'Search (OpenStreetMap)',
  limit: 15,
  debug: false,
  autoComplete: true,
  keepOpen: true,
  zindex: 50,
  // countrycodes : 'it'
});
map.addControl(geocoder);
  
//Listen when an address is chosen
// geocoder.on('addresschosen', function (evt) {
	// console.info(evt);
  // window.setTimeout(function () {
    // popup.show(evt.coordinate, evt.address.formatted);
  // }, 3000);
// });
