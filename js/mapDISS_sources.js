const wfsFormat = new ol.format.WFS();

const areaOfRelevanceSource = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "areaofrelevance" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        areaOfRelevanceSource.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});

const ISS_wfs_source = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "iss330" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        ISS_wfs_source.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});

const DSS_wfs_source = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "dss330" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        DSS_wfs_source.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});

const CSScontour_wfs = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "csscnt330" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        CSScontour_wfs.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});

const SUBcontour_wfs_contour = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "subdcnt330" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        SUBcontour_wfs_contour.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});
const SUBzone_wfs_source = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "subdzon330" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        SUBzone_wfs_source.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});
const CSS_wfs_source = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "csspln330" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        CSS_wfs_source.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});

const CSS_planeColored_source = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "css330contour" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        CSS_planeColored_source.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});

const CSSTOP_wfs_source = new ol.source.Vector({
  format: wfsFormat,
  loader: function (extent, resolution, projection) {
    var urlWfs = "https://services.seismofaults.eu/geoserver/DISS_current/wfs" + '?service=WFS&' +
      'version=1.1.0&request=GetFeature&typename=' + '' + ':' + "csstop330" + '&' +
      'outputFormat=gml3&srsname=' + projection.getCode() + '&'
      // +'bbox=' + extent.join(',') + ',' + projection.getCode()
      ;

    fetch(urlWfs)
      .then(function (response) {
        return response.text();
      })
      .then(function (responseText) {
        const features = wfsFormat.readFeatures(responseText);
        CSSTOP_wfs_source.addFeatures(features);
      });
  },
  //strategy: new ol.loadingstrategy.bboxStrategy
});

var AF_source = new ol.source.Vector({
  url: 'kml/AFT.kml',
  format: new ol.format.KML(),
});

var AFD_source = new ol.source.Vector({
  url: 'kml/AFD.kml',
  format: new ol.format.KML(),
});