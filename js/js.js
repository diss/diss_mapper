var sTypeUrl;



	
function  test_type() {
	var url = window.location.href;
	var nchar = url.length - 3;
	var sType = url.substr(nchar,3);


	var pos = url.indexOf("/diss3")

	var vers = url.substring(pos+5, pos+8)

	var DISSVER = vers.substring(0,1) + "." + vers.substring(1,2) + "." + vers.substring(2,3)
	
	document.getElementById("VERS").innerHTML = "DISS " + DISSVER;

	
	switch (sType) {
	case "ISS":
		sTypeUrl = "ISS"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "_SELECTEDTAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Individual Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/individual-seismogenic-sources" title="What is a Individual Seismogenic Source?" target="_blank"><img src="img/Help2.png"/></a>';
		
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';
		
		GeneralInfo();
		
		break;
		
	case "CSS":
		sTypeUrl = "CSSTOP"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "_SELECTEDTAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Composite Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/composite-seismogenic-sources" title="What is a Composite Seismogenic Source?" target="_blank"><img src="img/Help2.png"/></a>';
		
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';
		
		GeneralInfo();
		
		break;
		
	case "DSS":
		sTypeUrl = "DSS"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "_SELECTEDTAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Debated Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/debated-seismogenic-sources" title="What is a Debated Seismogenic Source?" target="_blank"><img src="img/Help2.png"/></a>';
		
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';

		GeneralInfo();
		
		break;
		
	case "UBD":
		sTypeUrl = "SUBD"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "_SELECTEDTAB";
		
		document.getElementById("TITLE").innerHTML = "Subduction Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="https://diss.ingv.it/knowledge-base/basic-definitions/subduction-zones" title="What is a Subduction Source?" target="_blank"><img src="img/Help2.png"/></a>';
				
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';	

		GeneralInfo();
		
		break;
		
	default: 
		document.getElementById("TAB1").className = "_SELECTEDTAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "DISS Data Tables - Version " + DISSVER;
		document.getElementById("INFO_ICON").innerHTML = '';
		
		$(document).ready(function() {
			$("#mainDIV").load("html/home.html");
		});		
	
		break;
	}
	
	
	// document.getElementById("TITLE").innerHTML = "DISS Data Tables - Version " + DISSVER;
	
}

function GeneralInfo() {
	  var line =[];
	  var i;
	$.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/html; charset=ISO-8859-1');
		},
	}); 
	$.get('data/'  + sTypeUrl + '4web.MID', function(data){
	
		var Splitdata = data.split('\r\n');
		var i;
		for (i = 0; i < Splitdata.length; i++) {

			line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
			// alert(line[1]);
				// line[i] = line[i].replace(/"/g,'');
			var sID = line[i][0];
			if (sID == "") {
				i = i-1
				break; 
			};
			sID = line[i][0].replace(/"/g,'');
			// alert(sID);
		if ( i % 2 == 0 ) {
			var tr = '<tr class="EVEN">' 
		} else {
			var tr = '<tr class="ODD">' 
		}

		
		$("#TABINT").append(
		tr + 
		'<td class="stretto pointer">' + sID + '</td>' +
		'<td class="largo pointer">' + line[i][1].replace(/"/g,'')	 + '</td>' +
		'<td class="stretto pointer"><a href="sources.php?' + sID + '" ><img src="img/lens.gif" alt="Details" ></a></td>' +
		'<td class="stretto pointer"><a href="dissmap.html?ll=' + sID + '" target="_blank"><img class="pointer" src="img/map.gif" alt="map" ></a></td>' +
		'</tr>'
		)
		};
		
		$(".num").append(i+1);
	})
}



function createAFF(sIDsource, sType) {

	console.log("test paperino")
	console.log(sIDsource)
	console.log(sType)

	// ACTIVE FAULTS
	$.get('data/AFaults.MID', function (data) {
		var line = [];
		var nRow = 0;
		var Splitdata = data.split('\r\n');
		// alert(Splitdata.length);
		for (var i = 0; i < Splitdata.length - 1; i++) {

			line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
			// alert(line[1]);
			// line[i] = line[i].replace(/"/g,'');

			switch (sType) {
				case "IS":
					var sID = line[i][0].replace(/"/g, '');
					break;
				case "CS":
					var sID = line[i][1].replace(/"/g, '');
					break;
				case "SD":
					var sID = line[i][2].replace(/"/g, '');
					break;
			}
			// alert(sID);
			if (sID == sIDsource) {
				nRow = nRow + 1;
				var sIDA = line[i][3].replace(/"/g, '');
				var sName = line[i][5].replace(/"/g, '');
				var sRef = line[i][28].replace(/"/g, '');
				var sRow = document.getElementById("AFAcont").innerHTML;
				if (nRow & 1) {
					document.getElementById("AFAcont").innerHTML = sRow + '<tr class="ODD">	<td class="ODD">' + sIDA + '</td>	<td class="ODD">' + sName + '</td><td class="ODD">' + sRef + '</tr>'
				}
				else {
					document.getElementById("AFAcont").innerHTML = sRow + '<tr class="EVEN">	<td class="EVEN">' + sIDA + '</td>	<td class="EVEN">' + sName + '</td><td class="EVEN">' + sRef + '</tr>'
				}


			}
		}
		if (nRow == 0) { document.getElementById("AFAcont").innerHTML = "" }
	});


	// ACTIVE FOLDS
	$.get('data/AFolds.MID', function (data) {
		var line = [];
		var nRow = 0;
		var Splitdata = data.split('\r\n');
		// alert(Splitdata.length);
		for (var i = 0; i < Splitdata.length - 1; i++) {

			line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
			// alert(line[1]);
			// line[i] = line[i].replace(/"/g,'');

			switch (sType) {
				case "IS":
					var sID = line[i][0].replace(/"/g, '');
					break;
				case "CS":
					var sID = line[i][1].replace(/"/g, '');
					break;
				case "SD":
					var sID = line[i][2].replace(/"/g, '');
					break;
			}
			// alert(sID);
			if (sID == sIDsource) {
				nRow = nRow + 1;
				var sIDA = line[i][3].replace(/"/g, '');
				var sName = line[i][5].replace(/"/g, '');
				var sRef = line[i][28].replace(/"/g, '');
				var sRow = document.getElementById("AFOcont").innerHTML;
				if (nRow & 1) {
					document.getElementById("AFOcont").innerHTML = sRow + '<tr class="ODD">	<td class="ODD">' + sIDA + '</td>	<td class="ODD">' + sName + '</td><td class="ODD">' + sRef + '</tr>'
				}
				else {
					document.getElementById("AFOcont").innerHTML = sRow + '<tr class="EVEN">	<td class="EVEN">' + sIDA + '</td>	<td class="EVEN">' + sName + '</td><td class="EVEN">' + sRef + '</tr>'
				}


			}
		}
		if (nRow == 0) { document.getElementById("AFOcont").innerHTML = "" }
	});


	// Return array of string values, or NULL if CSV string not well formed.


}



function chiudiDiv() {
	var div = document.getElementById("mioDiv");
	div.classList.remove("animazione");
}


function activeButton(idElement) {
	var divActive = document.getElementById(idElement);

	// var btnActiveList = divActive.getElementsByClassName("TAB_A");
	// var btnActive = btnActiveList[0];

	var btnContainer = document.getElementById("sourcesMenu");
	var btns = btnContainer.getElementsByClassName("TAB");

	for (var i = 0; i < btns.length; i++) {
		btns[i].classList.remove("active");
	}

	if (idElement !== null){
		divActive.classList.add("active");
	}

	
}
	
