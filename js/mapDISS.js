var imageBig ="75";
var imageSmall = "50";
var divToHide = document.getElementById('layersElegenda');
divToHide.style.display = 'None';
var divToHide2 = document.getElementById('licenza');
divToHide2.style.display = 'None';

var defaultExtent =  [309921.0567469017, 1078821.99000645, 6544701.1877486508, 7951766.235597581];

mostraInfo();
let previouslySelected = [];
let stylesSelected = [];
let allFeaturesList = [];

var url = window.location.href;
var pos = url.indexOf("/dissmap")
var vers = url.substring(pos-3, pos)
var {fromLonLat} = ol.proj;
var DISSVER = "&nbsp&nbspDISS " + vers.substring(0,1) + "." + vers.substring(1,2) + "." + vers.substring(2,3)
document.getElementById("VERS").innerHTML = "<a href='./' target='blank'>" + DISSVER + "</a>";


var mousePositionControl = new ol.control.MousePosition({
  coordinateFormat: ol.coordinate.createStringXY(3),
  projection: 'EPSG:4326',
  className: 'custom-mouse-position',
  target: document.getElementById('mouse-position'),
  undefinedHTML: '&nbsp;'
});
	  
/*** Elements that make up the popup.*/
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

/*** Create an overlay to anchor the popup to the map.*/
var overlay  = new ol.Overlay({
  element: container,
  positioning: 'bottom-left',
  autoPan: true,
  autoPanAnimation: {
    duration: 250
  }
});

/*** Add a click handler to hide the popup. @return {boolean} Don't follow the href.*/
closer.onclick = function() {
  overlay.setPosition(undefined);
  closer.blur();
  return false;
};
 
var styles = [
  'Road',
  'Topo1',
  'Topo2',
	'Aerial',
	'Hybrid',
	'Ocean'
];


///////////////////////BASE LAYERS//////////////////////////
var baselayers = new ol.layer.Group({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete:  true,  //GT2023 CUSTOM
  displayShrink: true,  //GT2023 CUSTOM
  title: 'Base Layers',
  name: 'Base Layers',
  displayInLayerSwitcher: true,
  openInLayerSwitcher: false,
  layers: [
    Ocean,
    Hybrid,
    Aerial,
    Topo2,
    Topo1,    
    Road
  ],
  visible: true
})

var copy=[];
//ROAD
copy[0] =  '<b>Map Data © <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors.</b>';
//TOPO1
copy[2] = '<b>Map Data © <a href="https://openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> Contributor, SRTM | Map View © <a href="http://opentopomap.org" target="_blank">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/" target="_blank">CC-BY-SA</a></b>)'
//TOPO2
copy[1] = '<b>Map tiles by <a href="http://stamen.com" target="_blank">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0" target="_blank">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org" target="_blank">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright" target="_blank">ODbL</a></b>'
//AERIAL - ESRI
copy[3] = '<b>Map Tiles © 2022 <a href="https://www.arcgis.com/home/item.html?id=ab399b847323487dba26809bf11ea91a" target="_blank">Esri</a>, Maxar, Earthstar Geographics, CNES/Airbus DS, USDA FSA, USGS,<br> Getmapping, Aerogrid, IGN, IGP, and the GIS User Community</b>'
//HYBRID - ESRI & STAMEN	  
copy[4] = '<b>Map Tiles © 2022 <a href="https://www.arcgis.com/home/item.html?id=ab399b847323487dba26809bf11ea91a" target="_blank">Esri</a>, Maxar, Earthstar Geographics, CNES/Airbus DS, USDA FSA, USGS,<br> Getmapping, Aerogrid, IGN, IGP, and the GIS User Community and by <a href="http://stamen.com" target="_blank">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0" target="_blank">CC BY 3.0</a>.<br>Data by <a href="http://openstreetmap.org" target="_blank">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright" target="_blank">ODbL</a></b>'
//OCEAN - Esri
copy[5] = '<b>Map Tiles © 2022 <a href="https://www.arcgis.com/home/item.html?id=ab399b847323487dba26809bf11ea91a" target="_blank">Esri</a>, GEBCO, NOAA, Garmin, HERE, and other contributors</b>'


//////////////////////DISS /LAYERS//////////////////////////

//active faults
var AFT = new ol.layer.Vector({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  title: '<span class="name" title="Active Faults">Active Faults</span><span class="img" title="Active Faults"><img src="img/AFT.png" height="' + imageBig + '"></span>',
  name: 'Active Faults',
  extent: defaultExtent,
  source: AF_source,
  visible: false

});
AFT.getSource().on('change', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {
    var extent = source.getExtent();
    if (extent) {
      AFT.setExtent(extent);
    }
  }
});



//active folds
var AFD = new ol.layer.Vector({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  title: '<span class="name" title="Active Folds">Active Folds</span><span class="img" title="Active Folds"><img src="img/AFD.png" height="' + imageBig + '"></span>',
  name: 'Active Folds',
  source: AFD_source,
  extent: defaultExtent,
  visible: false,
  oninfo: false  //tentativo di disabilitare le info in modo selettivo
});

AFD.getSource().on('change', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {
    var extent = source.getExtent();
    if (extent) {
      AFD.setExtent(extent);
    }
  }
});


var ISS = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  title: '<span class="name" title="Individual Seismogenic Sources">Individual<br>Seismogenic Sources <br>(ISS)</span><span class="img" title="Individual Seismogenic Sources"><img src="img/ISS.png" height="' + imageSmall + '"></span>',
  name: 'ISS',
  extent: defaultExtent,
  source: ISS_wfs_source,
  style: iss_style
});
ISS.getSource().on('change', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {
    var extent = source.getExtent();
    if (extent) {
      ISS.setExtent(extent);
    }
  }
});



//DSS
var DSS = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  title: '<span class="name" title="Debated Seismogenic Sources">Debated<br>Seismogenic Sources<br>(DSS)</span><span class="img" title="Debated Seismogenic Sources"><img src="img/DSS.png" height="' + imageSmall + '"></span>',
  name: "DSS",
  // source: new ol.source.Vector({
  //   url: 'kml/DSS.kml',
  //   format: new ol.format.KML()
  // }),
  extent: defaultExtent,
  source: DSS_wfs_source,
  visible: false,
  style: dss_style
});

DSS.getSource().on('change', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {
    var extent = source.getExtent();
    if (extent) {
      DSS.setExtent(extent);
    }
  }
});


//SUBD
var SUBDgray = new ol.layer.Group({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  baseLayer: true,
  name: '<span class="name" title="">Monochromatic <br>depth isolines</span><span class="img" title="Gray"><img src="img/SUBD_gray.png" height="' + imageSmall + '"></span>',
  openInLayerSwitcher: false,
  //layers: [SUBDC, SUBDG],
  layers: [SUBzone, SUBcontour],
  visible: true
}) 
var SUBDcolor = new ol.layer.Group({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  baseLayer: true,
  name: '<span class="name" title="">Color-coded depth <br>isolines</span><span class="img" title="Colour"><img src="img/SUBD.png" height="' + imageSmall + '"></span>',
  openInLayerSwitcher: false,
  //layers: [SUBDC, SUBDG],
  layers: [SUBzone_color, SUBcontour_color],
  visible: false
}) 
var SUBD = new ol.layer.Group({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayShrink: false,
  title: '<span class="name" title="Subduction Sources">Subduction Sources<br>(SDS)</span>',
  name: 'SDS',
  extent: defaultExtent,
  openInLayerSwitcher: true,
  //layers: [SUBDC, SUBDG],
  layers: [SUBDcolor, SUBDgray],
  //layers: [ SUBDgray],
  visible: true
});

SUBzone.getSource().on('change', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {
    var extent = source.getExtent();
    if (extent) {
      SUBD.setExtent(extent);
    }
  }
});




//AREA OF RELEVANCE
var AR = new ol.layer.Vector({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete:  true,  //GT2023 CUSTOM
  title: '<span class="name" title="Area of relevance">Area of relevance</span><span class="img" title="Area of relevance"><img src="img/AoR.png" height="' + imageSmall +'"></span>',
  name: 'Area',
  source: areaOfRelevanceSource,
  style: areaOfRelevanceStyle,
  extent: defaultExtent,
  // source: new ol.source.Vector({
  //   url: 'kml/RelevanceArea.kml',
  //   format: new ol.format.KML()
  // })
});
AR.getSource().on('change', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {
    var extent = source.getExtent();
    if (extent) {
      AR.setExtent(extent);
    }
  }
});
 
//CSS
var CSS_O_group = new ol.layer.Group({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: '<span class="name" title="">Fault-plane projection</span><span class="img" title="Classic"><img src="img/CSS.png" height="' + imageSmall + '"></span>',
  baseLayer: true,
  openInLayerSwitcher: false,
  //layers: [CSS_O, CSS_O],
  layers: [CSS_wfs, CSSTOP_wfs],
  visible: true
})
var CSS_C_group = new ol.layer.Group({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: '<span class="name" title="">Color-coded depth <br>isolines</span><span class="img" title="Colour"><img src="img/CSS_iso.png" height="' + imageSmall + '"></span>',
  baseLayer: true,
  openInLayerSwitcher: false,
  //layers: [CSSTOP_C, CSS_C],
  // layers: [CSS_planeForColored, CSS_Colored_wfs, CSSTOP_Colored_wfs],
  layers: [CSSAreaGray, CSS_contour],
  visible: false
})
var CSS_group = new ol.layer.Group({
  displayInfo: true,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  displayShrink: false,  //GT2023 CUSTOM
  title: '<span class="name" title="Composite Seismogenic Sources">Composite<br>Seismogenic Sources<br>(CSS)</span>',
  name: 'CSS',
  extent: defaultExtent,
  openInLayerSwitcher: true,
  layers: [CSS_C_group, CSS_O_group],
  visible: true
});

CSS_wfs.getSource().on('change', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {
    var extent = source.getExtent();
    if (extent) {
      CSS_group.setExtent(extent);
    }
  }
});

///////////////////////MAP OBJ//////////////////////////
view = new ol.View({
  center: ol.proj.fromLonLat([12.5, 42]),
  zoom: 6,
	maxZoom: 13,
	minZoom: 2
});

var map = new ol.Map({
  loadTilesWhileInteracting: true,
  target: 'map',
  view: view,
  overlays: [overlay],
  controls: ol.control.defaults().extend([mousePositionControl,
    new ol.control.FullScreen()
  ]),
});	  

map.addLayer(baselayers);


//*** ISTAT LAYERS */

var ITA_WMS = new ol.source.TileWMS({
  "url": "https://services-storing.ingv.it/geoserver/CFTI/wms?service=wms",
  "projection": "EPSG:3857",
  "crossOrigin": 'anonymous',
  "params": {
    "LAYERS": "ITA01012019_WGS84",
    "FORMAT": "image/gif",
    "TRANSPARENT": "TRUE",
    "VERSION": "1.3.0",
  },
  "attributions": [
    "<a href='http:www.istat.it>&copy; ISTAT</a>"
  ]
})

var REG_WMS = new ol.source.TileWMS({
  "url": "https://services-storing.ingv.it/geoserver/CFTI/wms?service=wms",
  "projection": "EPSG:3857",
  "crossOrigin": 'anonymous',
  "params": {
    "LAYERS": "Reg01012019_WGS84",
    "FORMAT": "image/gif",
    "TRANSPARENT": "TRUE",
    "VERSION": "1.3.0",
  },
  "attributions": [
    "<a href='http://www.istat.it'>&copy; ISTAT</a>"
  ]
})

var PROV_WMS = new ol.source.TileWMS({
  "url": "https://services-storing.ingv.it/geoserver/CFTI/wms?service=wms",
  "projection": "EPSG:3857",
  "crossOrigin": 'anonymous',
  "params": {
    "LAYERS": "ProvCM01012019_WGS84",
    "FORMAT": "image/gif",
    "TRANSPARENT": "TRUE",
    "VERSION": "1.3.0",
  },
  "attributions": [
    "<a href='http://www.istat.it'>&copy; ISTAT</a>"
  ]
})

var COM_WMS = new ol.source.TileWMS({
  "url": "https://services-storing.ingv.it/geoserver/CFTI/wms?service=wms",
  "projection": "EPSG:3857",
  "crossOrigin": 'anonymous',
  "params": {
    "LAYERS": "Com01012019_WGS84",
    "FORMAT": "image/gif",
    "TRANSPARENT": "TRUE",
    "VERSION": "1.3.0",
  },
  "attributions": [
    "<a href='http://www.istat.it'>&copy; ISTAT</a>"
  ]
})


var istat1 = new ol.layer.Tile({
  "displayInfo": false,     //GT2023 CUSTOM
  "noSwitcherDelete": true,  //GT2023 CUSTOM
  "title": "Country (1)",
  "name": "ITA",
  "visible": true,
  "opacity": 0.9,
  "source": ITA_WMS,
  "zIndex": 0
});

var ISTATLayers = new ol.layer.Group({
  "displayInfo": false,     //GT2023 CUSTOM
  "noSwitcherDelete": true,  //GT2023 CUSTOM
  "displayShrink": true,  //GT2023 CUSTOM
  title: '<span class="name" title="Italian administrative boundaries">Italian administrative<br>boundaries</span>',
  name: "ISTAT",
  openInLayerSwitcher: false,
  extent: defaultExtent,
  visible: false,
  layers: [
    istat1,
    new ol.layer.Tile({
      "displayInfo": false,     //GT2023 CUSTOM
      "noSwitcherDelete": true,  //GT2023 CUSTOM
      "title": "Regions (20)",
      "name": "REG",
      "visible": true,
      "opacity": 0.9,
      "source": REG_WMS,
      "zIndex": 0
    }),
    new ol.layer.Tile({
      "displayInfo": false,     //GT2023 CUSTOM
      "noSwitcherDelete": true,  //GT2023 CUSTOM
      "title": "Provinces (107)",
      "name": "PROV",
      "visible": true,
      "opacity": 0.7,
      "source": PROV_WMS,
      "zIndex": 0
    }),
    new ol.layer.Tile({
      "displayInfo": false,     //GT2023 CUSTOM
      "noSwitcherDelete": true,  //GT2023 CUSTOM
      "title": "Municipalities (7.926)",
      "name": "COM",
      "visible": true,
      "opacity": 0.5,
      "source": COM_WMS,
      "zIndex": 0
    })
  ]
})


istat1.getSource().on('tileloadend', function (event) {
  var source = event.target;
  if (source.getState() === 'ready') {

    var extent = source.projection_.extent_;
    

    ISTATLayers.setExtent([478673.8104110759, 4411265.910262679, 2293181.5103414333, 5909489.863677079]);

  }
});

//map.addLayer(vectorLayerA);
map.addLayer(ISTATLayers);
map.addLayer(AR);
map.addLayer(SUBD);
map.addLayer(DSS);
map.addLayer(CSS_group);
map.addLayer(ISS);
map.addLayer(AFD);
map.addLayer(AFT);

///////////////////////LayerSwitcher//////////////////////////
//rita
var layerURLs = {
  'Active Faults': 'https://diss.ingv.it/knowledge-base/general-concepts/understanding-active-faulting',
  'Active Folds': 'https://diss.ingv.it/knowledge-base/general-concepts/understanding-active-faulting',
  'ISS': 'https://diss.ingv.it/basic-definitions/individual-seismogenic-sources',
  'CSS': 'https://diss.ingv.it/basic-definitions/composite-seismogenic-sources',
  'SDS': 'https://diss.ingv.it/basic-definitions/subduction-zones',
  'DSS': 'https://diss.ingv.it/basic-definitions/debated-seismogenic-sources',
  'Area': 'https://diss.ingv.it/basic-definitions/area-of-relevance'
};
map.addControl(new ol.control.LayerSwitcher({ 
  target: 'layersElegenda',
  trash: true, 
  extent: true,
  oninfo: function (l) {
    if (l.get('name') && l.get('name') =='ExternalLayer'){
      var url = 'https://diss.ingv.it/mapper-credits';
    }else{
      var layerName = l.get('name');
      var url = layerURLs[layerName];
    }
    
    if (url) {
      var win = window.open(url, '_blank');
      win.focus();
    }
  },
  collapsed: true 
}));

///////////////////////CONTROL//////////////////////////
var scaleline = new ol.control.ScaleLine();
map.addControl(scaleline);
var select = document.getElementById('layer-select'); 

var cap = new ol.control.WMSCapabilities({
  // target: $('.options').get(0),
  target: document.body,
  srs: ['EPSG:3857'],
  cors: true,
  popupLayer: true,
  placeholder: 'Select a WMS  service or type/paste an URL...',
  title: 'WMS Services',
  searchLabel: 'Search....',
  // optional: 'token',
  services: {
    'ITHACA (ISPRA)': 'https://sgi2.isprambiente.it/arcgis/services/servizi/ithaca_new/MapServer/WmsServer?Request=GetCapabilities&service=WMS',
    // 'MPS04': 'http://mps04-ws.pi.ingv.it/wms_mps04_pga?SERVICE=WMS&VERSION=1.0.0&REQUEST=GetCapabilities',
    // 'ZS9 (Geoportale Nazionale)': 'http://wms.pcn.minambiente.it/ogc?map=/ms_ogc/WMS_v1.3/Vettoriali/Zone_sismogenetiche_ZS9.map',
    'GEO 1:100.000 (ISPRA)': 'https://sgi2.isprambiente.it/arcgis/services/servizi/carta_geologica_100k/MapServer/WmsServer?Request=GetCapabilities&service=WMS',
    // 'Pericolosità da alluvioni (ISPRA)': 'http://wms.pcn.minambiente.it/ogc?map=/ms_ogc/WMS_v1.3/Vettoriali/Alluvioni_Estensione.map',
    'EFSM20': 'https://services.seismofaults.eu/EFSM20/ows?service=WMS&request=getCapabilities',
    'NEAMTHM18' : 'https://services.tsunamidata.org/NEAMTHM18/ows?service=WMS&request=GetCapabilities',
    'MPS04 PGA': 'https://mps04-ws.pi.ingv.it/wms_mps04_pga?SERVICE=WMS&REQUEST=GetCapabilities',
    'MPS04 SA': 'https://mps04-ws.pi.ingv.it/wms_mps04_sa?SERVICE=WMS&REQUEST=GetCapabilities',
    'ESRM20  Risk': 'https://maps.eu-risk.eucentre.it/mapproxy/European_Risk_Index_Gridded/ows?service=WMS&REQUEST=GetCapabilities',
    'ESRM20 Exposure': 'https://maps.eu-risk.eucentre.it/mapproxy/European_Exposure_Gridded/ows?service=WMS&REQUEST=GetCapabilities',
    'ESRM20 Site Response': 'https://maps.eu-risk.eucentre.it/mapproxy/European_Site_Model_Data/ows?service=WMS&REQUEST=GetCapabilities'
  },
  // Show trace in the console
  trace: true
});
map.addControl(cap);

cap.on('load', function (e) {
  console.log(e)
  map.addLayer(e.layer)
  urlString = e.options.source.url;
  if (urlString.includes('maps.eu-risk.eucentre.it') || urlString.includes('mps04-ws.pi.ingv.it') 
    || urlString.includes('services.tsunamidata.org') || urlString.includes('services.seismofaults.eu')
    || urlString.includes('sgi2.isprambiente.it')  ){
    e.layer.set('displayInfo', true);
    e.layer.set('name', 'ExternalLayer');
  }
  
})


//save button
document.getElementById('export-png').addEventListener('click', function () {
  map.once('rendercomplete', function () {
    var mapCanvas = document.createElement('canvas');
    // mapCanvas.setAttribute("crossorigin", "anonymous");
    // mapCanvas.crossOrigin = "anonymous"
    var size = map.getSize();
    mapCanvas.width = size[0];
    mapCanvas.height = size[1];
    var mapContext = mapCanvas.getContext('2d');
    Array.prototype.forEach.call(document.querySelectorAll('.ol-layer canvas'), function (canvas) {
      // canvas.setAttribute("crossorigin", "anonymous");
      if (canvas.width > 0) {
        var opacity = canvas.parentNode.style.opacity;
        mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
        var transform = canvas.style.transform;
        // Get the transform parameters from the style's transform matrix
        var matrix = transform
          .match(/^matrix\(([^\(]*)\)$/)[1]
          .split(',')
          .map(Number);
        // Apply the transform to the export map context
        CanvasRenderingContext2D.prototype.setTransform.apply(
          mapContext,
          matrix
        );
        mapContext.drawImage(canvas, 0, 0);
      }
    }
    );

    const img = new Image();
    img.src = 'img/header2021.jpg';
    var scaleFactor = mapCanvas.width / img.width;
    var newHeight = img.height * scaleFactor;
    mapContext.drawImage(img, 0, 0, mapCanvas.width, newHeight);
    mapContext.fillStyle = 'white';
    mapContext.fillRect(mapCanvas.width, 0, mapCanvas.width, newHeight);

    mapContext.font = '30px Arial';
    mapContext.fillStyle = 'black';
    mapContext.fillText('DISS Version 3.3.1', mapCanvas.width, newHeight / 2);

    if (navigator.msSaveBlob) {
      // link download attribuute does not work on MS browsers
      navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
    } else {
      var link = document.getElementById('image-download');
      // link.crossOrigin = "anonymous";
      link.href = mapCanvas.toDataURL();
      link.click();
    }
  });
  map.renderSync();
});


///////////////////////EVENTS//////////////////////////
//single click in feature
map.on('singleclick', function (evt) {

  var url = new URL(window.location.href);
  url.searchParams.delete('source');
  window.history.replaceState({}, '', url.href);
  
  coordinate = evt.coordinate;

 
  showPointFeature = null;
  // features = map.getFeaturesAtPixel(evt.pixel);

  //verifico se è attivo il layer Instrumental seismicity : se si lo metto su (z-index)
  
  map.getLayers().forEach(function (layer) {
    
    if (layer.values_.title && layer.values_.title == "Instrumental seismicity" && layer.values_.visible && layer.values_.visible == true){    
      layer.values_.zIndex = 500;

    }
  });

  //verifico se ho selezionato una feature Instrumental seismicity o meno
  
  map.forEachFeatureAtPixel(evt.pixel, function (feat, layer) {

    if (layer.values_.title && layer.values_.title == "Instrumental seismicity" && layer.values_.visible && layer.values_.visible == true) {
      
      control = null
      showIS(feat, coordinate, evt.pixel);
    } else if (feat.values_.idsource){
      control = "showDiss"
    }

  });



  if (window.matchMedia("(max-width: 768px)").matches){
    if (control == "showDiss"){
      displayFeatureInfo(evt.pixel);
      open_closeMenuMobile = 0;
      showMenuMobile();
    }else{
      // open_closeMenuMobile = 0;
      // showMenuMobile();
    }
    
    
  } else{
    displayFeatureInfo(evt.pixel);
  }


});




//pointermove on feature
map.on('pointermove', function (evt) {
  if (evt.dragging) {
    return;
  }
  var pixel = map.getEventPixel(evt.originalEvent);
  displayFeatureInfoRoll(pixel)

});

/////////////////////// FUNCTIONS FOR EVENTS//////////////////////////
//events on vect layers
var displayFeatureInfo = function (pixel) {

  var features = map.getFeaturesAtPixel(pixel);
  if (features.length > 0) {
    activeButton(null);
    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'none';
    
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoCSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoSDSList');
    divToHide2.style.display = 'none';
    var content = document.getElementById('contenutoDetail');
    content.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoISSList');
    divToHide2.style.display = 'block';
  }


  

  //1.ripristinare stili delle features vecchie
  if (previouslySelected.length > 0) {
    for (i = 0; i < previouslySelected.length; i++) {
      previouslySelected[i].setStyle(stylesSelected[i]);
    }

  }
  previouslySelected = [];
  stylesSelected = [];


  var features = [];
  map.forEachFeatureAtPixel(pixel, function (feature) {
    if (feature.id_){
      if (!feature.id_.includes('csstop')) {
        features.push(feature);
      }
    }
    


  });

  //2.prendo gli stili delle features nuove
  previouslySelected = features;
  for (i = 0; i < previouslySelected.length; i++) {
    var style = previouslySelected[i].getStyle();
    stylesSelected.push(style);
  }


  if (features.length > 0 && features[0].id_) {
    var info = [];
    var i, ii;
    var descr = '';
    var tr = "";
    for (i = 0, ii = features.length; i < ii; ++i) {

      //3. rita cambio colore delle features selezionate

      
      features[i].setStyle(selected_style);
      var extent = features[i].getGeometry().getExtent(); 
      var size = ol.extent.getSize(extent);
      var maxDimension = Math.max(size[0], size[1]);
      var zoom = map.getView().getZoomForResolution(maxDimension / map.getSize()[0]);
      zoom = (zoom * 95)/100

      sID = features[i].values_.idsource;

      // alert(fetnam)
      if (features[i].id_ != 'csstop' && features[i].id_ != 'Releva' && features[i].values_.idsource) {
        descr = descr + '<b>' + features[i].values_.idsource + '</b><br>' + '<hr><br>';
      }

      //creo intestazione tabella
      document.getElementById("contenutoISSList").innerHTML = '<table class="MAINTABLE listSingleClick"></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Map</th></table></div>';
      //popolo tabella delle sorgenti cliccate

      sType = sID.substring(2, 4) + "S";

      if (sType == "CSS") {
        //activeButton("TAB3");
        sType = "CSSTOP"
      } else if (sType == "SDS") {
        //activeButton("TAB5");
        sType = "SUBD"
      } else if (sType == "ISS") {
        // activeButton("TAB2");

      } else if (sType == "DSS") {
        //activeButton("TAB4");
      }

      if (i % 2 == 0) {
        tr += '<tr class="EVEN">'
      } else {
        tr += '<tr class="ODD">'
      }
      

      if (features[i].id_.includes("subdcnt")){
        d1 = features[i].values_.depth;
      } else if (features[i].id_.includes("csscnt330")) {
        d1 = features[i].values_.depth;
      } else{
        d1 = features[i].values_.sourcename;
      }


      if (features[i].id_.includes("subdcnt")) {
        tr +=
          '<td class="stretto" style="cursor:default;">' + sID + '</td>' +
          '<td class="largo" style="cursor:default;">' + d1 + '</td>' +
          '<td class="stretto" style="cursor:default;"></td>' +
          '</tr>';
      } else if (features[i].id_.includes("csscnt330")) {
 
        tr +=
          '<td class="stretto" style="cursor:default;">' + sID + '</td>' +
          '<td class="largo" style="cursor:default;">' + d1 + '</td>' +
          '<td class="stretto" style="cursor:default;"></td>' +
          '</tr>';
      } else {
        tr +=
          '<td class="stretto pointer" onclick=\'detail("' + sID + '", "' + sType + '", "' + zoom + '")\'>' + sID + '</td>' +
          '<td class="largo pointer" onclick=\'detail("' + sID + '", "' + sType + '",  "' + zoom + '")\'>' + d1 + '</td>' +
          '<td class="stretto pointer"><a onclick=\'zoomSorgenti("' + sID + '", "' + zoom + '")\'><img class="pointer" src="img/map.gif" alt="map" ></a></td>' +
          '</tr>';
      }

      //dentro il for
    }

    $("#TABINT").append(tr);


  }


};
var displayFeatureInfoRoll = function(pixel) {
    var features = [];
    map.forEachFeatureAtPixel(pixel, function(feature) {
      
      features.push(feature);
    });

    if (features.length > 0) {
      var info = [];
      var i, ii;
  
      for (i = 0, ii = features.length; i < ii; ++i) {
        info.push(features[i].values_.idsource);
	
      }
  
      if (info != '') {
	      document.body.style.cursor = 'pointer';
      } else { 
        document.body.style.cursor = '';
      }
    } else {
      document.body.style.cursor = '';
      
    }
};
//events for wms layers


///////////////////////?????????//////////////////////////
if  (control == 1)  //solo se NON sono stati inseriti i parametri aggiuntivi nel link
	{} else {
 // alert(centroidX);
	map.getView().fit(ol.proj.transformExtent([MinX,MinY,MaxX,MaxY], 'EPSG:4326', 'EPSG:3857'))
  	// view.animate({
          // center: ol.proj.transform([centroidX,centroidY], 'EPSG:4326', 'EPSG:3857'),
		  // zoom: 8,
          // <!-- duration: 2000 -->
        // });
	var url = window.location.href;
	var nchar = url.length - 5;
	var sType = url.substr(nchar,2);
	if (sType=="DS") {
		map.addLayer(DSS);
		DSS.setZIndex(3)
		document.getElementById("DSS").checked = true;
	}

};
var iconFeaturesA=[];

var IDs = new ol.Feature({
  geometry: new ol.geom.Point(ol.proj.transform([centroidX,centroidY], 'EPSG:4326',     
  'EPSG:3857')),
  name: IDsource,
  id: 'A'
});

iconFeaturesA.push(IDs);


var vectorSourceA = new ol.source.Vector({
  features: iconFeaturesA //add an array of features
});
iconStyleA = new ol.style.Style({
      image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
        opacity: 1,
		anchor: [0.5, 1],
        src: 'img/icon.png',
		scale: 0.05
      }))
    });
var vectorLayerA = new ol.layer.Vector({
  source: vectorSourceA,
  style: iconStyleA
});






///////////////////////FUNCTIONS////////////////////////

function mostraInfo() {

  activeButton("TAB1");

  var divToHide = document.getElementById('tornaSu');
  divToHide.style.display = 'none';

  //legenda
  var divToHide = document.getElementById('layersElegenda');
  divToHide.style.display = 'none';
  //testo
  var divToHide2 = document.getElementById('credits_licence');
  divToHide2.style.display = 'block';
  $("#contenutoCredits").load("html/home.html");
  var divToHide3 = document.getElementById('contenutoCredits');
  divToHide3.style.display = 'block';
  //licenza
  var divToHide2 = document.getElementById('licenza');
  $("#licenza").load("html/licenza.html");
  divToHide2.style.display = 'block';

  var divToHide2 = document.getElementById('contenutoISSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoCSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoDSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoSDSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoDetail');
  divToHide2.style.display = 'none';

}

function mostraLegend() {

  activeButton("TAB6");

  var divToHide = document.getElementById('tornaSu');
  divToHide.style.display = 'none';

  //legenda
  var divToHide = document.getElementById('layersElegenda');
  divToHide.style.display = 'block';
  //testo
  var divToHide2 = document.getElementById('credits_licence');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoISSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoCSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoDSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoSDSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoDetail');
  divToHide2.style.display = 'none';


}

function mostraISS() {

  var ISS_div = document.getElementById("contenutoISSList").innerHTML.trim();

  if (ISS_div == '' || ISS_div.indexOf("listSingleClick") != -1 ){
    activeButton("TAB2");
    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoCSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoSDSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    sTypeUrl = "ISS";
    document.getElementById("contenutoISSList").innerHTML = '<table class="MAINTABLE"></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Map</th></table></div>';
    listSources("contenutoISSList");
    
    var divToHide3 = document.getElementById('contenutoISSList');
    divToHide3.style.display = 'block';
  }else{
    activeButton("TAB2");
    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoCSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoSDSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    var divToHide3 = document.getElementById('contenutoISSList');
    divToHide3.style.display = 'block';
  }

  
}
function mostraCSS() {

  var CSS_div = document.getElementById("contenutoCSSList").innerHTML.trim();

  if (CSS_div == '') {
    activeButton("TAB3");
    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoISSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoSDSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    

    sTypeUrl = "CSSTOP";
    document.getElementById("contenutoCSSList").innerHTML = '<table class="MAINTABLE"><tr class="EVEN"></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Map</th></table></div>';
    listSources("contenutoCSSList");
    var divToHide3 = document.getElementById('contenutoCSSList');
    divToHide3.style.display = 'block';

  }else{
    activeButton("TAB3");
    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoISSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoSDSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    var divToHide3 = document.getElementById('contenutoCSSList');
    divToHide3.style.display = 'block';
  }

  
}
function mostraDSS() {

  var DSS_div = document.getElementById("contenutoDSSList").innerHTML.trim();

  if (DSS_div == '') {
    activeButton("TAB4");

    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoISSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoCSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoSDSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    
    sTypeUrl = "DSS";
    document.getElementById("contenutoDSSList").innerHTML = '<table class="MAINTABLE"><tr class="EVEN"></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Map</th></table></div>';
    listSources("contenutoDSSList");
    var divToHide3 = document.getElementById('contenutoDSSList');
    divToHide3.style.display = 'block';

  }else{
    activeButton("TAB4");
    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoISSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoCSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoSDSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    var divToHide3 = document.getElementById('contenutoDSSList');
    divToHide3.style.display = 'block';
  }

  
}
function mostraUBD() {

  var SDS_div = document.getElementById("contenutoSDSList").innerHTML.trim();

  if (SDS_div == '') {
    activeButton("TAB5");

    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoISSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoCSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    
    sTypeUrl = "SUBD";
    document.getElementById("contenutoSDSList").innerHTML = '<table class="MAINTABLE"><tr class="EVEN"></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Map</th></table></div>';
    listSources("contenutoSDSList");
    var divToHide3 = document.getElementById('contenutoSDSList');
    divToHide3.style.display = 'block';

  }else{
    activeButton("TAB5");
    var divToHide = document.getElementById('layersElegenda');
    divToHide.style.display = 'none';
    var divToHide2 = document.getElementById('credits_licence');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoISSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoCSSList');
    divToHide2.style.display = 'none';
    var divToHide2 = document.getElementById('contenutoDetail');
    divToHide2.style.display = 'none';
    var tornasu = document.getElementById('tornaSu');
    tornasu.style.display = 'block';
    var divToHide3 = document.getElementById('contenutoSDSList');
    divToHide3.style.display = 'block';
  }

  
}
function mostraDetail() {

  activeButton("TAB7");

  var divToHide = document.getElementById('layersElegenda');
  divToHide.style.display = 'none';
  var divToHide2 = document.getElementById('credits_licence');
  divToHide2.style.display = 'none';
  var tornasu = document.getElementById('tornaSu');
  tornasu.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoISSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoCSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoDSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoSDSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoDetail');
  divToHide2.style.display = 'block';
}


//rita
function listSources(contenutoDiv) {


  if (sTypeUrl == "CSS" || sTypeUrl == "CSSTOP") {
    // layerToUse = CSS_group;
  } else if (sTypeUrl == "SUBD") {


    // layerToUse = SUBD;
  } else if (sTypeUrl == "ISS") {
    // layerToUse = ISS;
  } else if (sTypeUrl == "DSS") {
    // layerToUse = DSS;

  }

  var line = [];
  var i;
  $.ajaxSetup({
    'beforeSend': function (xhr) {
      xhr.overrideMimeType('text/html; charset=ISO-8859-1');
    },
  });
  if (sTypeUrl == "CSS" || sTypeUrl == "CS"){
    sTypeUrl = "CSSTOP";
  }
  $.get('data/' + sTypeUrl + '4web.MID', function (data) {
    var Splitdata = data.split(/\r?\n/);
    var i;
    for (i = 0; i < Splitdata.length; i++) {
      line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
      // alert(line[1]);
      // line[i] = line[i].replace(/"/g,'');
      var sID = line[i][0];
      if (sID == "") {
        i = i - 1
        break;
      };
      sID = line[i][0].replace(/"/g, '');
      // alert(sID);
      if (i % 2 == 0) {
        var tr = '<tr class="EVEN" onclick="addSelectionToRow(this)">'
      } else {
        var tr = '<tr class="ODD" onclick="addSelectionToRow(this)">'
      }


      //calcolo zoom dalla feature:
      // featList = [];
      // getAllFeaturesInLayerGroup(layerToUse, featList);
      // featList.forEach(function (feature) {
      //   let description = feature.get('description');
      //   // Confronta il valore con quello desiderato
      //   if (description.includes(sID)) {
      //     var extent = feature.getGeometry().getExtent();
      //     var size = ol.extent.getSize(extent);
      //     var maxDimension = Math.max(size[0], size[1]);
      //     var zoom = map.getView().getZoomForResolution(maxDimension / map.getSize()[0]);
      //     zoom = (zoom * 95) / 100
      //   }
      // });


      $("div#" + contenutoDiv + " #TABINT").append(
        tr +
        '<td class="stretto pointer" onclick=\'detail("' + sID + '", "' + sTypeUrl + '", ' + null + ')\'>' + sID + '</td>' +
        '<td class="largo pointer" onclick=\'detail("' + sID + '", "' + sTypeUrl + '",  ' + null + ')\'>' + line[i][1].replace(/"/g, '') + '</td>' +
        '<td class="stretto pointer"><a onclick=\'zoomSorgenti("' + sID + '", ' + null +')\'><img class="pointer" src="img/map.gif" alt="map" ></a></td>' +
        '</tr>'
      )
    };
    $(".num").append(i + 1);
  })
}

function checkSource() {
  const url = new URL(window.location.href);
  const sourceParam = url.searchParams.get('source');
  sType = sourceParam.substring(2, 4) + "S";

  detail(sourceParam, sType, null);



}

function scrollToDiv(divToScroll){
  var container = document.getElementById('tabcont1');
  var targetElement = document.getElementById(divToScroll);
  container.scrollTop = targetElement.offsetTop;
  // divToScroll.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest", container: container });
  // tabcont1.getElementById(divToScroll).scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
}


function addSelectionToRow(rowSel){
  var rowSelToString= (rowSel.innerHTML).toString();

  if (rowSelToString.indexOf("IS") !== -1){
    var cont = document.getElementById('contenutoISSList');
  } else if (rowSel.innerHTML.indexOf("CS") !== -1) {
    var cont = document.getElementById('contenutoCSSList');
  } else if (rowSel.innerHTML.indexOf("DS") !== -1) {
    var cont = document.getElementById('contenutoDSSList');
  } else if (rowSel.innerHTML.indexOf("SD") !== -1) {
    var cont = document.getElementById('contenutoSDSList');
  }

  // var cont1 = document.getElementById('contenutoCSSList'); 
  var table = cont.querySelector('#TABINT');

  var rows = table.getElementsByTagName('tr');

  for (var i = 0; i < rows.length; i++) {
    rows[i].classList.remove('rowSelected');
    rowSel.classList.add('rowSelected');
 
  }

}



function showPicture(s1Pict, idSource){

  if (idSource.includes("IS")){
    sType = "IS";
  } else if (idSource.includes("CS")) {
    sType = "CS";
  } else if (idSource.includes("DS")) {
    sType = "DS";
  } else if (idSource.includes("SD")) {
    sType = "SD";
  }

  idSource = idSource.trim();


  $("#pictureContent").load("imgContent.php?idSource=" + idSource +"&img=" + s1Pict+"&sType="+sType, function () {
    dragElement(document.getElementById("pictureContent"));
    document.getElementById("pictureContent").style.display = "block";
  });
}


function detail(sID, sTypeUrl, zoom) {

  var url = new URL(window.location.href);
  url.searchParams.delete('source');
  url.searchParams.append('source', sID);
  window.history.replaceState({}, '', url.href);

  activeButton("TAB7");

  var divToHide = document.getElementById('layersElegenda');
  divToHide.style.display = 'none';
  var divToHide2 = document.getElementById('credits_licence');
  divToHide2.style.display = 'none';
  var tornasu = document.getElementById('tornaSu');
  tornasu.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoISSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoCSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoDSSList');
  divToHide2.style.display = 'none';
  var divToHide2 = document.getElementById('contenutoSDSList');
  divToHide2.style.display = 'none';

  var divToHide2 = document.getElementById('contenutoDetail');
  divToHide2.style.display = 'block';

  sType = sID.substring(2, 4) + "S";
  // if (sType == "CSS") {
  //   activeButton("TAB3");
  // } else if (sType == "SDS") {
  //   activeButton("TAB5");
  // } else if (sType == "ISS") {
  //   activeButton("TAB2");
  // } else if (sType == "DSS") {
  //   activeButton("TAB4");
  // }


  zoomSorgenti(sID, zoom);
  
  $("#contenutoDetail").load("mapperDetail.php?" + sID, function () {
    // Questa funzione di callback viene eseguita dopo il caricamento del template
    generalInfo(sTypeUrl, sID);
    csv = "data/ContComp4web.txt";
    csvparserNew(csv, sID);
    commimport_mapper(sID);

    pict = "data/LinkTable.txt";
    pictparser_mapper(pict, sID);
    ref = "data/AssignReferences4web.txt";
    refparser_mapper(ref, sID);

    if(zoom == null){
      anchor = '<a onclick=\'zoomSorgenti("' + sID + '", ' + zoom + ')\'><img class="pointer" src="img/map.gif" alt="map" ></a>';
    }else{
      anchor = '<a onclick=\'zoomSorgenti("' + sID + '", "' + zoom + '")\'><img class="pointer" src="img/map.gif" alt="map" ></a>';
    }
    
    $("#VALUEELEMENT_display").append(anchor);


    //window.location.href = "#TAB1";

    

    

    //zoom sulla feature
    //zoomToFeature(selectedFeature);

    // commimport();
    // var pos = url.indexOf("/diss3")
    // var vers = url.substring(pos + 5, pos + 8)
    // var DISSVER = vers.substring(0, 1) + "." + vers.substring(1, 2) + "." + vers.substring(2, 3)
    // document.getElementById("VERS").innerHTML = "DISS " + DISSVER;
  });

}

function zoomToFeature(feature) {
  var extent = feature.getGeometry().getExtent();
  map.getView().fit(extent, map.getSize());
}

function zoomSorgenti(IDsource, val_zoom) {


 

  //rita
  checkVisibleLayerAndZoom(IDsource, val_zoom);


  //0. itera su tutti i layer della mappa
  
  map.getLayers().forEach(function (layer) {
    if (layer instanceof ol.layer.Group) {
      getAllFeaturesInLayerGroup(layer, allFeaturesList);
    }
    else if (layer.getSource && layer.getSource() instanceof ol.source.Vector) {
      var features = layer.getSource().getFeatures();
      features.forEach(function (feature) {
        allFeaturesList.push(feature);
      });
    }
  });

  // filtra la collezione di feature sulla base della funzione di filtro
  var filteredFeatures = allFeaturesList.filter(
    function (feature) {
      
      if (feature.values_.idsource){
        var id = feature.values_.idsource;
        if (id.indexOf(this.IDsource) > -1) {
          return true;
        } else {
          return false;
        }
      }
    }, { IDsource }  
    );
  allFeaturesList = [];

  //1.ripristinare stili delle features vecchie
  if (previouslySelected.length > 0) {
    for (i = 0; i < previouslySelected.length; i++) {
      previouslySelected[i].setStyle(stylesSelected[i]);
    }

  }
  previouslySelected = [];
  stylesSelected = [];

  //2.prendo gli stili delle features nuove
  previouslySelected = filteredFeatures;
  for (i = 0; i < previouslySelected.length; i++) {
    var style = previouslySelected[i].getStyle();
    stylesSelected.push(style);
  }

  //3. cambio colore delle features selezionate
  for (i = 0; i < filteredFeatures.length; i++) {
    filteredFeatures[i].setStyle(selected_style);
  }

}

function checkVisibleLayerAndZoom(IDsource, val_zoom) {

  //per IDsource trovo le coordinate del centroide centroidX e centroidY
  var SOURCEARR = new Array();
  carica_mapper(SOURCEARR);
  for (i = 0; i < 1000; i++) {
    if (SOURCEARR[0][i] == IDsource) {
      var centroidX = (SOURCEARR[1][i]);
      var centroidY = (SOURCEARR[2][i]);
      var MaxX = (SOURCEARR[3][i]);
      var MaxY = (SOURCEARR[4][i]);
      var MinX = (SOURCEARR[5][i]);
      var MinY = (SOURCEARR[6][i]);
      var Polygon = (SOURCEARR[7][i]);
    };
  };


  console.log("************")
  console.log(centroidX)
  console.log(centroidY)

  if (centroidX != null && centroidX != "" && centroidY != null && centroidY != "") {
    var zoomList = [];
    if (val_zoom == null) {
      map.getLayers().forEach(function (layer) {

        //controllo se il layer è visibile
        if (layer.values_.name == "ISS") {
          zoomList[0] = layer.state_.visible;
        } else if (layer.values_.name == "CSS") {
          zoomList[1] = layer.state_.visible;
        } else if (layer.values_.name == "DSS") {
          zoomList[2] = layer.state_.visible;
        } else if (layer.values_.name == "SDS") {
          zoomList[3] = layer.state_.visible;
        }


        //calcolo il valore di zoom per la feature in questione (IDsource)
        if (layer instanceof ol.layer.Group) {

          if (layer.values_.name == "CSS" || layer.values_.name == "SDS") {


            allFeaturesList = [];
            getAllFeaturesInLayerGroup(layer, allFeaturesList);

            for (i = 0, ii = allFeaturesList.length; i < ii; ++i) {

              if (allFeaturesList[i].id_ != "csstop") {
                var id = allFeaturesList[i].values_.idsource;
                if (id.indexOf(IDsource) > -1) {

                  val_zoom = zoomFromBoundingBox(allFeaturesList[i]);
                  return;
                }
              }
            }
          }
        }
        else if (layer.getSource && layer.getSource() instanceof ol.source.Vector) {
          if (layer.values_.name == "ISS" || layer.values_.name == "DSS") {

            var features = layer.getSource().getFeatures();
            features.forEach(function (feature) {
              var id = feature.values_.idsource;
              if (id.indexOf(IDsource) > -1) {
                val_zoom = zoomFromBoundingBox(feature);
                return;
              }
            });
          }

        }

      });
    }

    if (IDsource.indexOf("IS") !== -1) {
      zoomVisible = zoomList[0];
    } else if (IDsource.indexOf("CS") !== -1) {
      zoomVisible = zoomList[1];
    } else if (IDsource.indexOf("DS") !== -1) {
      zoomVisible = zoomList[2];
    } else if (IDsource.indexOf("SD") !== -1) {
      zoomVisible = zoomList[3];
    }


    if (zoomVisible == true || zoomVisible == "true") {
      var center = ol.proj.fromLonLat([centroidX, centroidY]);
      map.getView().setCenter(center);
      map.getView().setZoom(val_zoom);
    } else if (zoomVisible == false || zoomVisible == "false") {
      $("#warningContent").load("html/alert.html", function () {
        dragElement(document.getElementById("warningContent"));
        document.getElementById("warningContent").style.display = "block";
      });
    }
  }




}


function zoomFromBoundingBox(feature){
  var extent = feature.getGeometry().getExtent();
  var size = ol.extent.getSize(extent);
  var maxDimension = Math.max(size[0], size[1]);
  var zoom = map.getView().getZoomForResolution(maxDimension / map.getSize()[0]);
  zoom = (zoom * 95) / 100;

  return zoom;

}

function generalInfo(sTypeUrl, sIDsource) {
  
  var line = [];
  var i;
  $.ajaxSetup({
    'beforeSend': function (xhr) {
      xhr.overrideMimeType('text/html; charset=ISO-8859-1');
    },
  });

  if (sTypeUrl == "CSS" || sTypeUrl == "CS") {
    sTypeUrl = "CSSTOP";
  } 
  $.get('data/' + sTypeUrl + '4web.MID', function (data) {

    var Splitdata = data.split(/\r?\n/);
    for (var i = 0; i < Splitdata.length; i++) {

      line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
      var sID = line[i][0];
      sID = line[i][0].replace(/"/g, '');
      sType = sTypeUrl.substring(0, 2)

      if (sID == sIDsource) {

        document.getElementById("VALUEELEMENT_IDSource").innerHTML = sIDsource;
        document.getElementById("VALUEELEMENT_SourceName").innerHTML = line[i][1].replace(/"/g, '');

        if (sType == 'IS') {
          ASSISS = "data/Assign_ISS.txt"
          Assign_ISS_parser_mapper(ASSISS, sIDsource);
          paramISS(line, i);
          createActiveF(sIDsource, sType);
        }

        if (sType == 'CS') {
          ASSCSS = "data/Assign_ISS.txt"
          Assign_CSS_parser_mapper(ASSCSS, sIDsource);
          paramCSS(line, i);
          createActiveF(sIDsource, sType);
        }
        if (sType == 'DS') {
          //  ASSCSS = "data/Assign_ISS.txt"
          //  Assign_CSS_parser_mapper(ASSCSS, sIDsource);
          paramOfDSS(line, i, sIDsource);
        }
        if (sType == 'SU') {
          ASSCSS = "data/Assign_ISS.txt"
          Assign_SDS_parser_mapper(ASSCSS, sIDsource);
          paramSUBD(line, i);
          createActiveF(sIDsource, sType);
        }


      }
    }
  });
  $.ajaxSetup({
    'beforeSend': function (xhr) {
      xhr.overrideMimeType('text/html; UTF-8');
    },
  });
}


function commimport_mapper(sIDsource) {
  $.get("data/com/" + sIDsource + "COM.txt", function (data) {
    //sostituzione dei caratteri per avere il BOLD e il ritorno a capo in HTML
    data = data.replace(/\*\*\r/g, '</b><br>');

    data = data.replace(/\*\*/g, '');

    data = data.replace(/\r/g, '<br>');


    document.getElementById("tab2").innerHTML = data;

  });

}

function pictparser_mapper(pict, sIDsource) {
  $.ajaxSetup({
    'beforeSend': function (xhr) {
      xhr.overrideMimeType('text/plain; charset=utf-8');
    },
  });
  var sPict;
  sPict = " ";
  $.get(pict, function (data) {
    var line = [];
    data = data.replace(/\,\,/g, ',"",""');	//sostituzione dei caratteri per l'attuale formato del "txt" (campi vuoti) che crea problemi al parser JSON
    // alert(data);
    var pictList = data.split(/\r?\n/);

    $.ajaxSetup({
      'beforeSend': function (xhr) {
        xhr.overrideMimeType('text/plain; charset=utf-8');
      },
    });

    $.get("data/MainTable.txt", function (data1) {
      var line1 = [];
      data1 = data1.replace(/\,\,/g, ',"",""');	//sostituzione dei caratteri per l'attuale formato del "txt" (campi vuoti) che crea problemi al parser JSON
      var didaList = data1.split(/\r?\n/);

      var n = 1
      for (var i = 0; i < pictList.length; i++) {
        line[i] = pictList[i].split('\t');

        if (line[i][1] == sIDsource) {

          //il primo valore è il nome del file
          var s1Pict = line[i][0]

          //chiamo la funzione che legge MainTable.csv
          for (var k = 0; k < didaList.length; k++) {
            line1[k] = didaList[k].split('\t'); //split della "," che non considera quelle tra doppi apici
            if (line1[k][0] == s1Pict) {
              //il primo valore è il titolo della picture
              var sTIT = line1[k][1]
            }
          }

          //procedura per attribuire alla variabile classe il pari o dispari
          var classRow;
          if (n % 2 == 1) { classRow = "EVEN" } else { classRow = "ODD" };

          sPict = sPict + '<tr class="' + classRow + '" id="images_emb_ROW_0_1"><td class="stretto"><img src="img/lens.gif" class="pointer" onclick="showPicture(\'' + s1Pict + '\', \'  ' + sIDsource + '   \')" alt="Details"></td><td class="' + classRow + '">' + sTIT + '</td></tr>';
          n = n + 1
        };
      }
      document.getElementById("tab3").innerHTML = '<br><table class="MAINTABLE"><tbody>' + sPict + '</tbody></table>';
    });
  })
}

function refparser_mapper(ref, sIDsource) {
  $.ajaxSetup({
    'beforeSend': function (xhr) {
      xhr.overrideMimeType('text/html; charset=ISO-8859-1');
    },
  });
  var sref;
  sref = " ";
  $.get(ref, function (data) {
    var line = [];
    var refList = data.split(/\r?\n/);
    var n = 1
    // alert(sIDsource);
    for (var i = 0; i < refList.length; i++) {
      line[i] = refList[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/);
      var sID = line[i][0].replace(/"/g, '');

      if (sID == sIDsource) {
        // alert(line[i][3] )
        sR = line[i][1] + ' (' + line[i][2] + '), ' + line[i][3] + ' ' + line[i][4]
        sR = sR.replace(/"/g, '');
        //procedura per attribuire alla variabile classe il pari o dispari
        var classRow = "EVEN";
        // if (n %2 == 1) {classRow = "EVEN"} else {classRow = "ODD"};

        sref = sref + '<tr class="' + classRow + '" id="images_emb_ROW_0_1"><td class="' + classRow + '">' + sR + '<hr></td></tr>';

        n = n + 1
      };
    }

    document.getElementById("tab4").innerHTML = '<br><table class="MAINTABLE" id="tabRef"><tbody><tr id="sepRef"><th data-sort-default></th>' + sref + '</tbody></table>';
    new Tablesort(document.getElementById('tabRef'), {
      descending: false
    });
  });

}

function Assign_ISS_parser_mapper(ASSISS, sIDsource) {

  var val_zoom = null;
  checkVisibleLayerAndZoom(sIDsource, val_zoom);

  $.get(ASSISS, function (data) {
    var line = [];
    var ASSISSList = data.split(/\r?\n/);
    // alert(ASSISSList);
    for (var i = 0; i < ASSISSList.length; i++) {
      line[i] = JSON.parse('[' + ASSISSList[i] + ']');
      
      if (line[i][0] == sIDsource) {
        if (line[i][1] == "") {
          sTypeUrl = line[i][2].substring(2, 4) + "S";
          
          document.getElementById("VALUEELEMENT_relatedSourcesList").innerHTML = '<a onclick=\'detail("' + line[i][2] + '", "' + sTypeUrl + '", "' + val_zoom + '")\'>' + line[i][2] + '</a>';
        }
        else {
          
          sTypeUrl = line[i][1].substring(2, 4) + "S";
          document.getElementById("VALUEELEMENT_relatedSourcesList").innerHTML = '<a onclick=\'detail("' + line[i][1] + '", "' + sTypeUrl + '",  "' + val_zoom + '")\'>' + line[i][1] + '</a>';
        }
      }
    }
  });
}

function Assign_CSS_parser_mapper(ASSCSS, sIDsource) {

  var val_zoom = null;
  checkVisibleLayerAndZoom(sIDsource, val_zoom);
  $.get(ASSCSS, function (data) {
    var line = [];
    var ASSCSSList = data.split(/\r?\n/);
    var sASSCSS = "";
    for (var i = 0; i < ASSCSSList.length; i++) {
      line[i] = JSON.parse('[' + ASSCSSList[i] + ']');
      if (line[i][1] == sIDsource) {
        sTypeUrl = line[i][0].substring(2, 4) + "S";
        sASSCSS = sASSCSS + ' <a onclick=\'detail("' + line[i][0] + '", "' + sTypeUrl + '",  "' + val_zoom + '")\'>' + line[i][0] + '</a>'
      }
    }
    document.getElementById("VALUEELEMENT_relatedSourcesList").innerHTML = sASSCSS;
  });
}

function Assign_SDS_parser_mapper(ASSCSS, sIDsource) {

  var val_zoom = null;
  checkVisibleLayerAndZoom(sIDsource, val_zoom);
  $.get(ASSCSS, function (data) {
    var line = [];
    var ASSCSSList = data.split(/\r?\n/);
    var sASSCSS = "";
    for (var i = 0; i < ASSCSSList.length; i++) {
      line[i] = JSON.parse('[' + ASSCSSList[i] + ']');
      if (line[i][1] == sIDsource) {
        sTypeUrl = line[i][0].substring(2, 4) + "S";
        sASSCSS = sASSCSS + ' <a onclick=\'detail("' + line[i][0] + '", "' + sTypeUrl + '",  "' + val_zoom + '")\'>' + line[i][0] + '</a>'
      }
    }
  });
}

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

function closeDiv(divElement){
  document.getElementById(divElement).style.display = "none";
}

//rita
function nextImage(idSource, s1Pict, sType ){

  idSource = idSource.trim();


  $("#pictureContent").load("imgContent.php?idSource=" + idSource +"&img=" + s1Pict + "&sType=" + sType, function () {
    dragElement(document.getElementById("pictureContent"));
    document.getElementById("pictureContent").style.display = "block";
  });

 
  
  
}

function previousImage(idSource, s1Pict, sType) {

  idSource = idSource.trim();


  $("#pictureContent").load("imgContent.php?idSource=" + idSource +"&img=" + s1Pict + "&sType=" + sType, function () {
    dragElement(document.getElementById("pictureContent"));
    document.getElementById("pictureContent").style.display = "block";
  });


  
}

function getAllFeaturesInLayerGroup(layerGroup, allFeaturesList) {
  layerGroup.getLayers().forEach(function (layer) {
    if (layer instanceof ol.layer.Group) {
      getAllFeaturesInLayerGroup(layer, allFeaturesList);
    }
    else if (layer.getSource && layer.getSource() instanceof ol.source.Vector) {
      var features = layer.getSource().getFeatures();
      features.forEach(function (feature) {
        allFeaturesList.push(feature);
      });
    }
  });

}



function csvparserNew(csv, sIDsource) {
  $.ajaxSetup({
    'beforeSend': function (xhr) {
      xhr.overrideMimeType('text/html; charset=ISO-8859-1');
    },
  });
  $.get(csv, function (data) {

    var line = [];
    var CSVList = data.split(/\r?\n/);

    for (var i = 0; i < CSVList.length; i++) {
      line[i] = JSON.parse('[' + CSVList[i] + ']');
      if (line[i][0] == sIDsource) {
        document.getElementById("VALUEELEMENT_compilers").innerHTML = line[i][1];
        document.getElementById("VALUEELEMENT_contributors").innerHTML = line[i][2];
        document.getElementById("VALUEELEMENT_affiliations").innerHTML = line[i][3];
      }
    }
  });

}

map.on('postrender', function () {
  
});


function leggiFile(nomeFile) {
  return new Promise((resolve, reject) => {
    fs.readFile(nomeFile, 'utf8', (err, data) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(data);
    });
  });
}

function trovaVariabili(fileContent, stringToFind) {
  const righe = fileContent.split('\n');
  let valoreVariabileSeconda = null;

  for (let i = 0; i < righe.length; i++) {
    const riga = righe[i].trim();

    if (riga.startsWith(stringToFind)) {
      const variabili = riga.split('\t');

      if (variabili.length >= 2) {
        const nomeVariabileSeconda = variabili[1].trim();

        for (let j = i + 1; j < righe.length; j++) {
          const rigaSuccessiva = righe[j].trim();

          if (rigaSuccessiva.startsWith(nomeVariabileSeconda)) {
            const valoreVariabileSeconda = rigaSuccessiva.split('=')[1].trim();
            return valoreVariabileSeconda;
          }
        }
      }
    }
  }

  return valoreVariabileSeconda;
}

function paramOfDSS(line, i, sIDsource) {
  sDate1 = line[i][3].substring(6, 8) + "/" + line[i][3].substring(4, 6) + "/" + line[i][3].substring(0, 4)
  sDate2 = line[i][4].substring(6, 8) + "/" + line[i][4].substring(4, 6) + "/" + line[i][4].substring(0, 4)
  document.getElementById("VALUEELEMENT_created").innerHTML = sDate1;
  document.getElementById("VALUEELEMENT_latestupdate").innerHTML = sDate2;

  $.get('data/DSSA4web.MID', function (data) {


    var Splitdata = data.split(/\r?\n/);

    for (var i = 0; i < Splitdata.length; i++) {

      line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/);
      var sID = line[i][0];
      sID = line[i][0].replace(/"/g, '');
      if (sID == sIDsource) {
        for (var k = 1; k < 38; k++) {
          sk = k.toString();
          document.getElementById(sk).innerHTML = line[i][k].replace(/"/g, '');
        }
      }

    }
  });
};


function createActiveF(sIDsource, sType) {

  

  // ACTIVE FAULTS
  $.get('data/AFaults.MID', function (data) {
    var line = [];
    var nRow = 0;
    var Splitdata = data.split(/\r?\n/);
 
    for (var i = 0; i < Splitdata.length - 1; i++) {

      line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
      // alert(line[1]);
      // line[i] = line[i].replace(/"/g,'');

      switch (sType) {
        case "IS":
          var sID = line[i][0].replace(/"/g, '');
          break;
        case "CS":
          var sID = line[i][1].replace(/"/g, '');
          break;
        case "SU":
          var sID = line[i][2].replace(/"/g, '');
          break;
      }
      // alert(sID);

      if (sID == sIDsource) {
        nRow = nRow + 1;
        var sIDA = line[i][3].replace(/"/g, '');
        var sName = line[i][5].replace(/"/g, '');
        var sRef = line[i][28].replace(/"/g, '');
        var sRow = document.getElementById("AFAcont").innerHTML;
        if (nRow & 1) {
          document.getElementById("AFAcont").innerHTML = sRow + '<tr class="ODD">	<td class="ODD">' + sIDA + '</td>	<td class="ODD">' + sName + '</td><td class="ODD">' + sRef + '</tr>'
        }
        else {
          document.getElementById("AFAcont").innerHTML = sRow + '<tr class="EVEN">	<td class="EVEN">' + sIDA + '</td>	<td class="EVEN">' + sName + '</td><td class="EVEN">' + sRef + '</tr>'
        }


      }
    }
    if (nRow == 0) { document.getElementById("AFAcont").innerHTML = "" }
  });


  // ACTIVE FOLDS
  $.get('data/AFolds.MID', function (data) {
    var line = [];
    var nRow = 0;
    var Splitdata = data.split(/\r?\n/);
    // alert(Splitdata.length);
    for (var i = 0; i < Splitdata.length - 1; i++) {

      line[i] = Splitdata[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/)
      // alert(line[1]);
      // line[i] = line[i].replace(/"/g,'');

      switch (sType) {
        case "IS":
          var sID = line[i][0].replace(/"/g, '');
          break;
        case "CS":
          var sID = line[i][1].replace(/"/g, '');
          break;
        case "SU":
          var sID = line[i][2].replace(/"/g, '');
          break;
      }
      // alert(sID);
      if (sID == sIDsource) {
        nRow = nRow + 1;
        var sIDA = line[i][3].replace(/"/g, '');
        var sName = line[i][5].replace(/"/g, '');
        var sRef = line[i][28].replace(/"/g, '');
        var sRow = document.getElementById("AFOcont").innerHTML;
        if (nRow & 1) {
          document.getElementById("AFOcont").innerHTML = sRow + '<tr class="ODD">	<td class="ODD">' + sIDA + '</td>	<td class="ODD">' + sName + '</td><td class="ODD">' + sRef + '</tr>'
        }
        else {
          document.getElementById("AFOcont").innerHTML = sRow + '<tr class="EVEN">	<td class="EVEN">' + sIDA + '</td>	<td class="EVEN">' + sName + '</td><td class="EVEN">' + sRef + '</tr>'
        }


      }
    }
    if (nRow == 0) { document.getElementById("AFOcont").innerHTML = "" }
  });


  // Return array of string values, or NULL if CSV string not well formed.


}





//GEOLOCATION
// Inizializza un layer per visualizzare la posizione dell'utente
const userLocationLayer = new ol.layer.Vector({
  source: new ol.source.Vector(),
  displayInLayerSwitcher: false,
  style: new ol.style.Style({
    image: new ol.style.Circle({
      radius: 6,
      fill: new ol.style.Fill({ color: 'blue' }),
      stroke: new ol.style.Stroke({ color: 'white', width: 2 }),
    }),
  }),
});

map.addLayer(userLocationLayer);

// Aggiungi un gestore di eventi al pulsante
document.getElementById('geolocation-button').addEventListener('click', () => {
  if ('geolocation' in navigator) {
    // Richiedi la posizione dell'utente
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;

        // Crea un punto sulla mappa alla posizione dell'utente
        const userLocation = new ol.Feature({
          geometry: new ol.geom.Point(ol.proj.fromLonLat([longitude, latitude])),
        });

        // Aggiungi il punto al layer della posizione dell'utente
        userLocationLayer.getSource().clear();
        userLocationLayer.getSource().addFeature(userLocation);

        // Centra la mappa sulla posizione dell'utente
        map.getView().setCenter(ol.proj.fromLonLat([longitude, latitude]));
        map.getView().setZoom(15); // Puoi regolare il livello di zoom desiderato

        // Puoi anche fare altre azioni qui, come mostrare un popup con la posizione
      },
      (error) => {
        console.error('Errore nella geolocalizzazione:', error);
      }
    );
  } else {
    alert('Geolocalizzazione non supportata dal tuo browser.');
  }
});


