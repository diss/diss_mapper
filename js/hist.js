$(function() {
	$('#HISTeqIcon').click(function(event) {
		$('#HISTclose').show();
		$('#HISTeqMenu').show();
	});
	$('#HISTclose').click(function(event) {
		$('#HISTeqMenu').hide();
	});

	// fillHISTdiv()

});

var CPTI_WMS = new ol.source.TileWMS({
	"url": "https://emidius.mi.ingv.it/services/italy/wms/?service=WMS",
	"projection": "EPSG:3857",
	"crossOrigin": 'anonymous',
	"params": {
		"LAYERS": "CPTI_current",
		"FORMAT": "image/png",
		"VERSION": "1.3.0",
		"STYLES": "italy:CPTI15_squares_colors"
	},
	"attributions": [
		"<a href='https://emidius.mi.ingv.it/>&copy; INGV</a>"
	]
})

var CPTI = new ol.layer.Tile ({
	"title": "CPTI 15 (current version)",
	"visible": true,
	"opacity": 1,
	"source": CPTI_WMS,
	"zIndex": 10
});

var CFTI_WMS = new ol.source.TileWMS({
	"url": "https://services-storing.ingv.it/geoserver/CFTI/ows?service=WMS",
	"projection": "EPSG:3857",
	"crossOrigin": 'anonymous',
	"params": {
		"LAYERS": "CFTI_formatoCPTI",
		"FORMAT": "image/png",
		"VERSION": "1.3.0",
		"STYLES": "CPTI4CFTI",
		"CQL_FILTER": "rel<>'F'"   //escludo dalla visualizzazione i falsi
	},
	"attributions": [
		"<a href='https://storing.ingv.it/cfti/cfti5/>&copy; INGV</a>"
	]
})

var CFTI = new ol.layer.Tile ({
	"title": "CFTI5Med",
	"visible": true,
	"opacity": 1,
	"source": CFTI_WMS,
	"zIndex": 10
});


 map.on('singleclick', function (evt) {
	 
// function testWMS(evt) {
  if (document.getElementById("CPTI").checked || document.getElementById("CFTI").checked) {
	  // document.getElementById('layercontrol').innerHTML = '';
	  var viewResolution = /** @type {number} */ (view.getResolution());
	  // alert(viewResolution)
	  var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857','EPSG:4326')
	  var lon = lonlat[0].toPrecision(7);
	  var lat = lonlat[1].toPrecision(7);
	  
		if (document.getElementById("CPTI").checked) {
		  var url = CPTI_WMS.getFeatureInfoUrl(
			evt.coordinate,
			viewResolution,
			'EPSG:3857',
			{'INFO_FORMAT': 'application/json',
			'STYLES:italy':'CPTI15_squares_colors',
			// 'buffer':'30',
			 'feature_count': 50}  //IMPORTANTE: di default richiama solo un record!
		  );
		}
		if (document.getElementById("CFTI").checked) {	  
		  var url = CFTI_WMS.getFeatureInfoUrl(
			evt.coordinate,
			viewResolution,
			'EPSG:3857',
			{'INFO_FORMAT': 'application/json',
			// 'STYLES':'CPTI4CFTI',
			// 'buffer':'30',
			 'CQL_FILTER': "rel<>'F'",
			 'feature_count': 50}  //IMPORTANTE: di default richiama solo un record!
		  );
		}
	  if (url) {
		  // window.prompt("URL",url)
			$.ajax({
				type: "POST",
				url: 'php/load_data.php',
				data: {WMSurl:url},
				success: function(result){
					// alert(result)
						// document.getElementById('layercontrol').innerHTML = '<br><b>lat:</b> ' + lat + ' - <b>lon:</b> ' + lon + '<br><br>' + html;
					var myObj = JSON.parse(result);
					EpiText ='';
					numret = myObj["numberReturned"]
					if (numret > 0) {

						//per eventuale collegamento con CFTI (DA SVILUPPARE)
						// csv = "CPTI-CFTI.txt"
		
						var EQ_corr = []

						container.style.backgroundColor = "#FFECD9"
						var n = myObj["features"].length;
						for (var i = 0; i < n; i++) {
							var Year = myObj["features"][i]["properties"]["Year"];
							var Mo = myObj["features"][i]["properties"]["Mo"];						
							var Da = myObj["features"][i]["properties"]["Da"];
								if (Mo) {
									if (Da) {
										var Data = Year + "-" + ('0' + Mo).slice(-2) + "-" + ('0' + Da).slice(-2)
									} else {
										var Data = Year + "-" + ('0' + Mo).slice(-2) + "-" + "??"
									}	
								} else {
									var Year
								}	
							
							var Ho = myObj["features"][i]["properties"]["Ho"];	
							var Mi = myObj["features"][i]["properties"]["Mi"];	
							var Se = myObj["features"][i]["properties"]["Se"];
							if (Ho) {
								if (Mi) {
									if (Se) {
										var Time = ('0' + Ho).slice(-2)  + ":" + ('0' + Mi).slice(-2)  + ":" + ('0' + Se).slice(-2) 
									} else {
										var Time = ('0' + Ho).slice(-2)  + ":" + ('0' + Mi).slice(-2)
									}	
								} else {
									var Time = ('0' + Ho).slice(-2) + ":" + "??"
								}									
							} else {
								Time = '';
							}
							
							var LatC = Math.round(myObj["features"][i]["properties"]["LatDef"]*1000) / 1000;	
							var LonC = Math.round(myObj["features"][i]["properties"]["LonDef"]*1000) / 1000;	;	
							var EpicentralArea = myObj["features"][i]["properties"]["EpicentralArea"];
							var IoDef = myObj["features"][i]["properties"]["IoDef"];
							if (IoDef) {
							} else{
								IoDef = "-"
							}
							var MwDef = myObj["features"][i]["properties"]["MwDef"];
							if (MwDef) {
							} else{
								MwDef = "-"
							}
							var EqID = myObj["features"][i]["properties"]["EqID"];
							var URL = myObj["features"][i]["properties"]["URL"];			
							if (document.getElementById("CPTI").checked) {
								var sLink = "<a href='" + URL + "' target='_blank'>CPTI link</a>"							
							} else { //se è CFTI
								var sLink = "<a href='https://storing.ingv.it/cfti/cfti5/quake.php?" + myObj["features"][i]["properties"]["N"] + "IT' target='_blank'>CFTI link</a>"		
							}
							EpiText = EpiText + "<b><font color = 'red'>Earthquake info</font></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sLink + "<br><br>" + "<b>Mw: " + MwDef + " - " + EpicentralArea + "</b><br>" + "Time (UTC): <b>" + Data + "</b> " + Time + "</b><br>Lat: " + LatC + ", Lon: " + LonC + "<b> - Epicentral Intensity: " + IoDef + "</b><div align='right' /><i>Further info in </i><a href='https://emidius.mi.ingv.it/ASMI/event/" + EqID +"' target='_blank'>ASMI</a></div><hr>";

						}
						content.innerHTML = EpiText
						overlay.setPosition(evt.coordinate);
					}
				}
				
		  });
	  }
  };
});

function ShowEPI_CFTI() {
	document.getElementById('clearHIST').innerHTML = '<abbr title="Remove historical seismicity from map"><img src="./images/clearMap.png" width = "25px" ></abbr>';
	map.removeLayer(CPTI);
	map.addLayer(CFTI);
	document.getElementById("ztopE").disabled = false;
	document.getElementById("zbotE").disabled = false;
};

function ShowEPI_CPTI() {
	document.getElementById('clearHIST').innerHTML = '<abbr title="Remove historical seismicity from map"><img src="./images/clearMap.png" width = "25px" ></abbr>';
	map.removeLayer(CFTI);
	map.addLayer(CPTI);
	document.getElementById("ztopE").disabled = false;
	document.getElementById("zbotE").disabled = false;
}

function HideEPI() {
	map.removeLayer(CFTI);
	map.removeLayer(CPTI);
	document.getElementById("CPTI").checked = false;
	document.getElementById("CFTI").checked = false;
	document.getElementById('clearHIST').innerHTML = '';
	document.getElementById("ztopE").disabled = true;
	document.getElementById("zbotE").disabled = true;
}
function SetZIndexEPI(topbot) {
	if (topbot=='top') {
		CFTI.setZIndex(10);
		CPTI.setZIndex(10);
	} else {
		CPTI.setZIndex(0);
		CFTI.setZIndex(0);
	}
}

function csvparserEQ(csv){
	$.get(csv, function(data){
		var line =[];
		var CSVList = data.split('\r');
		for (var i = 0; i < CSVList.length; i++) {
			line[i] = CSVList.split('\r');
		}
		return line;
	});
}