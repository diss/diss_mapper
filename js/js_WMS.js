var sTypeUrl;
	
function  test_type() {
	var url = window.location.href;
	var nchar = url.length - 3;
	var sType = url.substr(nchar,3);
	
	switch (sType) {
	case "ISS":
		sTypeUrl = "iss"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "_SELECTEDTAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Individual Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="http://diss.rm.ingv.it/diss/index.php/tutorial/15-individual-seismogenic-sources" title="What is a Individual Seismogenic Source?" target="_blank"><img src="img/Help2.png"/></a>';
		
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';
		
		break;
		
	case "CSS":
		sTypeUrl = "csspln"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "_SELECTEDTAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Composite Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="http://diss.rm.ingv.it/diss/index.php/tutorial/15-individual-seismogenic-sources" title="What is a Individual Seismogenic Source?" target="_blank"><img src="img/Help2.png"/></a>';
		
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';
				
		break;
		
	case "DSS":
		sTypeUrl = "dss"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "_SELECTEDTAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "Debated Seismogenic Sources";
		document.getElementById("INFO_ICON").innerHTML = '<a href="http://diss.rm.ingv.it/diss/index.php/tutorial/15-individual-seismogenic-sources" title="What is a Individual Seismogenic Source?" target="_blank"><img src="img/Help2.png"/></a>';
		
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';
				
		break;
		
	case "UBD":
		sTypeUrl = "subdzon"
		document.getElementById("TAB1").className = "TAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "_SELECTEDTAB";
		
		document.getElementById("TITLE").innerHTML = "Subduction Zones";
		document.getElementById("INFO_ICON").innerHTML = '<a href="http://diss.rm.ingv.it/diss/index.php/tutorial/15-individual-seismogenic-sources" title="What is a Individual Seismogenic Source?" target="_blank"><img src="img/Help2.png"/></a>';
				
		document.getElementById("TABCONT").innerHTML = '									<table class="MAINTABLE"><tr class="EVEN"><th scope="col" class="stretto">DISS&ndash;ID</th><th scope="col" class="largo">Source Name</th><th scope="col" class="stretto">Details</th><th scope="col" class="stretto">Map</th></tr></table> <div class="DIVINTERNO"><table class="MAINTABLE" id ="TABINT"></table></div>';	
		
		break;
		
	default: 
		document.getElementById("TAB1").className = "_SELECTEDTAB";
		document.getElementById("TAB2").className = "TAB";
		document.getElementById("TAB3").className = "TAB";
		document.getElementById("TAB4").className = "TAB";
		document.getElementById("TAB5").className = "TAB";
		
		document.getElementById("TITLE").innerHTML = "DISS Data Tables - Version 3.2.1";
		document.getElementById("INFO_ICON").innerHTML = '';
		
		$(document).ready(function() {
			$("#mainDIV").load("html/home.html");
		});		
	
		break;
	}
	xmlreading();
}

function xmlreading() {
	// prompt("testo","http://services.seismofaults.eu/geoserver/DISS_current/ows?service=WFS&version=1.0.0&request=GetFeature&format_options=CHARSET:UTF-8&typeName=DISS_current:" + sTypeUrl + "321")
	WMSurl = "http://services.seismofaults.eu/geoserver/DISS_current/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=DISS_current:" + sTypeUrl + "321"; 
   	     
	 $.ajax({
        type: "POST",
        url: 'php/load_data.php',
		data: {WMSurl:WMSurl},
        success: function(xml) {
          xmlParser(xml);
        },
      });
}


function xmlParser(xml) {
	var idsource = jQuery(xml).find("DISS_current\\:idsource");
	var sName = jQuery(xml).find("DISS_current\\:sourcename");
	var maxMag = jQuery(xml).find("DISS_current\\:maxmag");
	var details = jQuery(xml).find("DISS_current\\:idsource");
	$(".num").append(idsource.length);
	for (var i = 0; i < idsource.length; i++) {
		if ( i % 2 == 0 ) {
			var tr = '<tr class="EVEN">' 
		} else {
			var tr = '<tr class="ODD">' 
		}
		var IDs = jQuery(idsource[i]).text();
		var NAMEs = jQuery(sName[i]).text();
		
		$("#TABINT").append(
		tr + 
		'<td class="stretto">' + IDs + '</td>' +
		'<td class="largo">' + NAMEs + '</td>' +
		'<td class="stretto"><a href="sources.php?' + IDs + '" ><img src="img/lens.gif" alt="Details" ></a></td>' +
		'<td class="stretto"><a href="dissmap.php?ll=' + IDs + '" target="_blank"><img class="pointer" src="img/map.gif" alt="map" ></a></td>' +
		'</tr>'
		)
	}
}