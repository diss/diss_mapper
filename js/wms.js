var CGT100 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "Mosaici_UTM-WGS84_100k_Fisica.ecw&"; //WMS layers
						url += "&STYLES=default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					 //minZoom: 10,
					 //maxZoom: 14,
					opacity: 1.0,
                    
                });

http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?VERSION=1.1.1&REQUEST=GetMap&SRS=EPSG:4326&WIDTH=512&HEIGHT=512&LAYERS=Mosaici_UTM-WGS84_100k_Fisica.ecw&STYLES=default&TRANSPARENT=TRUE&FORMAT=image/png

var IGM25 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "Mosaici_UTM-WGS84_IGM25k_WGS84.ecw"; //WMS layers
						url += "&STYLES=default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });

var IGM100 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "Mosaici_UTM-WGS84_IGM100k_WGS84.ecw"; //WMS layers
						url += "&STYLES=default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });
				

var IGM200 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "Mosaici_UTM-WGS84_IGM200k_WGS84.ecw"; //WMS layers
						url += "&STYLES=default"; 
                       
                        url += "&TRANSPARENT=true";
						url += "&OPACITY=0.9";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });
				
var ORTO2013 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "Mosaici_UTM-WGS84_Ortofoto2013.ecw"; //WMS layers
						url += "&STYLES=default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });
				
var ORTO2009 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "Mosaici_UTM-WGS84_Orto2009_AGEA.ecw"; //WMS layers
						url += "&STYLES=default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });
				
var ORTO1997 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geocatalogo.regione.abruzzo.it/erdas-iws/ogc/wms/?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "Mosaici_UTM-WGS84_1997_AGEA.ecw"; //WMS layers
						url += "&STYLES=default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });				
				
				
var GEO50 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        //var url = "http://sgi1.isprambiente.it/arcgis/services/servizi/geologia25k/MapServer/WMSServer?";
						var url = "http://geoservices.isprambiente.it/arcgis/services/Geologia/geologia_italia_50k/ImageServer/WMSServer?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						//url += "&LAYERS=" + "0,1,2,3,4"; //WMS layers
						url += "&LAYERS=" + "geologia_italia_50k"; //WMS layers
						url += "&STYLES=default";   //,default,default,default,default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });
				
				
var GEO100 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geoservices.isprambiente.it/arcgis/services/Geologia/geologia_italia_100k/ImageServer/WMSServer?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "geologia_italia_100k"; //WMS layers
						url += "&STYLES=default";   //,default,default,default,default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });				
				
				
var GEOSTR500 = new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        var proj = map.getProjection();
                        var zfactor = Math.pow(2, zoom);
                        // get Long Lat coordinates
                        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 1024 / zfactor, coord.y * 1024 / zfactor));
                        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 1024 / zfactor, (coord.y + 1) * 1024 / zfactor));

                        //corrections for the slight shift of the WMS LAYER
                        var deltaX = 0.000;
                        var deltaY = 0.0000;

                        //create the Bounding box string
                        var bbox =     (top.lng() + deltaX) + "," +
    	                               (bot.lat() + deltaY) + "," +
    	                               (bot.lng() + deltaX) + "," +
    	                               (top.lat() + deltaY);

                        //base WMS URL 

                        var url = "http://geoservices.isprambiente.it/arcgis/services/Geologia/strutturale_italia_500k/ImageServer/WMSServer?";
                        url += "&VERSION=1.1.1";  //WMS version  
					    url += "&REQUEST=GetMap"; //WMS operation
						url += "&SRS=EPSG:4326";     //set WGS84 
						url += "&WIDTH=1024";         //tile size in google
                        url += "&HEIGHT=1024";
						url += "&LAYERS=" + "strutturale_italia_500k"; //WMS layers
						url += "&STYLES=default";   //,default,default,default,default"; 
                        url += "&TRANSPARENT=false";
					    url += "&OPACITY=0.1";
                        url += "&FORMAT=image/png" ; //WMS format
                       
                       

                        url += "&BBOX=" + bbox;      // set bounding box

                         return url;                // return URL for the tile
						

						
                    },
                    tileSize: new google.maps.Size(1024, 1024),
					// minZoom: 14,
					// maxZoom: 16,
					opacity: 0.9,
                    
                });						
		
				
				