var iss_style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgb(250,190,10, 0)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgb(250,190,10)', // Colore del bordo del poligono
    width: 2,
    lineJoin_: 'bevel'
  })
});

var dss_style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(114, 110, 242, 0.8)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgba(123,104,250, 0.7)', // Colore del bordo del poligono
    width: 2 // Larghezza del bordo del poligono
  }),
  // Opzionalmente, puoi aggiungere altri attributi come testo o icone
  text: new ol.style.Text({
    text: '',
    fill: new ol.style.Fill({
      color: '#000' // Colore del testo
    }),
    stroke: new ol.style.Stroke({
      color: '#fff', // Colore del bordo del testo
      width: 2 // Larghezza del bordo del testo
    })
  })
});

var sdsLines_style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(128,128,128, 1)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgba(128,128,128, 1)', // Colore del bordo del poligono
    width: 1 // Larghezza del bordo del poligono
  }),
  // Opzionalmente, puoi aggiungere altri attributi come testo o icone
  text: new ol.style.Text({
    text: '',
    fill: new ol.style.Fill({
      color: '#000' // Colore del testo
    }),
    stroke: new ol.style.Stroke({
      color: '#fff', // Colore del bordo del testo
      width: 2 // Larghezza del bordo del testo
    })
  })
});

var cssTopGray_style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(128,128,128, 0)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgba(128,128,128, 1)', // Colore del bordo del poligono
    width: 2 // Larghezza del bordo del poligono
  }),

});

var cssPlaneForColored_style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(128,128,128, 0.1)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgba(128,128,128, 0)', // Colore del bordo del poligono
    width: 2 // Larghezza del bordo del poligono
  }),

});

var sdsZone_style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(169, 169, 169, 0.25)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgba(35, 35, 35, 0)', // Colore del bordo del poligono
    width: 1,
    lineJoin_: 'bevel'
  })
});

var css_style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(255,140,0, 0.7)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgba(255,140,0, 0.7)', // Colore del bordo del poligono
    width: 2 // Larghezza del bordo del poligono
  }),
  // Opzionalmente, puoi aggiungere altri attributi come testo o icone
  text: new ol.style.Text({
    text: '',
    fill: new ol.style.Fill({
      color: '#000' // Colore del testo
    }),
    stroke: new ol.style.Stroke({
      color: '#fff', // Colore del bordo del testo
      width: 2 // Larghezza del bordo del testo
    })
  })
});

var areaOfRelevanceStyle = function (feature) {

  var position = feature.values_.id;
  
  if (position == "300"){
    color = 'rgba(0,255,0, 1)'
  } else if (position == "200") {
    color = 'rgba(255,255,0, 1)'
  } else if (position == "100") { 
    color = 'rgba(255,0,0, 1)'
  }

  style = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: color,
      width: 1,
      lineJoin: 'bevel',
      lineCap: 'square'
    }),
  });

  return style;
};

var css_style2 = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(255,69,0, 0.7)' // Colore di riempimento del poligono
  }),
  stroke: new ol.style.Stroke({
    color: 'rgba(255,69,0, 0.7)', // Colore del bordo del poligono
    width: 2 // Larghezza del bordo del poligono
  }),
  // Opzionalmente, puoi aggiungere altri attributi come testo o icone
  text: new ol.style.Text({
    text: '',
    fill: new ol.style.Fill({
      color: '#000' // Colore del testo
    }),
    stroke: new ol.style.Stroke({
      color: '#fff', // Colore del bordo del testo
      width: 2 // Larghezza del bordo del testo
    })
  })
});

var selected_style = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: 'rgba(96,96,96, 0.6)',
    width: 1.5
  }),
  fill: new ol.style.Fill({
    color: 'rgba(96,96,96, 0.6)'
  })
})



//rules for sds contour color
// var stylesArray = [];
// for (var depth = 1; depth <= 600; depth++) {
//   var red = 255 - depth;
//   var blue = depth - 3;

//   // Regola il colore in base alla depth
//   var color = 'rgb(' + red + ', 0, ' + blue + ')';

//   // Crea lo stile dinamico e aggiungilo all'array styles
//   stylesArray.push(createDynamicStyle(depth, color));
// }



var sds_style2 = function (feature) {

  var depth = feature.get('depth');

  var depthN = parseFloat(depth);

  depth = Math.floor(depthN);

  var depth_string = String(depth);

  var style = calculateStyle(depth);
  return style;
};

function createDynamicStyle(depth, color) {
  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: color,
      width: 1,
      lineJoin: 'bevel',
      lineCap: 'square'
    }),
    text: new ol.style.Text({
      text: String(depth),
      font: "0px sans-serif",
    })
  });
}


function calculateStyle(depth) {
  const fZmin = 1;
  const fZmax = 600;
  const fZflx = 25;
  const iRGBmin = 0;
  const iRGBmax = 255;
  const iRGBTot = Math.floor(fZmax - fZmin);

  let red = '';
  let green  = '';
  let blue = '';

    // 1 - 25
  if (depth <= fZflx && depth >= fZmin) {
    red = getLinearInterpolatedValue(depth, fZflx, fZmin, iRGBmin, iRGBmax);
    green = 0;
    blue = getLinearInterpolatedValue(depth, fZmin, fZflx, iRGBmin, iRGBmax);
  } 
  // 25 - 600
  else if (depth > fZflx && depth <= fZmax) {
    red = getLinearInterpolatedValue(depth, fZflx + 1, fZmax, iRGBmin, iRGBmax);
    green = getLinearInterpolatedValue(depth, fZflx + 1, fZmax, iRGBmin, iRGBmax);
    blue = iRGBmax;
  }


  var color = 'rgb(' + red + ', ' + green + ', ' + blue + ')';
  let style = createDynamicStyle(depth, color);

  return style;
}

function getLinearInterpolatedValue(x, x1, x2, y1, y2) {
  return Math.floor((x - x1) * (y2 - y1) / (x2 - x1) + y1);
}