$(function() {
	$('#PointIcon').click(function(event) {
		$('#Pointclose').show();
		$('#PointMenu').show();
	});
	$('#Pointclose').click(function(event) {
		$('#PointMenu').hide();
	});

	// fillHISTdiv()

});

var LAT;
var LON; 

	var vectorLayerC;
function plotPoint(LON, LAT) {
	var iconFeaturesC=[];
	HidePoint();
		
	var CustomMarker = new ol.Feature({
		geometry: new ol.geom.Point(ol.proj.transform([Number(LON),Number(LAT)], 'EPSG:4326', 'EPSG:3857')),
		title: "CustomMarker",
		name: "CustomMarker",
		id: CustomMarker
	});
	
	markerIconC = new ol.style.Style({
		image: new ol.style.Icon(({
			src: "images/point_icon.png",
		    scale: 0.05,
			anchor: [0.5,790],
			anchorXUnits: 'fraction',
			anchorYUnits: 'pixels',
			})),

	});
	
	CustomMarker.setStyle(markerIconC)
	iconFeaturesC.push(CustomMarker);


	var vectorSourceC = new ol.source.Vector({
	  features: iconFeaturesC
	});;
	

	
	vectorLayerC = new ol.layer.Vector({
		title: "Marker",
		name: "Marker",
		source: vectorSourceC,	
	});
	
	vectorLayerC.setZIndex(12);
	map.addLayer(vectorLayerC);
	
	map.getView().setCenter(ol.proj.transform([LON, LAT], 'EPSG:4326', 'EPSG:3857'));	

    var abbr = "Remove point(s) from map";

	$('#clearPoint').show();
};
	
function HidePoint() {
	if (vectorLayerC) {
		var featureCount = vectorLayerC.getSource();
	}
	if (featureCount) {
		var features = vectorLayerC.getSource().getFeatures();
		features.forEach((feature) => {
			vectorLayerC.getSource().removeFeature(feature);
		});
	};
    $('#clearPoint').hide();
	map.removeLayer(vectorLayerC);
}	
	
	
	
	
	