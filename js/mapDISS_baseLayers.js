function openPage(url) {
  window.open(url, "_blank");
}

//OSM ROAD
var Road = new ol.layer.Tile({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: 'OpenStreetMap - <a class="pointer" onclick="openPage(\'https://www.openstreetmap.org/copyright\')">©OpenStreetMap</a> contributors',
  baseLayer: true,
  source: new ol.source.OSM(),
  visible: false
});

//STAMEN TERRAIN
var Topo1 = new ol.layer.Tile({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: 'Stamen Terrain - <a class="pointer" onclick="openPage(\'http://stamen.com\')">Stamen Design</a>, under <a class="pointer" onclick="openPage(\'http://creativecommons.org/licenses/by/3.0\')">CC BY 3.0</a>. Data by <a class="pointer" onclick="openPage(\'http://openstreetmap.org\')">OpenStreetMap</a>,<br> under <a class="pointer" onclick="openPage(\'http://www.openstreetmap.org/copyright\')">ODbL</a>',
  baseLayer: true,
  source: new ol.source.Stamen({
    layer: 'terrain',
  }),
  visible: false
});
//OPENTOPOMAP
var Topo2 = new ol.layer.Tile({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: 'OpenTopoMap - © <a class="pointer" onclick="openPage(\'https://openstreetmap.org/copyright\')">OpenStreetMap</a> Contributor, SRTM | Map View © <a class="pointer" onclick="openPage(\'http://opentopomap.org\')">OpenTopoMap</a> <br>(<a class="pointer" onclick="openPage(\'https://creativecommons.org/licenses/by-sa/3.0\')" >CC-BY-SA</a>)',
  baseLayer: true,
  source: new ol.source.XYZ({
    url: '//{a-c}.tile.opentopomap.org/{z}/{x}/{y}.png',
    crossOrigin: "anonymous",
    tileOptions: { crossOriginKeyword: null }
  }),
  visible: false
});

//AERIAL - ESRI WITH API
var Aerial = new ol.layer.Tile({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: 'Satellite (ESRI) - Map Tiles © 2022 <a class="pointer" onclick="openPage(\'https://www.arcgis.com/home/item.html?id=ab399b847323487dba26809bf11ea91a\')"  >Esri</a>, Maxar, Earthstar Geographics, <br>CNES/Airbus DS, USDA FSA, USGS, Getmapping, Aerogrid, IGN, IGP, and the GIS <br> User Community',
  baseLayer: true,
  source: new ol.source.XYZ({
    url: 'https://ibasemaps-api.arcgis.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}?token=AAPK2dcb0758e7424ca9a745a39c7f30a33ePD12xcU2udx1RhdtT5iGvYuWR_8sQkqMH3H-VvqYqTX8qry15s3igP5VVnLPW2ht"',
    crossOrigin: "anonymous",
  }),
  visible: false
});

//HYBRID, ESRI
var Hybrid = new ol.layer.Group({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: 'Hybrid (ESRI) - Map Tiles © 2022 <a class="pointer" onclick="openPage(\'https://www.arcgis.com/home/item.html?id=ab399b847323487dba26809bf11ea91a\')"  >Esri</a>, Maxar, Earthstar Geographics, CNES/Airbus <br> DS, USDA FSA, USGS, Getmapping, Aerogrid, IGN, IGP, and the GIS User Community <br> and by <a class="pointer" onclick="openPage(\'http://stamen.com\')">Stamen Design</a>, under <a class="pointer" onclick="openPage(\'http://creativecommons.org/licenses/by/3.0\')">CC BY 3.0</a>.Data by <a class="pointer" onclick="openPage(\'http://openstreetmap.org\')">OpenStreetMap</a>,  under <a class="pointer" onclick="openPage(\'http://www.openstreetmap.org/copyright\')">ODbL</a>',
  baseLayer: true,
  layers: [
    // AERIAL - ESRI WITH API
    new ol.layer.Tile({
      //baseLayer:true,
      source: new ol.source.XYZ({
        url: 'https://ibasemaps-api.arcgis.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}?token=AAPK2dcb0758e7424ca9a745a39c7f30a33ePD12xcU2udx1RhdtT5iGvYuWR_8sQkqMH3H-VvqYqTX8qry15s3igP5VVnLPW2ht"',
        crossOrigin: "anonymous"
      }),
      visible: true,
      displayInLayerSwitcher: false,
    }),

    // LABELS&BOUNDARIES - ESRI WITH API
    new ol.layer.Tile({
      //baseLayer:true,
      source: new ol.source.XYZ({
        url: 'https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}?token=AAPK2dcb0758e7424ca9a745a39c7f30a33ePD12xcU2udx1RhdtT5iGvYuWR_8sQkqMH3H-VvqYqTX8qry15s3igP5VVnLPW2ht"',
        crossOrigin: "anonymous"
      }),
      displayInLayerSwitcher: false,
    }),

    // STAMEN - TONER HYBRID		
    //new ol.layer.Tile({
    //  name: 'Labels',
    //	opacity: 0.6,
    //  source: new ol.source.Stamen({
    //    layer: 'toner-hybrid',
    //  }),
    //  displayInLayerSwitcher: false,
    //})
  ],
  visible: true
});

// OCEAN
var Ocean = new ol.layer.Tile({
  displayInfo: false,     //GT2023 CUSTOM
  noSwitcherDelete: true,  //GT2023 CUSTOM
  name: 'Ocean (ESRI) - Map Tiles © 2022 <a class="pointer" onclick="openPage(\'https://www.arcgis.com/home/item.html?id=ab399b847323487dba26809bf11ea91a\')">Esri</a>, GEBCO, NOAA, Garmin, HERE, <br>and other contributors',
  baseLayer: true,
  source: new ol.source.XYZ({
    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}',
    crossOrigin: "anonymous",
  }),
  visible: false
});	
