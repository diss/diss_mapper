﻿<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<title>DISS - Mapper</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="favicon.ico" rel="shortcut icon">

	

	<!--  OL 6.5  -->
	<link rel="stylesheet" href="ol-6.5/ol.css" type="text/css">
	<script src="ol-6.5/ol.js"></script>
	<!-- <link rel="stylesheet" href="../OL4/css/ol.css" type="text/css"> -->
	<!-- <script src="../OL4/build/ol.js"></script> -->

	<!--  Download map  -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.min.js"></script>

	<!--  codice per GEOCODER  -->
	<script src="ol-geocoder/ol-geocoder.js"></script>
	<link rel="stylesheet" type="text/css" href="ol-geocoder/ol-geocoder.css" />

	<!--  JQUERY 3.6  -->
	<script src="jquery/jquery-3.6.0.min.js"></script>
	<script src="jquery/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css" />

	<!--librerie ol-ext -->
	<link rel="stylesheet" href="https://cdn.rawgit.com/Viglino/ol-ext/master/dist/ol-ext.min.css" />
	<script type="text/javascript" src="ol-ext/ol-ext.js"></script>
	<!--  CSS personalizzato ol-ext  -->
	<link href="css/my_ol-ext.css" rel="stylesheet" type="text/css">

	<!--  CSS GENERALE  -->
	<link href="css/css.css" rel="stylesheet" type="text/css">

	<!--  CODICE JS GENERALE DISS  -->
	<script type="text/javascript" src="js/js.js"></script>
	<!-- <script type="text/javascript" src="js/toggle.js"></script> -->
	<script type="text/javascript" src="kml/sources.js"></script>


	<!--  BOOTSTRAP JS PER STILE POPUP  -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"> -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script> -->


	<!--  codice per Google Analytics  -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-421006-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-421006-2');
	</script>


	<script type="text/javascript">
		// bounds of the desired area
	

		var coordinate;
		var ToggleAR = "on";
		var ToggleAFT = "off";
		var ToggleAFD = "off";
		var ToggleISS = "on";
		var ToggleCSS = "on";
		var ToggleSUBD = "on";
		var ToggleDSS = "off";
		var map;


		function gup(name) {
			name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
			var regexS = "[\\?&]" + name + "=([^&#]*)";
			var regex = new RegExp(regexS);
			var results = regex.exec(decodeURIComponent(window.location.search));
			if (results == null)
				return "";
			else
				return results[1];
		}

		var ll = gup('ll');
		var b = ll.split(",");
		var IDsource = String(b[0]);
		var view;

		var control = 0;

		if (IDsource == '')
			control = 1;

		//alert (IDsource);
		var SOURCEARR = new Array();
		carica();

		for (i = 0; i < 1000; i++) {
			if (SOURCEARR[0][i] == IDsource) {
				var centroidX = (SOURCEARR[1][i]);
				var centroidY = (SOURCEARR[2][i]);
				var MaxX = (SOURCEARR[3][i]);
				var MaxY = (SOURCEARR[4][i]);
				var MinX = (SOURCEARR[5][i]);
				var MinY = (SOURCEARR[6][i]);
				var Polygon = (SOURCEARR[7][i]);
				//alert (centroidX);
			};
		};
	</script>

</head>

<body onload='checkSource()'>
	<div id="mouse-position"></div>

	<div id="banner">
		<img class="center" src="img/header2021.jpg" usemap="#logo"></a>
		<map name="logo">
			<area shape="rect" coords="816,65,201,0" href="https://diss.ingv.it/" target="_blank">
			<area shape="rect" coords="200,65,0,0" href="https://www.ingv.it" target="_blank">
		</map>
	</div>

	<div class="navbar">
		<ul>
			<li><a class="" href="https://diss.ingv.it/" target="_blank">HOME</a></li>
			<!-- <li><a href="https://diss.ingv.it/using-the-database-interface" target="_blank">Doc</a></li> -->
			<li><a href="https://diss.ingv.it/user-interface" target="_blank">Help</a></li>

		</ul>
	</div>

	<div class="sidebar">
		<table BORDER=0 CELLPADDING=0 CELLSPACING=0>
			<?php include("html/mapperMenu.html"); ?>
		</table>

		<div id="layersElegenda" class="blocchiContenuti"><br><span>DISS 3.3.0</span><br><br></div>
		<br>
		<div id="credits_licence">
			<div id="contenutoCredits" class="blocchiContenuti"></div>
			<div id="licenza"><br><span></span><br><br></div>
		</div>

		<div id="contenutoISSList" class="blocchiContenuti"></div>
		<div id="contenutoCSSList" class="blocchiContenuti"></div>
		<div id="contenutoDSSList" class="blocchiContenuti"></div>
		<div id="contenutoSDSList" class="blocchiContenuti"></div>
		<div id="contenutoDetail" class="blocchiContenuti"></div>

		<span class="resize"></span>
		<button onclick="topFunction()" id="tornaSu" title="Torna su">Up</button>
		
	</div>

	<div id="map" class="map"></div>

	<div class="mobile-menu" onclick="showMenuMobile()"><button id="toggleSidebar" >menu</button></div>

	<button id="geolocation-button">Geo
		loc</button>

	

	

	<div id="pictureContent">
		<div id="mydivheader">Diss-Image</div>
	</div>

	<div id="warningContent">
		<div id="mydivheader">Warning</div>
	</div>

	<div id="buttonList">
		<div id="download"><abbr title="save map (png)"><button class="button" id="export-png"><b> <img src="img/save.png" height="27px"> </b></button><a id="image-download" download="map.png"></a></abbr></div>
		<div id="measure"><abbr title="measure spherical distance"><button class="button" id="b1"><a onclick="ToggleMeas();"> <img src="img/ruler.png" height="27px"> </a></button></abbr></div>
		<div id="STRUMeqIcon"><abbr title="instrumental seismicity"><a href="#"><button class="button" id="STRUMeqtitle"><img src="images/STRUMeq_icon.png" height="27px"></button></a></abbr></div>
		<div id="STRUMeqMenu" style="display: none;">
			<div id="STRUMeqMenuheader"><b>INSTRUMENTAL SEISMICITY (from <a href="https://terremoti.ingv.it" target="_blank"> ONT - INGV</a>) <br> AND ACCESS TO <abbr title="Hai Sentito Il Terremoto?"><a href="http://www.haisentitoilterremoto.it" target="_blank"> HSIT</a></abbr> DATA</b></div><a href="#" id="STRUMclose"></a>

			<div id="STRUMmenuContent">
				<br>

				<div id="periodSTRUM" class="TitleMenuSTRUM">Time interval</div>
				<div class="inputdiv"><label id="fromDateSTRUM">From </label> <input type="text" id="StartDateSTRUM" class="datepicker"><label id="toDateSTRUM"> to </label> <input type="text" id="StopDateSTRUM" class="datepicker"></div><br>

				<div class="TitleMenuSTRUM" id="CoordSTRUM">Area</div>
				<div class="inputdiv">
					<label>Lat.</label> <input type="text" id="StartLatSTRUM" size="4px" maxlength="5" value="34.5" onchange="resizeRectSTRUM()"> <label>-</label> <input type="text" id="StopLatSTRUM" size="4px" maxlength="5" value="48.5" onchange="resizeRectSTRUM()">
					<label> Lon.</label> <input type="text" id="StartLonSTRUM" size="4px" maxlength="5" value="5.5" onchange="resizeRectSTRUM()"> <label>-</label> <input type="text" id="StopLonSTRUM" size="4px" maxlength="5" value="25.5" onchange="resizeRectSTRUM()">
					<div id="selAreaSTRUM" onclick="CreateRectSTRUM();"><abbr id="selAreaAbbrSTRUM"><img src="./images/area_selection.png" width="25px"></abbr></div>
					<div id="NoselAreaSTRUM" onclick="RemoveBox();"> <abbr id="noselAreaAbbrSTRUM"><img src="./images/area_selection_gray.png" width="25px"></abbr></div><br>
				</div>



				<div id="magSTRUM" class="TitleMenuSTRUM">Magnitude interval</div>
				<div class="inputdiv"><label id="fromMMSTRUM">Min. </label> <input type="text" id="StartMagSTRUM" size="5px" maxlength="4" value="2" />
					<label id="toMMSTRUM">Max. </label><input type="text" id="StopMagSTRUM" size="5px;" maxlength="4" / value="10">
					<div id="sliderMSTRUM"></div>
				</div>
				<br>
				<div id="depSTRUM" class="TitleMenuSTRUM">Depth interval (km)</div>
				<div class="inputdiv"><label id="fromDDSTRUM">Min. </label> <input type="text" id="StartDepSTRUM" size="5px" maxlength="4" value="0" />
					<label id="toDDSTRUM">Max. </label><input type="text" id="StopDepSTRUM" size="5px" maxlength="4" value="1000" />
					<div id="sliderDSTRUM"></div>
				</div>




				<div id="resultSTRUM">

					<abbr class="OKbutton" title=""><input type="button" id="FilterByKindSTRUM" value="OK" onclick="$('#loading').show(); getSTRUMeq(document.getElementById('StartDateSTRUM').value, document.getElementById('StopDateSTRUM').value, document.getElementById('StartLatSTRUM').value, document.getElementById('StopLatSTRUM').value, document.getElementById('StartLonSTRUM').value, document.getElementById('StopLonSTRUM').value, document.getElementById('StartMagSTRUM').value, document.getElementById('StopMagSTRUM').value, document.getElementById('StartDepSTRUM').value, document.getElementById('StopDepSTRUM').value)" /></abbr>
					<div id="numSTRUM"></div>
					<div id="warningSTRUM">Exceeded maximum number of earthquakes allowed</div>
				</div>

				<div id="legSTRUM" class="TitleLegendSTRUM">MAP LEGEND</div>
				<table id="legendSTRUM">
					<tr>
						<td class="empty"></td>
						<td class="legSTRUMcol">
							<img src="images/strum_quake_icon_blue5.png" height="30px">
						</td>
						<td class="legSTRUMcol">
							<img src="images/strum_quake_icon_blue4.png" height="23px">
						</td>
						<td class="legSTRUMcol">
							<img src="images/strum_quake_icon_blue3.png" height="17px">
						</td>
						<td class="legSTRUMcol">
							<img src="images/strum_quake_icon_blue2.png" height="13px">
						</td>
						<td class="legSTRUMcol">
							<img src="images/strum_quake_icon_blue1.png" height="8px">
						</td>
						<!-- non più usato col nuovo controllo dei livelli GT2023 -->
						<!-- <td class="legSTRUMcol2">
							<input type="radio" name="topbot" id ="ztop" value="top" onclick="SetZIndexSTR('top')" checked disabled>△ Top<br>
							<input type="radio" name="topbot" id ="zbot" value="bot" onclick="SetZIndexSTR('bot')" disabled>▽ Bottom
					</td> -->
					</tr>
					<tr>
						<td class="legSTRUMcol">Mag.</td>
						<td class="legSTRUMcol"> &ge;6</td>
						<td class="legSTRUMcol">5.9-5</td>
						<td class="legSTRUMcol">4.9-4</td>
						<td class="legSTRUMcol">3.9-3</td>
						<td class="legSTRUMcol"> &#60;3</td>


					</tr>
				</table>

			</div>

		</div>
		<div id="HISTeqIcon">
			<abbr title="CPTI and CFTI data"><a href="#"><button class="button" id="HISTeqtitle"><img src="images/HISTeq_icon.png" height="27px"></button></a></abbr>
		</div>
		<div id="HISTeqMenu" style="display: none;">
			<div id="HISTeqMenuheader"><b>CPTI and CFTI</b></div><a href="#" id="HISTclose"></a>

			<div id="HISTmenuContent">
				<br>Data from:<br>
				<table>
					<tr>
						<td>
							<input type="radio" name="HISTsel" id="CPTI" value="CPTI" onclick="ShowEPI_CPTI()"><abbr title="Provides homogeneous macroseismic and instrumental data and parameters, for Italian earthquakes with maximum intensity ≥ 5 or magnitude ≥ 4.0 in the period 1000-2014."><b> CPTI15 v.4.0</b></abbr>
							<br>Earthquakes from 1000 to 2020 <br>(instrumental and historical data).
							<br><br>Rovida A., Locati M., Camassi R., Lolli B., Gasperini P., Antonucci A. (2022).  <i>Catalogo Parametrico dei Terremoti Italiani (CPTI15), versione 4.0.</i> Istituto Nazionale di Geofisica e Vulcanologia (INGV). <br><a href="https://doi.org/10.13127/CPTI/CPTI15.4" target="_blank"><b>https://doi.org/10.13127/CPTI/CPTI15.4</b></a>
						</td>
						<td rowspan="2">
							<img src="https://emidius.mi.ingv.it/services/italy/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=CPTI15_v2&style=CPTI15_squares_colors">
						</td>
					</tr>
					<tr>
						<td>
							<br>
							<input type="radio" name="HISTsel" id="CFTI" value="CFTI" onclick="ShowEPI_CFTI()"><b> <abbr title="Provides data of 1167 earthquakes that occurred in Italy from 461 a.C. to 1997 (territorial impact and associated social and economic upheaval) and of 473 large earthquakes that occurred in the Mediterranean area from 760 b.C. to 1500.">CFTI5Med</abbr> (Italian earthquakes)</b>
							<br>Earthquakes from 760 a.C. to 1997 <br>(historical data).
							<br><br>Guidoboni E., Ferrari G., Mariotti D., Comastri A., Tarabusi G., Sgattoni G., Valensise G. (2018). <i>CFTI5Med, Catalogo dei Forti Terremoti in Italia (461 a.C.-1997) e nell’area Mediterranea (760 a.C.-1500).</i> Istituto Nazionale di Geofisica e Vulcanologia (INGV). <br><a href="https://doi.org/10.6092/ingv.it-cfti5" target="_blank"><b>https://doi.org/10.6092/ingv.it-cfti5</b></a>
							<br>
						</td>
					</tr>
					<tr>
						<td>
							<div id="clearHIST" onclick="HideEPI()"></div>
						</td>
						<td class="legEPIcol2">
							<input type="radio" name="topbotE" id="ztopE" value="topE" onclick="SetZIndexEPI('top')" checked disabled>△ Top<br>
							<input type="radio" name="topbotE" id="zbotE" value="botE" onclick="SetZIndexEPI('bot')" disabled>▽ Bottom
						</td>
				</table>

			</div>

		</div>
		<div id="PointIcon">
			<abbr title="add a custom point"><a href="#"><button class="button" id="Pointtitle"><img src="images/point_icon.png" height="27px"></button></a></abbr>
		</div>
		<div id="PointMenu" style="display: none;">
			<div id="PointMenuheader"><b>add a custom point</b></div><a href="#" id="Pointclose"></a>

			<div id="PointmenuContent"><br>
				<b>Latitude</b>: <input type="text" id="LAT" size="5px" maxlength="6" value="44.495" class="customp"><br><br>
				<b>Longitude</b>: <input type="text" id="LON" size="5px" maxlength="6" value="11.345" class="customp"><br><br>
				<abbr class="OKbutton" title=""><input type="button" id="FilterByKindSTRUM" value="OK" onclick="plotPoint(document.getElementById('LON').value, document.getElementById('LAT').value)"></abbr>
			</div>
			<div id="clearPoint" onclick="HidePoint()"><abbr id="HidePointabbr" title="Remove point(s) from map"><img src="./images/clearMap.png" width="25px"></abbr></div>
		</div>

	</div>



	<!--    ======================================       CUSTOM MARKER       ======================================-->



	


	<div id="layercontrol">
		<div id="layercontrolheader">
			<table>
				<tr>

				</tr>
			</table>
		</div>

		<div id="layercontrolContent">
			<div id="credits">
				<table>
					<tr>
						<td>
							<a href="https://diss.ingv.it/" target="_new"><img src="img/diss.png"></a>
						</td>
						<td style="white-space: nowrap;">
							<font size="+1"><b id="VERS"> DISS .....</b></font>
						</td>
						<td style="white-space: nowrap;">
							&nbsp;&nbsp;
						</td>
						<td>
							<a title="Except where otherwise noted, content on this site is licensed under a Creative Commons Attribution 4.0 International (CC BY 4.0) licence." href="http://creativecommons.org/licenses/by/4.0/" target="_blank">
								<img src="img/CC-BY.png" alt="CC-BY"></a>
						</td>
						<td style="padding:4px">
							Except where otherwise noted, content on this site is licensed under a Creative Commons Attribution 4.0 International (CC BY 4.0) licence.
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>


	<!--    ======================================       SISMICITA' STRUMENTALE       ======================================-->




	<!--    ======================================       SISMICITA' STORICA       ======================================-->






	<div id="popup" class="ol-popup">
		<a href="#" id="popup-closer" class="ol-popup-closer"></a>
		<div id="popup-content"></div>
	</div>


	<!--    ======================================       CODICE PERSONALIZZATO PER FUNZIONI MAPPA       ======================================-->


	<script type="text/javascript" src="js/mapDISS_caricaMapper.js"></script>
	<script type="text/javascript" src="js/mapDISS_styles.js"></script>
	<script type="text/javascript" src="js/mapDISS_sources.js"></script>
	<script type="text/javascript" src="js/mapDISS_baseLayers.js"></script>
	<script type="text/javascript" src="js/mapDISS_vectLayers.js"></script>
	<script type="text/javascript" src="js/mapDISS.js"></script>
	<script type="text/javascript" src="js/mapDISS_listenerOfSource.js"></script>


	<!--    ======================================       SLIDER (Sism. STRUMENTALE)       ======================================-->
	<script src="slider/nouislider.js"></script>
	<link rel="stylesheet" href="slider/nouislider.css" />

	<!--    ======================================       CODICE PERSONALIZZATO PER DRAG DELLE FINESTRE       ======================================-->
	<!-- <script type="text/javascript" src="js/draggable.js"></script> -->

	<!--    ======================================       STRUMENTO DI MISURAZIONE DISTANZE       ======================================-->
	<link href="css/measure.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/measure.js"></script>

	<!--    ======================================       CODICE PERSONALIZZATO PER GEOCODER       ======================================-->
	<link href="css/my_ol-geocoder.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/my_ol-geocoder.js"></script>


	<!--    ======================================       CODICE PERSONALIZZATO PER SISMICITA' STRUMENTALE       ======================================-->
	<link href="css/strum.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/strum.js"></script>

	<!--    ======================================       CODICE PERSONALIZZATO PER SISMICITA' STORICA       ======================================-->
	<link href="css/hist.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/hist.js"></script>

	<!--    ======================================       CODICE PERSONALIZZATO PER CUSTOM POINT      ======================================-->
	<link href="css/point.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/point.js"></script>

	<!--    ======================================       REFRESH DELLA MAPPA ALLA FINE PER RIDIMENSIONARLA CORRETTAMENTE DOPO LA CREAZIONE DI TUTTI I TOOLS      ======================================-->
	<script type="text/javascript">
		this.map.updateSize();
	</script>


	<script>
		//resize la sidebar
		const resizeHandle = document.querySelector('.resize');
		resizeHandle.addEventListener('mousedown', function(e) {
			window.addEventListener('mousemove', resizeSidebar);
			window.addEventListener('mouseup', stopResizeSidebar);
		});

		function resizeSidebar(e) {
			// const menu = document.querySelector('#sourcesMenu');
			const canvas = document.querySelector('canvas');
			const sidebar = document.querySelector('.sidebar');
			sidebar.style.width = (e.clientX - sidebar.offsetLeft) + 'px';
			const mapDiv = document.querySelector('.map');
			mapDiv.style.width = (window.innerWidth - e.clientX) + 'px';
			mapDiv.style.left = (e.clientX) + 'px';
			canvas.style.width = (window.innerWidth - e.clientX) + 'px';
			//(window.innerWidth - sidebarWidth)
			//mapDiv.style.width = (e.clientX - mapDiv.offsetLeft) + 'px';
			this.map.updateSize();

			const topButton = document.querySelector('#tornaSu');
			topButton.style.left = (e.clientX - sidebar.offsetLeft - 50) + 'px';
		}

		function stopResizeSidebar() {
			window.removeEventListener('mousemove', resizeSidebar);
		}

		function topFunction() {
			// window.location.href = "#TAB1";
			// $("#contenutoISSList").animate({
			// 	scrollTop: 0
			// }, "fast");
			$(".DIVINTERNO").animate({
				scrollTop: 0
			}, "fast");

		}

		var open_closeMenuMobile = 0

		function showMenuMobile(){

			var sidebar = document.querySelector('.sidebar');
			
			if (open_closeMenuMobile==0){
				
				if (sidebar) {
					sidebar.style.display = 'block';
				}
				open_closeMenuMobile = 1;

			}else if (open_closeMenuMobile == 1){
				if (sidebar) {
					sidebar.style.display = 'none';
				}
				open_closeMenuMobile = 0
			}

		}
	</script>


</body>

</html>