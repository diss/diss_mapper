<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
	<title>DISS - Data Tables </title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<link rel="icon" type="image/png" href="img/ingv.ico">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="stylesheet" href="css/tab.css" type="text/css">
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/s_js.js" charset="utf-8"/></script>
	<script type="text/javascript" src='js/tablesort.js'></script>

	<!--  codice per Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-421006-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-421006-2');
</script>
</head>

<body onload = "test_type()">
	<div id="all">
		<?php include("html/header.html"); ?>
		<div class="right-column">
			<table BORDER=0 CELLPADDING=0 CELLSPACING=0>
					<?php include("html/leftmenu.html"); ?>
			</table>
		</div>
		
		<div class="principale">
			<div class="TITLE" id="TITLE">

			</div>
			<div class="TITLE_A" id="INFO_ICON">
								
			</div>
			<div class="mainDIV" id="mainDIV">
				<div id="ID_S"></div>
				<div id="tabcont1" class="tabpanel">
					<ul id="tabcont1-nav" class="tabnav">
					<li><a href="#tab1"><span>Information</span></a></li>
					<li><a href="#tab2"><span>Commentary</span></a></li>
					<li><a href="#tab3"><span>Pictures</span></a></li>
					<li><a href="#tab4"><span>References</span></a></li>
					</ul>
					<div id="tab1" class="tab">

					<table class="MAINTABLE">
						<tbody>
							<tr class="SEPARATOR" id="CONTAINERELEMENT_sep-labe">
								<td colspan="2" class="LABEL"><strong>General information</strong></td>
							</tr>
							<tr class="ODD" id="CONTAINERELEMENT_IDSource">
								<td scope="row" class="ODD">
									<strong><label for="VALUEELEMENT_IDSource">DISS-ID</label></strong>
								</td>
								<td id="VALUEELEMENT_IDSource" class="ODD">
								</td>
							</tr>
						<tr class="EVEN" id="CONTAINERELEMENT_SourceName">
							<td scope="row" class="EVEN">
								<strong><label for="VALUEELEMENT_SourceName">Name</label></strong>
							</td>

							<td id="VALUEELEMENT_SourceName" class="EVEN">
						</td>
						</tr>
						<tr class="ODD" id="CONTAINERELEMENT_compilers">
							<td scope="row" class="ODD">
								<strong><label for="VALUEELEMENT_compilers">Compiler(s)</label></strong>
							</td>

							<td id="VALUEELEMENT_compilers" class="ODD">
						</td>
						</tr>
						<tr class="EVEN" id="CONTAINERELEMENT_contributors">
							<td scope="row" class="EVEN">
								<strong><label for="VALUEELEMENT_contributors">Contributor(s)</label></strong>
							</td>

							<td id="VALUEELEMENT_contributors" class="EVEN">
						</td>
						</tr>
						<tr class="ODD" id="CONTAINERELEMENT_affiliations">
							<td scope="row" class="ODD">
								<strong><label for="VALUEELEMENT_affiliations">Affiliation(s)</label></strong>
							</td>

							<td id="VALUEELEMENT_affiliations" class="ODD">
						</td>
						</tr>
						<tr class="EVEN" id="CONTAINERELEMENT_created">
							<td scope="row" class="EVEN">
								<strong><label for="VALUEELEMENT_created">Created</label></strong>
							</td>

							<td id="VALUEELEMENT_created" class="EVEN">
						</td>
						</tr>
						<tr class="ODD" id="CONTAINERELEMENT_latestupdate">
							<td scope="row" class="ODD">
								<strong><label for="VALUEELEMENT_latestupdate">Updated</label></strong>
							</td>

							<td id="VALUEELEMENT_latestupdate" class="ODD">
						</td>
						</tr>
						<tr class="EVEN" id="CONTAINERELEMENT_latestupdate">
							<td scope="row" class="EVEN">
								<strong><label for="VALUEELEMENT_display">Display map</label></strong>
							</td>

							<td id="VALUEELEMENT_display" class="EVEN">
						</td>
						</tr>

						
						<?php 
						$pageUrl = $_SERVER['REQUEST_URI'];
						$Type = substr($pageUrl, 23, 2);
						// echo $Type;
						include("html/" . $Type . ".html"); 
						if ($Type == 'IS' || $Type == 'CS' || $Type == 'SD') {
							include("html/AFF.html");
						}						
						?>

					</table>
					
					</div>
					<div id="tab2" class="tab">							
					
					</div>
					<div id="tab3" class="tab">								
					
					</div>
					<div id="tab4" class="tab">							
					
					</div>
				</div>


			</div>
		</div>
		<script type="text/javascript">
		var tabber=new Yetii('tabcont1');
		tabber.init();
		</script>
	</div>
</body>
</html>
