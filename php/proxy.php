<?php
// Ottieni l'URL del server WMS dalla query string
$wmsUrl = $_GET['url'];

// Verifica se è stato fornito un URL valido
if (!filter_var($wmsUrl, FILTER_VALIDATE_URL)) {
    header("HTTP/1.1 400 Bad Request");
    echo "URL non valido";
    exit();
}

// Esegui la richiesta GetCapabilities al server WMS
$response = file_get_contents($wmsUrl);

// Verifica se la richiesta è andata a buon fine
if ($response === FALSE) {
    header("HTTP/1.1 500 Internal Server Error");
    echo "Errore durante la richiesta al server WMS";
    exit();
}

// Imposta gli header della risposta con il tipo di contenuto e consenti le richieste da qualsiasi dominio
header("Content-Type: application/xml");
header("Access-Control-Allow-Origin: *");

// Restituisci la risposta del server WMS al client
echo $response;


?>
