<?php
// https://stackoverflow.com/questions/32820376/fopen-accept-self-signed-certificate
$opts=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
); 

$WMSurl = $_REQUEST["WMSurl"];
// echo $WMSurl;
$file = fopen($WMSurl, 'rb', false, stream_context_create($opts));
$contents = stream_get_contents($file);

fclose($file);
echo $contents ;
?>
