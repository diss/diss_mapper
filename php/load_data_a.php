<?php
$REQurl = $_REQUEST["REQurl"];
$response = file_get_contents($REQurl);

if ($response !== false) {
    // L'URL è raggiungibile e hai ottenuto una risposta
    echo $response;
} else {
    // L'URL non è raggiungibile o si è verificato un errore
    echo "NULL";
}
?>
