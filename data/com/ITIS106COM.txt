﻿    **COMMENTS**
    
    This Source is associated with the 11 January 1693 earthquake. This event in DISS is hypothesized to be a complex earthquake, with two shocks occurring on two compressional structures located to the north and south of the Catania Plain.
    
    The 1693 earthquake ranks as the largest in the Italian historical record, and struck a large portion of eastern Sicily, affecting with maximum intensities the area around Catania (south-southeast of Mount Etna) and the whole Hyblean Plateau (Guidoboni et al., 2007).
    
    The 1693 earthquake was preceded by a strong foreshock on 9 January (Mw 6.2, Imax VIII-IX) and by another foreshock on the same day. The event of the 9 January caused severe damages in the region of the Hyblean Plateau and increased the vulnerability of the buildings, thus probably contributing to the very large damage caused on 11 January by the mainshock. The seismic sequence lasted until 1696, was felt all over eastern Sicily and was characterized by damaging aftershocks (Guidoboni et al., 2007).
    Most likely the mainshock included two or more subevents, as suggested by the existence of two separate concentrations of higher damage, but this hypothesis is not explicitly backed up by the available historical sources.
    
    Earthquakes characterized by source complexity or multiple shocks occurring close in time and space are a common feature of Italian seismicity, as testified by numerous examples from different tectonic environments and geographic areas (e.g. 1456, Southern Italy, Mw 7.0, strike-slip/oblique; 1857, Basilicata, Mw 6.9, normal faulting; 1976, Friuli, Mw 6.4, thrust faulting; 1980, Irpinia, Mw 6.9, normal faulting).
    
    In spite of the severity of damage and the widespread environmental effects, the causative source of the 11 January 1693 earthquake is still the object of lively debate. This is due to several circumstances, including: 1) the absence of any evidence of surface faulting and the lack of any contemporary report that could describe such effects, that could indicate the need to investigate a hidden or blind fault; 2) the proximity of the mesoseismal area to the coast, causing a lack of information for a significant sector of the shaken area; 3) the occurrence of a tsunami following the mainshocks, suggesting that the earthquake source was located near-or off-shore; 4) the presence of multiple shocks, which stressed the structural complexity of the seismogenic source and the potential for triggering of subsequent ruptures;
    
    According to the multiple shocks hypothesis implemented in DISS, the 1693 earthquake involved the the activation of two opposite-verging compressional faults: a NW-dipping thrust lying beneath the Terre Forti anticlines, that is a segment of the Apennine-Maghrebian thrust front near Catania, and a SE-dipping reverse fault located beneath Mount Lauro (northern margin of the Hyblean Plateau).
    
    Both Sources were identified taking into account various geomorphic and structural constraints, and fit the two main patches of largest intensities of the 1693 earthquake.
    
    This Source is a N-dipping thrust fault. Its hanging-wall block includes the Terre Forti anticlines, a system of folds deforming very recent sediments (e.g., Bousquet and Lanzafame, 1986) and controlling the drainage pattern. The source is a segment of the Gela-Catania Apennine-Maghrebian thrust front.
    
    The reverse/thrust kinematics of these seismogenic sources is in agreement with geological observations and present-day stress field and GPS data, that indicate NNW-directed active shortening between the Hyblean Plateau and the Etna area.
    
    Based on the occurrence of a large tsunami that hit the coast of Eastern Sicily, the source of the 11 January 1693 earthquake was thought to lie off-shore (e.g., Gutscher et al., 2006; Gerardi et al., 2008), and specifically along the Malta Escarpment (Piatanesi and Tinti, 1998; Bianca et al., 1999; Azzaro and Barbano, 2000; Jacques et al., 2001; Argnani and Bonazzi, 2005, among others). The main open question regards the mismatch between the hypothesized off-shore source and the damage pattern, which displays the highest intensities inland and a drop towards the coast (Guidoboni et al., 2007).
    
    For the causative fault of the 1693 earthquake different analyses and input data returned widely different hypotheses.
    Based on modeling of the intensity pattern using the kinematic function approach, Sirovich and Pettenati (2001) proposed a NNE-oriented, 60 km long-fault characterized by oblique motion and nearly overlapping the Scicli fault zone. At the opposite end of the spectrum, Gutscher et al. (2006) proposed that the 1693 earthquake was caused by slip over a portion of the locked basal thrust of the the Calabrian arc, and tested their hypothesis by reproducing the polarity and amplitude of the observed tsunami.
    Based on structural and seismological data, Lavecchia et al. (2007) suggested that the 1693 earthquake was caused by a segment of the S-verging, N-dipping Sicilian Basal Thrust, that is to say, a segment of the basal thrust of the Apennine-Maghrebian chain emerging along the outermost thrust front. Visini et al. (2008) carried out an analytical simulation of the damage pattern produced by different earthquake models proposed in the literature and contended that the best fitting solutions are obtained for three quite diverse sources corresponding with (i) the Scicli fault zone on-shore, (ii) a portion of the Ionian subduction plane (Gutscher et al., 2006), and (iii) a segment of the Sicilian Basal Thrust (Lavecchia et al., 2007). 
    
    Another issue regards the tectonic or non-tectonic origin of the Terre Forti Anticlines, due to their location along the southern margin of the Mt. Etna volcano. According to some authors (e.g. Borgia et al., 1992) these growing anticlines are due to the lateral spreading of the southern flank of the Etna volcano and do not reflect an active shortening. However, it must be observed that the anticlines follow the same strike of the underlying Gela-Catania thrust front (i.e. ENE-WSW), and that this trend is not the same as that of the southern flank of the volcanic edifice (i.e. locally E-W). Besides, the anticlines outcrop also west of the Simeto River course, that is outside of the supposed area of uplift induced by the gravitational movements.
    
    
    **OPEN QUESTIONS**
    
    1) Is it possible to explain the two concentrations of higher damage produced by the 11 January 1693 earthquake (in the Hyblean region and near Catania) with a complex event involving two shocks?
    
    2) What triggered the large and widespred tsunami that followed the earthquake?
    
    3) What is the seismogenic potential of the Scicli fault zone?
    
    4) Is the Malta Escarpment characterized by active extensional faults?
    
    
    **SUMMARIES**
    
    Bianca et al. (1999)
    They present geological data combined with the analysis of seismic reflection lines across the Ionian offshore, in order to recognized evidence of active faulting in southeastern Sicily. They suggest that the shock of January 11, 1693 is related to a 45 km long normal fault with a right-lateral component of motion. They locate this fault offshore along the Ionian coast between Catania and Siracusa. The shock of January 9, 1693 has an epicenter onland, probably along the Avola fault.
    
    Zollo et al. (1999)
    They present the numerical simulation of strong ground motion produced by extended faulting phenomena reproducing the areal extension and magnitude of the 1693 earthquake. They deem that the 1693 earthquake occurred at sea, along the Ibleo-Maltese fault scarp. Their work concerns the simulation of the high frequency radiation that could be generated during the rupture of one or more segments of the Ibleo-Maltese fault system and recorded in Southern Sicily. Their seismogenic source is a composite fault system formed by two sub-parallel segments of the Ibleo-Maltese fault. In detail they hypothesize two near-vertical normal faults dipping NE, at shallow depth, located at less than 30 km from the coastline between the town of Catania and Siracusa. 
    
    Sirovich and Pettenati (2001)
    They inverted the macroseismic intensity felt reports of the 1693 earthquake, and retrieved source parameters. Their technique uses Voronoi polygons and their kinematic function KF. The 1693 source, constrained by their inversion, is a NNE-oriented fault, approximately 60 km long, dipping from 90° to 54° toward ESE, 50% strike-slip and 50% dip-slip, which crosses SE Sicily from the Hyblean Plateau to the coast of the Ionian Sea, south of Catania. This source (with a dip angle of 54°) is compatible, at surface, with the Scicli-Ragusa-Monte Lauro transcurrent fault, which is 12-14 km to the west.
    
    Montone et al. (2004)
    They present an updated stress data compilation for the Italian region. The minimum horizontal stress in the Hyblean region is oriented ENE-WSW, consistently with the direction of Africa-Europe convergence.
    
    Musumeci et al. (2005)
    They analyze the spatial distribution of 414 earthquakes, recorded from 1994 to 2002 in southeastern Sicily. They computed focal mechanisms for the 70 best located events, by combining P-wave polarities with S-wave polarizations. The depth of seismicity extends to about 30 km, with hypocenters mainly concentrated in the 15-25 km depth range, and no located events in the very shallows region (0-3 km). Events with significant magnitude concentrate along the northernmost part of the Scicli-Ragusa fault system, where left-lateral motion on north-northeast fault planes are predominant. The results highlighted a region governed mainly by a north-northwest -south-southeast compressional stress regime.
