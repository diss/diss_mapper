**COMMENTS**

This Composite Source straddles the northeastern piedmont of the Abruzzo region, from the northern border of the Gran Sasso ridge (west) to the south of the city of Teramo, roughly parallel to the Vomano River. It represents the northernmost expression of the oblique to right-lateral strike-slip system that affects the central and southern Adriatic foreland. This Source is a S-dipping, high-angle fault bounding the southern part of the Central-Northern Apennines basal decollement.

Historical and instrumental catalogues (ISIDe Working Group, 2007; Guidoboni et al., 2019; Rovida et al., 2021) show scarce seismicity in this region, but for the notable 5 September 1950 (Mw 5.7) Gran Sasso earthquake, that has occurred in the eastern sector of this, and for its eastern replica on 8 August 1951 (Mw 5.3). For an in-depth analysis of seismogenesis in this region, the reader can refer to the individual source of the 1950 earthquake in this Database.

This Source represents the western and deeper section of a broader discontinuity along-strike, recognized by seismic reflection data offshore. Its presence is suggested by the quite abrupt termination of the deep-seated seismicity associated with the subducting Adria plate south of the Gran Sasso massif.

Field evidence in the area is scarce, while subsurface data (see Scisciani et al., 2001) suggest the presence of a deep-seated, blind set of structures whose westernmost segment has been associated with the 5 September 1950 earthquake. To the west, this source borders the shallower, NW-SE trending, outer extensional axis of the central Apennines.
