﻿    **COMMENTS**
    
    The Irpinia fault is certainly one of the best known sources of the Database. Its location and geometry are well defined on the basis of the 1980 earthquake.
    The 1980 earthquake is made up by a series of three mainshocks occurred within 40 seconds. They are generally referred to as 0 sec, 20 sec and 40 sec shocks.
    General consensus exists about a NE dip direction of the first two shocks and a SW dip direction of the third shock. This source represents only the segment responsible for the 20 sec shock.
    
    The main unresolved problem about this source is the lack of a geological record of the 1694 earthquake in the trenches excavated at Piano di Pecore and Piano di San Gregorio Magno. The 1694 event was previously considered the direct ancestor of the 1980 event. Several hypotheses can be put forward to explain this circumstance: a) the 1694 earthquake occurred on the Irpinia Fault but its intensity was overestimated, which means that it was not large enough to produce surface faulting. If this is the case, there is a chance that also other earthquakes that occurred in the same period and were studied with the same approach suffer from a similar problem. They would therefore need some reconsideration;
    b) the 1694 earthquake did not occur on the Irpinia Fault but on a different structure with a geometry capable of producing a damage pattern very similar to that of the 1980 earthquake (e.g. the antithetic fault that is presumed to have ruptured in the 40 sec sub-event could be a section of a longer structure that ruptured in 1694).
    
    The average recurrence time obtained by trenching analyses is much longer than that commonly expected on the basis of the Italian historical record of seismicity. This discrepancy should be seen as mainly due to the fact that by definition paleoseismology yields the true recurrence interval for an the individual source. A set of individual sources could concur (possibly even in a clustered fashion) in producing damage in the same general area, producing an apparently shorter recurrence interval.
    
    
    **OPEN QUESTIONS**
    
    1) Did the 1694 earthquake rupture the Irpinia fault? If this is the case, did it produce surface faulting? Does the geological evidence for this earthquake exist? 
    
    2) Do the two segments that form the Irpinia source (associated with the 0 sec and 20 sec sub-events) always rupture synchronously, as seen in 1980 and as suggested by paleoseismology? And what is the role of the 40 sec sub-event source?
    
    3) Does the source have a listric geometry in its southern part?
    
    4) What is the age of inception of the activity of the Irpinia source? Is the poor geomorphic expression of this source due mainly to its youthfulness?
    
    
    **SUMMARIES**
    
    FAULT GEOMETRY
    
    Bollettinari and Panizza (1981)
    Immediately following the November 23, 1980 earthquake they surveyed the epicentral area looking for coseismic effects on the local geology and geomorphology. They describe a 2 km-long, WNW-trending surface fault, producing about 50 cm of subsidence of the northern part of the Pantano di San Gregorio Magno plain and of the northern slope of Difesa di Ripa Rossa.
    
    Carmignani et al. (1981)
    These investigators study the main direction of the surface fractures related to the 1980 earthquake. They also describe NW-to WNW-trending fractures cutting the north-eastern side of Mt. Marzano -Mt. Sette Cuponi massif and Piano di Pecore plain for a total length of about 500 m and producing a maximum vertical offset of 1m. They interpret them as an effect of differential compaction which is not directly related to the slip of the seismogenic fault at depth.
    
    Del Pezzo et al. (1983)
    On the basis of the analysis of seismograms and strong motion data of the 1980 earthquake, they suggest a multiple rupture process composed at least of three sub-events.
    
    Deschamps and King (1983, 1984)
    These investigators calculate the fault plane solution for the 1980 Earthquake. They obtained a normal faulting mechanism with a component of left lateral movement on a high-angle NW-trending NE-dipping plane. They also identify three regions of high concentration of aftershocks separated by areas of reduced seismicity, the northern of which corresponds to the Sele river gap.
    They also speculate on the lack of clear surface faulting and presence of different fault mechanism of the aftershocks.
    
    Westaway and Jackson (1984)
    They identify and describe in detail two distinct sections of surface rupture: the northern is 10 km-long and bounds the north-eastern side of Mt. Marzano; the southern is 3 km-long, is located in the Pantano di San Gregorio Magno plain, and includes some of the fractures described by Carmignani.
    On the basis of the consistence between these observations and seismological evidence they suggest that these surface faults are of primary tectonic origin.
    Because of the location of the coseismic ruptures with respect to the Mainshock, they suggest that the rupture nucleated at a depth of 10 km on a high-angle NE-dipping NW-striking planar fault.
    
    Crosson et al. (1986)
    On the basis of the aftershocks distribution, the pattern of strong ground motion, focal mechanism of the mainshock and of quantitative modelling of the pattern of coseismic elevation changes of two levelling profiles, they reconstruct the complex rupture history of the 1980 earthquake. Their model predict two high-angle, NW and NE-trending perpendicular normal faults. In their hypothesis the 40 sec sub-event occurred on the NE-trending fault.
    
    Westaway and Jackson (1987)
    By means of different studies they show that the 1980 earthquake involved a complex sequence of fault ruptures with at least six sub-events. The first four events occurred within 10 sec of the origin time on three distinct NW-striking, NE-dipping, high angle fault sections, whereas the two later events occurred on two low-angle, NE-dipping normal faults located at the base of the seismogenic zone.
    
    Funiciello et al. (1988a, b) and Pantosti and Valensise (1990)
    These papers describe in detail the geometry of the 1980 surface fault. They extend the rupture recognised by Westaway and Jackson to as much as 38 km, from Monte Caravella to San Gregorio Magno. Scarp heights ranges between 0.4-1 m. The authors associate the main sub-event to a N315° steeply NE-dipping located between M. Caravella and M. Carpineta, the 20 sec sub-event to a N300° steeply NE-dipping plane located in San Gregorio Magno plain, and the 40 sec sub-event to an antithetic high-angle SW-dipping fault located about 10 km away the main rupture. They interpret the surface faulting discontinuities between the different segments of the main fault as mixed relaxation-geometric barrier.
    
    Bernard and Zollo (1989)
    They re-analyse all the data available on the 1980 earthquake and describe the kinematics of the process of rupture as consisting of three main episodes. They suggest that the rupture started near the north-eastern edge of the Mt. Marzano segment, propagated to the north on a high-angle NE-dipping fault, then the 20 sec sub-event occurred on a low-angle NE-dipping normal fault located between 8 and 12 km of depth and associated with secondary faulting on a steeper plane reaching the surface in the San Gregorio Magno plain. Following their interpretation the 40 sec sub-event nucleated on an high-angle SW-dipping antithetic fault.
    
    Valensise et al. (1989)
    On the basis of geodetic observations, depth distribution of aftershocks and localisation of nucleation points, they analyse the complex time-history for the 1980 events. According to them the main-shock was composed of three sub-events (0 sec, 20 sec and 40 sec), the first two of them nucleated on NW-trending, NE-dipping high-angle normal faults and produced surface faulting. Conversely the 40 sec sub-event occurred on an antithetic SW-dipping structure striking parallel to the main fault which did not produce surface faulting.
    
    Amato and Selvaggi (1993)
    By means of a computed three-dimensional velocity model of the volume affected by the main-shock and by the aftershocks of the 1980 earthquake they suggest that the rupture started at a depth of about 10 km on a 60° NE-dipping plane that steepens to vertical in the upper 6-7 km. They also interpret the antithetic fault, responsible for the 40 sec sub-event, as a reactivated NE verging thrust inherited by the pre-Pliocene compressional tectonics.
    
    Pingue et al. (1993)
    They model the 1980 source from levelling data. They obtained a source that consists of three fault sections each correlated to a sub-event. Because the 0 sec sub-event was composed of three rupture pulses they propose for it a 25 km-long, N317° striking, high-angle fault plane, that would be the summation of the contribution of all these pulses. They also suggest that the 20 sec sub-event involved a low-angle NE-dipping fault and that the 40 sec event occurred on an high-angle SW-dipping antithetic fault, similar to that proposed by Pantosti and Valensise (1990).
    
    Westaway (1993)
    On the basis of a re-examination of P-wave arrival time data he relocates the nucleation point of the initial fault rupture of the 1980 earthquake and describes the kinematics of the faulting process. By means of analysis of aftershock location and of ground acceleration evidence he suggests a fourth NW-trending fault rupture near Castelfranci north-west of the northern end of the Picentini fault; he also hypothesizes that the 20 sec sub-event involved a low-angle NE-dipping fault at the base of the brittle upper crust.
    
    PALEOSEISMOLOGY
    
    Pantosti et al. (1989)
    They provide the preliminary results of trench studies conducted at Piano di Pecore site. They recognise four paleo-earthquakes occurred in the last 6700 years and provide a preliminary slip rate of 0.4 mm/yr and average recurrence time of 1700 years for the Irpinia fault. On the basis of the close similarity between the deformation pattern of these paleo-events with that of the 1980 earthquake, they suggest that this earthquake is characteristic for this fault. They did not found evidence in the trenches for the 1694 earthquake, commonly considered the twin of the 1980 earthquake. On this basis they suyggest that the 1694 event occurred on the antithetic structure responsible for the 40 sec sub-event. 
    
    D'Addezio et al. (1990, 1991)
    On the basis of detailed analyses and logging of two trenches excavated at Pantano di San Gregorio Magno they recognise the geologic records of four paleoevents, preceding the 1980 earthquake; radiometric dating of organic samples taken from the sedimentary sequence was used to constrain the age of occurrence of paleoearthquakes. A slip rate of 0.3 mm/yr and average recurrence time of about 2000 years, in agreement with the results obtained at Piano di Pecore site are estimated for the San Gregorio fault section. The authors point out that the occurrence of surface faulting events in these two sites with similar dislocation magnitude and geometry, strengthen the idea that the 1980 earthquake can be considered characteristic for the Irpinia Fault.
    
    Pantosti et al. (1993a, b) 
    By using all the ages ranges of occurrence for the paleo-earthquakes recognised in the Irpinia trenches along with the reviewed amount of displacement observed in the trenches, they suggest that the 1980 earthquake is characteristic for this source and that the three sections of the fault that produced surface faulting rupture always synchronously. They also re-evaluate the slip rate and the average recurrence time for the Irpinia Fault, giving respectively 0.17-0.60 mm/yr and 2150-3140 yr with about 2000 years as preferred estimate for the average recurrence time. They confirm the lack of evidence for the 1694 earthquake surface faulting in the trenches. Assuming an average fault dip of 60° they evaluate an extension rate contribution of the Irpinia Fault close to 0.2 mm/yr; on the basis of stratigraphic consideration and assuming that the slip rate has been constant during Holocene, they calculate that the inception of the activity of this fault should postdate 0.7-1.0 Myr.
