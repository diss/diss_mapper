﻿    **COMMENTS**
    
    The northern Marche coastal zone is presently affected only by low seismic activity, but damaging earthquakes struck this area in the recent past (e.g. in 1930, Senigallia). A NW-SE trending minimum stress axis and a NE-SW trending maximum compressional stress axis were shown respectively by bore-hole breakouts and earthquake focal mechanisms (Mariucci et al., 1999; Frepoli and Amato, 1997). The available field geological observations are still inadequate to devise a fully convincing correlation between the occurrence of earthquakes and realistic seismogenic sources.
    However, the most realistic hypothesis that can be envisaged to-date on the basis of the local geologic setting considers the blind thrust-faults located at the leading-edge of the Apennine accretionary prism as the main active and probably seismogenic faults in the area. In addition, it seems likely that two parallel fault alignments of this sort exist along the northern Marche offshore.
    
    The analysis of two seismic reflection profiles (AN334-84 & B84-485-R-SW-84 and BR_6) provided by the Marche Region and located in the northwest offshore of the Conero promontory, contribute to better define the geometry of the source and the structural style of the offshore of the Conero area. The seismic data confirm that the structural style of the offshore area is the same of the on-shore sectors and that it is characterise by the presence of two different kind of compressive structures. There exist deeper thrusts involving all the carbonate succession generating major wave length anticlines and shallower thrusts, developed in the Neogene succession, which produce minor anticlines. The seismogenic fault may be correlated with a major blind thrust-plane which drives the growth of an anticline in the near offshore.
    Such fault may form a splay in the middle of a longer thrust front, running all along the Marche offshore.
    
    This Source is correlated with the 23 December 1690 'Anconetano' earthquake. Its location relies on the distribution of intensity data points, which is in good agreement with geological observations at regional and local scales.
    The fault geometry at depth is based on the results of the conversion in depth of the interpreted two seismic lines.
    The location of the hypocenter in the offshore implies a higher value of the magnitude with respect the value calculated on the basis of the macroseismic data.
    Consequently the size of the 1690 source conforms to what is predicted by empirical relationships between length/width and the supposed magnitude for the 23 December 1690 'Anconetano' earthquake.
    
    
    **OPEN QUESTIONS**
    
    1) This source suggests the presence of an active thrust fault in the Adriatic offshore.
    Could this fault belong to a longer fault system?
    
    2) How many parallel active thrust systems exist along the Adriatic coast?
    
    3) Are these active fault systems entirely seismogenic?
    
    
    **SUMMARIES**
    
    Di Bucci and Mazzoli (2002)
    The researchers present a review of geological and geophysical data available for the Marche coastal region, integrated with new data from the analysis of seismic reflection lines calibrated with deep well logs. In the Northern Apennines they recognize active NE -SW extension in the axial portion of the chain and the regional uplift as well. Moreover, for the external zone they propose that thrusting and related folding in this area ceased in the Early Pleistocene, in contrast with the hypothesis of active thrusting related to a subducting slab beneath the chain. This interpretation is based on the analysis of a number of reflection seismic lines, where the upper part of Quaternary deposits appears undeformed. To perform these specific considerations on Quaternary tectonics the investigators reconstruct a synthesis of Quaternary stratigraphy (1000 m thick) from well log study and detect with detail that succession on seismic reflection lines. They consider the seismicity recorded in the external sectors of the Northern Apennines (Senigallia earthquake, 1930) due to the present-day motion of the Apulia-Adria block. In the described geodynamic context the seismotectonic behaviour observed in these zones is produced by the slab detachment and the associated unbending of the foreland plate to which the regional uplift and the NE-SW compression are related.
    
    Santini (2003)
    He studies 11 new fault solutions of crustal events, with 2.9<M<4.3, using data recorded by the national seismic network of INGV in the period 1990-2000.
    He analyses 9 earthquakes located in Northern Marche and calculates the fault plane solutions for these events finding that those are divided as follow: a) three solutions for pure normal-faults, b) two solutions for pure thrust faults, c) one for thrust fault with a small strike-slip, d) three solutions with unknown stress-field. Assuming that the earthquake focal mechanisms reflect the state of stress of Northern Marche, he proposes as result of his study the possibility of dividing Northern Marche region into three areas with different seismological evidences: a) the inner area of Apenninic belt, which is characterized by extension; b) the Adriatic on-shore, which shows evidence of active compression; c) the Adriatic off-shore which shows not very clear evidence of active compression.
