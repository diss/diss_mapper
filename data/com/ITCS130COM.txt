**COMMENTS**

This Composite Source represents a system of blind faults confined at the footwall of the Acquasanta thrust activated during the 2009 L’Aquila and 2016-17 central Italy seismic sequences (Bigi et al., 2013; Tondi et al., 2020; Buttinelli et al., 2021).

This SW-dipping normal fault system was formed during the Mesozoic and reactivated during the Miocene flexure of the foreland in response to the orogenic build-up (e.g., Tondi et al., 2020). Some of the faults formed in the previous tectonic regime were transported onto the Acquasanta thrust hanging-wall (i.e., the Monti della Laga – Gorzano fault, MLG), whereas their roots remained confined at the thrust footwall.

The interpretation of seismic reflection profiles in the area (Di Bucci et al., 2021; www.retrace3d.it) allowed to image the geometry of the Acquasanta thrust (Buttinelli et al., 2021), dipping toward W-NW. This evidence has shown that the southern segment of the MLG in the Campotosto Lake area is detached at shallow levels, thus indicating that the seismicity recorded in this area (9 April 2009, Mw 5.4, and 18 January 2017, Mw 5.7) has occurred below the Acquasanta thrust (Bigi et al., 2013; Buttinelli et al., 2021).

Historical and instrumental catalogues (ISIDe Working Group, 2007; Guidoboni et al., 2019; Rovida et al., 2021) show a very dense intermediate (4.5 < Mw < 5.0) to damaging seismicity within the region, like the 28 April 1646 (Mw 5.9) Monti della Laga, the 8 June 1672 (Mw 5.3) Monti della Laga, and the 7 July 1619 (Mw 5.3) Aquilano earthquakes.
