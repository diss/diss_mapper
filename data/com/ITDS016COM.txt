﻿This seismogenic source was first proposed by Caputo et al. (2007) based on geological evidence and geophysical prospecting.
