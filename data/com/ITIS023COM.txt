﻿    **COMMENTS**
    
    Among the two source models available in current literature, the NNW-striking, E-dipping normal fault proposed by De Martini (1996) and the WNW-striking, SW-dipping lineament proposed by Vezzani (1967), Bousquet and Gueremy (1968) and Marra (1998), we selected the first as representative of this Source.
    
    The Mercure Basin is located in a historical and instrumental seismic gap area and only recently two earthquake of moderate magnitude occurred (8 January 1988, Md 4.1, and 9 September 1998, Ml 5.5). The lack of historical and instrumental evidence for stronger earthquakes in the northern Pollino area is substantial and could be explained as due:
    
    a) to the incompleteness of the record, due to the scarcity of population and of settlements in the area (SGA, 1994; Valensise and Guidoboni, 1995);
    
    b) to the occurrence of earthquakes with inter-event intervals longer than the time span covered by the historical record;
    
    c) to a real absence of earthquakes, as the active deformation is adjusted aseismically because of particular local structural conditions.
    
    We suspect that the answer to the lack of seismicity in the area is a combination of point a) and b).
    
    The Mercure normal fault proposed by De Martini (1996) is in good agreement with the structural data and the tectonic evolution presented by Schiattarella et al. (1994).
    In addition this source shows several similarities with the Castrovillari fault, located a few km to the south. In contrast with this interpretation some investigators (Vezzani, 1967; Bousquet and Gueremy, 1968) have suggested that the WNW-ESE striking, SW dipping Castelluccio-Viggianello lineament has been active until Rissian time as a normal fault, or, according to Marra (1998), with a predominant left lateral strike slip component, but the data supporting this last hypothesis are very limited.
    
    
    **OPEN QUESTIONS**
    
    1) What is the main seismogenic source in the area?
    
    2) Is the Castelluccio-Viggianello Fault an obstacle for further northward propagation of the Mercure Fault, as suggested by the structural analysis? And thus is it a segment boundary or a transverse lineament?
    
    3) If the Castelluccio-Viggianello Fault is active along its entire length (17 km), is it a normal or a strike-slip element? 
    
    4) Can the Mercure Fault and the Castelluccio-Viggianello Fault slip at the same time for their entire length?
    
    5) Should we expect aseismic slip on the Mercure Basin Source?
    
    
    **SUMMARIES**
    
    Baratta (1895)
    This paper describes the May 28,1894 earthquake, that was strongly felt in a wide area between Basilicata and Calabria, thus including the Mercure basin.
    Reinterpreting the report from Ing. M. Maglietta, which was in Episcopia village at the time of the event, Baratta estimates a duration of the shock not exceeding 7-10 seconds. Diffuse damage to buildings, with no loss, was recorded in the villages of Viggianello, Rotonda, Castelluccio Inferiore and Episcopia. The damage distribution forms a NNE-SSW elongated ellipse with a 17 km-long maximum axis.
    
    Lona and Ricciardi (1961)
    These workers sample the fine portion of the lacustrine sediments (upper 175 m of the sequence) of the Mercure Basin. By means of pollen analysis they suggest a post-Mindel and pre-Wurm age for the well stratified silty-clay, sand and marl layers, indicating a middle-upper Pleistocene age for the whole fluvial-lagoonal infill.
    
    GE.MI.NA (1963)
    This paper presents a detailed study of the Mercure Basin planned to reconstruct the geometry of some lignite layers. The interpretation of the stratigraphic log of several boreholes has been used to draw a map of the isobaths of the top surface of the carbonatic basement.
    
    Vezzani (1967)
    This investigator presents a geological, mainly stratigraphical, overview on the deposits filling the Mercure basin. The stratigraphic section starts from the bottom with a thick (200 m in the western sector) conglomerate, interbedded with few lenses/layers of sand and silty-sand, all derived from the erosion of the ranges bounding the depression. Vertically and laterally well stratified silty-clay, sand and marl layers, containing freshwater mollusca, diatoms and ostracoda follow, indicating a low-energy depositional system. Within this second unit there are several lignite lenses, intensely exploited in the recent past. At the top of the section only angular detrital deposits, badly cemented in a clay-sandy matrix, are present especially near range fronts. In the conclusions the author suggests that the lineament/fault between Castelluccio and Viggianello, responsible for the opening of the basin, was active during and after the deposition of the younger lacustrine sediments.
    
    Bousquet and Gueremy (1968)
    These investigators study the neotectonic structures of the Mercure Basin, using both geological and morphological approaches. They show the existence of two episodes of quaternary deformations have been detected.
    The first episode, related to the activity of the WNW fault system, is responsible for the formation of the Mercure Basin and took place during the Gunz and the interglacial Mindel-Riss. The second one was probably subsequent to Rissian depositions but ended before the Wurm.
    
    Esposito et al. (1988)
    They describe the 8 January 1988, Ml = 4.1 earthquake, that took place in the Mercure Basin between Viggianello and Castelluccio Inferiore.
    The shock was clearly felt in the Basilicata, Calabria and Campania regions.
    A maximum intensity VI-VII MCS is estimated. The authors detected a large attenuation toward the south (Calabria region) and suggested an hypocentral depth within the uppermost 10 km of the crust for the medium size seismic events characterizing this area.
    
    Gasparini et al. (1988)
    They also investigate the 8 January 1988 Ml = 4.1 earthquake, that was the main shock of a 2 week-long sequence, located in the Mercure Basin, at the Basilicata-Campania border. A maximum intensity of degree VI MCS is assessed over a wide area. According to these investigators the anomalously high number (37) of localities with I = VI is probably due to the occurrence of a Md 3.7 earthquake in the Vallo di Diano area on 13 January 1988 (to the north of the Mercure Basin) and to the presence of several buildings already damaged by the much stronger 1980 Irpinia earthquake.
    
    Lazzari (1989)
    This worker focuses on the geomorphology and especially the gravitational phenomena in the Mercure Basin. The lacustrine deposits, locally disturbed by tensional structures, reach a maximum thickness up to 400 meters in the central part of the basin. The author highlights the presence of deep gravitational phenomena together with rotational landslides and several rock-falls all over the basin, with a peculiar gravitational activity in the northern-north-eastern sector. In the 1972 a huge detrital flow hit the village of Castelluccio Superiore. He suggests as a possible explanation for the strong gravitational movements both a recent tectonic activity and the high average amount of rain-fall per year (up to 1500 mm) usually concentrated (73%) between October and March.
    
    Schiattarella et al. (1994)
    These workers carry out structural and morphostratigraphic analyses in the Mercure Basin. The genesis and the evolution of the basin has been driven by two different tectonic stages, predating the deposition of the middle-upper Pleistocene lacustrine sediments. The first stage is occurred in a strike-slip regime, related to the activity of the Pollino left-lateral N120° fault system, and is responsible for the development of an half-graben, NE-SW oriented. The second stage, related to a NE-SW extension axis rotating clockwise through time, set the conditions for the growth of an endoreic basin, starting from the end of lower Pleistocene. The authors correlate the measured N150°-trending extensional joint sets together with N-S and E-W trending normal faults, all affecting the middle-upper Pleistocene lacustrine marls and cutting across the N120°-trending listric normal faults, to the last position of the extension axis, rotating clockwise. At present range fronts bounding the Mercure Basin do not seem to be controlled by tectonic movements, and the authors underline that the Castelluccio-Gallizzi N120°-trending fault is clearly sealed by middle-upper Pleistocene fluvial-lacustrine sediments.
    
    Storia Geofisica Ambiente (1994)
    This is an internal report for Istituto Nazionale di Geofisica (Roma) on the historical seismicity of the Pollino Range area. The whole area exhibits a low seismic activity with the exception for the Viggianello area, for which an oral tradition reports a strong event dated at the "death of Jesus Christ", that usually means a generic old time.
    
    De Martini (1996)
    This worker presents a structural and geomorphic analysis in the Mercure Basin area. The upper Pleistocene lacustrine marls are affected by NNW-SSE trending normal faults and by many N-S trending extensional joints. The same deposits, with the exception of the eastern sector of the basin, show a gentle tilt (2° to 10°) to the W. The present depocenter, characterised by anomalous aggrading behaviour, migrated ca. 5 km to the W with respect to the paleo-depocenter before the fluvial-lagoonal infill. The author describes several stream rotations towards the present depocenter, the best example of which is the 90° turn of the Battendiero river. Also the regional divide is clearly concave westward. Finally the author suggests that a 22 km-long blind (or hidden) normal fault (called Mercure Fault), striking N350°-335°, dipping 60° to the East and located in the westernmost part of the basin, could better explain all the data collected.
    
    Marra (1998)
    This paper presents a structural and geomorphic study of the intramontane Mercure Basin. The author attributes a critical role to the Castelluccio left-lateral N120° fault (part of the main Pollino fault system) in the evolution of the depression and in the deformation of the lacustrine infill. A first transtensive phase originated a pull-apart basin between two main left lateral strike slip faults; thus starting in middle Pleistocene time a transpressive phase followed, acting on the same structures, due to the inversion of the step. The author suggests that the second phase has been responsible for N-S trending, W verging thrusts, affecting the middle-upper Pleistocene lacustrine sediments. The N-S extensional joints together with NNW-SSE trending normal faults, measured in the upper section (silty-clay layers) of the lacustrine sediments are interpreted by the author as secondary effects related to and compatible with the transpressive phase.
