﻿    **COMMENTS**
    
    We propose the existence of this Source based on the evidence of the recent tectonic activity of the Pedeapenninic Thrust Front (Castellarin et al., 1985; Amorosi et al., 1996; Boccaletti and Martelli, 2004) and on subsurface data (Pieri and Groppi, 1981; Cassano et al., 1986; Boccaletti and Martelli, 2004) constrained by the occurrence of the 3 January 1505 earthquake.
    
    The Pedeapenninic Thrust Front is traditionally considered an active out-of-sequence thrust of the Northern Apennines belt, with a continuous trace along the Apennines foot-hills defined by geologic and geomorphic evidence of activity (e.g., Boccaletti et al., 2004). However, recent papers proposed that the geomorphic feature of the Pede-Apennines foot-hills is not the expression of a continuos shallow thrust fault (Picotti and Pazzaglia, 2008). According to this view, most of the regional deformation would be taken by a deep, blind thrust ramp responsible of the large scale geomorphology of the Northern Apennines foot-hills.
    
    We propose the following geometrical parameters for the source:
    - the strike is chosen according to that of the Pedeapenninic Thrust Front;
    - the fault dips towards the South, according to subsurface seismic profiling;
    - the rake is assumed to be pure thrusting, based on geodynamic considerations constrained by mesostructures affecting recent deposits of the area;
    - the down-dip width is obtained by seismic profiles and constrained by scaling relationships with the 1505 earthquake magnitude;
    - the minimum and maximum depth are constrained by subsurface geology;
    - the length is obtained by scaling relationships with the 1505 earthquake magnitude.
    
    This Source is associated with the 3 January 1505 earthquake (Mw 5.5) reported by CPTI (2004) and having its epicentral area between Zola Predosa and Bologna, while a re-evaluation of macroseismic data (Boschi and Guidoboni, 2003) shifts the epicentre a few kilometres to the NW and assess a Me 5.7 for it. Our source model is consistent with the highest damage, reported in both cases at Zola Predosa, Bologna and S. Lorenzo in Collina.
    In the same area, Boschi et al. (2000) report landslides and surface fractures, while Prestininzi and Romeo (2000) report also liquefaction at Zola Predosa.
    
    The location of the source is constrained to the east by the occurrence of another Me 5.4 earthquake on 20 January 1505 (reported by Boschi and Guidoboni, 2003), and to the west by the 20 April 1929 earthquake (CPTI, 2004).
    
    This source is consistent with evidence of recent activity of the Pedeapenninic Thrust Front south of Bologna, consisting of triangular facets, the presence of a growth anticline and a fault scarp affecting Holocene alluvial fans. The rivers flowing south of Bologna are characterized by high erosional activity and by the presence of three orders of uplifted terraces.
    
    
    **OPEN QUESTIONS**
    
    1) What is the average return period of this Source?
    
    2) What is the relationship between the 3 and 20 January 1505 earthquakes? Should the first event have triggered the rupture of another segment of the Pedeapenninic Thrust Front to the west?
    
    3) Does this part of the Pedeapenninic Thrust Front rupture in repeated events with M around 5.5 or can it rupture also in larger events?
    
    4) Is there any relationship between the 1505 earthquakes and those occurred to the east in 1505, 1779 and 1801 and to the west in 1929 along the Pedeapenninic Thrust Front?
    
    
    **SUMMARIES**
    
    Although no specific studies exist on this Source, information about the location, geometry and activity of the source, possible causative fault of the Mw 5.5 1505 Bolognese earthquake, can be retrieved from several papers regarding the recent activity of the Pedeapenninic Thrust Front and from subsurface geology.
    
    Castellarin et al. (1985)
    These authors interpret the Pedeapenninic Thrust Front as a high angle reverse fault system that represent the thrusting of the Apennines over the sediments of the Po Plain.
    According to the tectonic evolution of the Apennine buried front proposed by these authors, the thrust and fold system have propagated toward its outer part until the Middle Pleistocene, and subsequently most of the compressional tectonics have occurred to the south of the arc fronts and, most of all, along the Pedeapenninic area around Bologna. They also report the activity of the Pedeapenninic Thrust Front during the Pleistocene, testified, in the area of Bologna, by a 600-700 m vertical offset of the Quaternary marine succession.
    
    Amorosi et al. (1996)
    These authors date the uplifted terraces of the rivers South of Bologna by correlating them with those of the Reno valley. The higher terraces are dated about 18 ka and the intermediate ones about 13 ka. The correlation show the presence of a major discontinuity, linked to the activity of the Pedeapenninic Thrust Front.
    
    Boschi et al. (2000)
    This catalogue describes some effects on the environment in the epicentral area of the 1505 earthquake.
    It reports a large arc-shaped ground breakage at Zola Predosa and landslips in the surroundings of S. Lorenzo in Collina.
    
    Prestininzi and Romeo (2000)
    These authors report an earth slide at S. Lorenzo in Collina and both linear and punctual emission of water and mud from the ground (interpreted as liquefaction) at Zola Predosa as effects of the 1505 earthquake.
    
    Boschi and Guidoboni (2003)
    Through the re-evaluation of historical sources, these authors assess an epicentral intensity of 8 and an equivalent magnitude of 5.7 for the 1505 earthquake and compute an epicentre about 3 km to the NW of the CPTI one. They also report two Me 5.4 earthquakes occurred respectively on 31 December 1504 about 20 km to the NW and on 20 January 1505 with the epicentre at Bologna.
    
    Boccaletti and Martelli (2004)
    This map defines the Pedeapenninc Thrust Front as an active structure and provides also a summary of the evidences of its tectonic activity around Bologna. To the south of Bologna, it reports a growth anticline parallel to the Apenninc margin that deforms deposits dated from the Messinian to the Middle Pleistocene. These deposits result tilted and are sometimes affected by reverse faults. The anticline is characterized by a fault scarp with well developed triangular facets affecting Holocene alluvial fan deposits. The rivers flowing toward the north present high erosional activity and three orders of uplifted alluvial terraces. This map provides also a NE-SW oriented geological section (section D-D') passing through Bologna and a seismic profile that shows the basal surface of the alluvial deposits of the Po Plain (dated 45 ka) cut by the Pedeapenninic Thrust Front.
    
    Rovida (2004)
    The author analyses the effects on the environment produced by historical earthquakes occurred in the Po Plain, and investigates in detail a selection of the largest events in order to assess their seismogenic sources. He first compares the sources derived from macroseismic data alone using the Boxer code (Gasperini et al., 1999), with the mapped active structures buried in the Po Plain.
    Then, to constrain better their geometry, he studies in detail the geomorphic expression of these sources using elastic dislocation models and geomorphological observations.
    As a final results the author proposes 11 new seismogenic sources, responsible for M 5+ and higher earthquakes in the Po Plain.
