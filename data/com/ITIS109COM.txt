﻿    **COMMENTS**
    
    We propose the existence of this Source following the structural and geomorphological observations of Galadini et al. (2005).
    
    We segmented the Arba-Ragogna thrust on the basis of difference in the hanging wall structural elevations (between western and eastern sectors), and of slight different orientations of the structural grain.
    
    The eastern, larger segment of the Arba-Ragogna thrust hosts this Source, that is not associated with any historical/instrumental earthquake. As such this source represents a potential seismic gap for a M 6+ earthquake.
    
    Galadini et al. (2005) proposed a single seismogenic structure occuring along the Arba-Ragogna thrust, but they were not analysing earthquakes with a magnitude lower than 6.
    
    The two sources follow a trend that is more easterly than the sources located to the west along the Aviano Line. Moving eastwards from this area on, the structural configuration seems to be more complicated, since seismicity is more widespread diffused and more thrust fronts seems to be active contemporary.
    In fact, north of these sources a further Source is present, that falls along the EW-trending Periadriatic thrust, and is associated with the Me 5.8, 1794 Tramonti earthquake. According to Galadini et al. (2005), the Periadriatic thrust is active until Kobarid in Slovenia, where it interacts with the NW-SE trending, right-lateral structures of the Idrija fault system.
    
    
    **OPEN QUESTIONS**
    
    1) Can we assume that the segment boundary between the two sources is permanent? Or can the Arba-Ragogna thrust host more infrequent earthquakes of larger magnitude (as suggested by Galadini et al., 2005)?
    
    
    **SUMMARIES**
    
    Information on the location and geometry of this Source, can be obtained from several papers dealing with the neotectonic evolution of the Friuli and Veneto region.
    
    Galadini et al. (2005)
    The authors, on the basis of structural and geomorphological observations and of the interpretation of a dense grid of reflection seismic profiles, define the 3-D geometry of 10 seismogenic sources able to generate M 6+ earthquakes in the Veneto-Friuli plain. They associate some of these sources with historical earthquakes, mainly basing on the distribution of the highest intensity data points, compared with the source geometry and position.
    As regards the Arba-Ragogna thrust, which hosts the Sequals source, they do not associate it with any historical earthquake, but infer from geological/geomorphological consideration a vertical slip rate of about 0.2 mm/yr. The authors point out that also for the Arba-Ragogna thrust the evidence of recent activity consists of subsiding areas, uplifted terraces and of displaced late Quaternary deposits.
    
    Poli et al. (2008)
    Starting from the work of Galadini et al. (2005), the authors present an updated compilation of the seismogenic sources affecting NE-Italy. The main implementations are derived from a three steps procedure by which the authors:
    1- checked the consistency of the source dimensions, of their position and geometry and of some seismological parameters of the expected/associated earthquakes, using analytical and empirical relationships;
    2- scaled the dimensions of the sources proposed by galadini et al. (2005) with the average seismological stress drop of the Friuli 1976 seismic sequence, assumed to be characteristic in this region and for thrust faulting earthquakes; and
    3- analysed the intermediate size historical seismicity (Mw between 5.5 and 6.0) and the possible association with faults belonging to the identified active thrust systems of the Eastern Southalpine Chain front or to more internal faults (both thrust and strike-slip faults) not included in the dataset of Galadini et al. (2005).
    
    Burrato et al. (2008)
    The authors present an overview of the seismogenic sources of northeastern Italy and western Slovenia, included in the version 3.0.2 of the Database of Individual Seismogenic Sources. They describe the method and the data used for their identification and characterization, discuss some implications for the seismic hazard and underline controversial points and open issues. They also reinterpret a large set of tectonic data and develop a segmentation model for the outermost Eastern Southalpine Chain thrust front. According to the picture given by DISS, in the Veneto-Friuli area (NE Italy), destructive earthquakes up to Mw 6.6 are generated by thrust faulting along N-dipping structures of the Eastern Southalpine Chain. Thrusting along the mountain front responds to about 2 mm/a of regional convergence, and it is associated with growing anticlines, tilted and uplifted Quaternary palaeolandsurfaces and forced drainage anomalies. In western Slovenia, dextral strike-slip faulting along the NW-SE trending structures of the Idrija Fault System dominates the seismic release. Activity and style of faulting are defined by recent earthquakes (e.g. the Ms 5.7, 1998 Bovec-Krn Mt. and the Mw 5.2, 2004 Kobarid earthquakes), while the related recent morphotectonic imprint is still a debated matter.
    On the basis of morphological and structural observations, they propose to segment the Arba-Ragogna thrust, and that its eastern termination hosts the Sequals source. According to the authors, and following the hypothesis of Galadini et al. (2005), this source is not associated to any historical/instrumental earthquake.
