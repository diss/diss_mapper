﻿    **COMMENTS**
    
    As of now, no studies in the literature addressed specifically the 20 Mar 1731, M 6.3 earthquake. This is in part due to the fact that only recently a new attention has been drawn on the active deformation in the Apulian foreland of the Southern Apennines, such attention being due essentially to the aftermath of the 31 Oct.-1 Nov. 2002 Molise sequence.
    
    In fact, the area was relatively poorly inhabited at the time the earthquake took place and, although the macroseismic information is sufficiently rich to provide good clues as to location and magnitude, no other evidence is available concerning the kinematics, nor has it been proposed in the literature.
    
    However, subsurface data, in part published, that image the top of the Apulian carbonate platform highlighted the presence of numerous structures trending E-W. We believe that the 1731 earthquake was caused by a deep-seated, blind fault like the ones we observed in the subsurface data. Such fault would be re-activated by the same stress field responsible for the earthquakes that were recently documented in the foreland, that is the above named Molise sequence and, previously, the 5 May 1990-26 May 1991 Potenza earthquakes.
    
    Also, the intensity distribution is suggestive of a seismogenic source with a circa E-W strike. For such source we propose a kinematics compatible with that of the above mentioned earthquakes.
    
    
    **OPEN QUESTIONS**
    
    1) Does the seeming lack of any surface evidence in the area hint at the possibility that the 1731 earthquake was caused by a deep-seated, blind fault?
    
    2) How much can an elongated mesoseismal area, characterized by a low intensity decay, be indicative of a deep source?
    
    3) Can normal faulting be entirely ruled out in this region, in favour of right-lateral strike-slip reactivation of older E-W trending structures?
    
    
    **SUMMARIES**
    
    Sella et al. (1988)
    Through interpretation of a broad network of industrial seismic lines and wells, they show a thorough reconstruction of the subsurface geology and structural pattern beneath the Apulian sector of the Southern Apennines. In this area, they show a set of circa E-W striking structures, east of the Monte Taverna oilfield, which rupture the top of the Apulian carbonate platform. Towards the south, these faults are accompanied by a number of smaller en-echelon structures along a roughly E-W trend.
    
    Pieri et al. (1997)
    They describe three main blocks with different degrees of uplift: Gargano, Murge, Salento.
    Murge and Salento are separated by a large E-W trending deformation zone, corresponding to the triangular Brindisi depression to the east and to a series of small uplifted blocks and depressions to the west. The latter feature is particularly evident between Matera and Mottola, where transpressional and transtensional structures are clearly associated with mainly dextral strike-slip faults; these structures show pre-, syn-and post-depositional activity referred to the outcropping late Pliocene-early Pleistocene deposits.
    These authors also describe the Murge Alte and Murge Basse Grabens, which mainly formed before the sedimentation of the Plio-Quaternary deposits of the Murge. Kinematic indicators show mainly dip-slip motion, but oblique-slip evidence has also been observed.
    Related morphological depressions are not completely sealed and/or filled by Plio-Quaternary deposits.
    According to these authors, Middle-late Quaternary activity was best recorded in the Northern Murge, where marine terraced deposits are relatively thick and widely distributed.
    
    Morelli (2002)
    This worker studied and summarised the main regional E-W striking structures offshore the Puglia coast (SE Italy), using seismic exploration lines and wells. Since the bottom of the Jurassic reflector and up to the bottom of the Quaternary, he shows three main such E-W systems. Starting from the north, one is the "Sistema Sud-Garganico", i.e. the regional structure offshore the Mattinata Fault in the Gargano Promontory. To the south, he presents the "Sistema di Bari", which seems of a more limited extent along strike. More to the south, he discusses the "Sistema di Monopoli", well developed along strike.
