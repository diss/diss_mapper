﻿This seismogenic source was first proposed by Benedetti et al. (1998) based on geological and geomorphological evidence.
