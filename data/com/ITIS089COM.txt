﻿    **COMMENTS**
    
    The area between the inner Abruzzi and Molise region is of crucial importance in the seismotectonics of central and southern Italy. To the south, at least three destructive earthquakes (9 Sept 1349, 5-30 Dec 1456 and 26 July 1805) and, to the north, the 7 May 1984 earthquake and the Aremogna-Cinque Miglia active fault (see: D'Addezio et al., 2001, J. Seismol., 5, 181-205) depict the seismotectonics of the area. Moreover, while the seismogenic faults known in Abruzzo (including the one responsible for the 1915, M 7.0 Fucino event) verge to the SW, in Molise the correspondingly relevant seismic sources dip toward the NE. Consequently, one can well hypothesize the presence of an accommodation zone (either brittle or ductile) between inner Abruzzi and Molise.
    
    The Carpino -Le Piane (CLP) active fault marks the northern end of the NE-verging, normal faulting responsible for the large part of seismic moment release in the Southern Apennines and, although its strike is slightly different from the adjoining, much larger normal fault responsible for the 26 July 1805, M 6.6 earthquake, the CLP fault clearly obeys to the same stress field.
    
    Therefore, the identification of the CLP Basins Fault System as an active one enriches the knowledge base concerning the boundary between the Central and Southern Apennines. Moreover, as it has been remarked in the literature, it is also relevant in terms of seismic risk, since it occurs in the vicinity of the town of Isernia, an important centre of the Molise region.
    
    
    **OPEN QUESTIONS**
    
    1) Is the active CLP fault provided with a seismogenic potential?
    
    2) What is the structural relationship among the CLP fault and those responsible for the 1984 and 1456 earthquakes?
    
    3) Was the CLP fault responsible for the 2001 Isernia sequence (Milano et al., 2005)?
    
    
    **SUMMARIES**
    
    Di Bucci et al. (2005)
    These authors studied the geological history and growth of the Carpino-Le Piane Basin Fault System (which they named CLPBFS), with respect to the neighbouring Isernia and Surrounding (named ISFS) and Boiano Basin (named BBFS) extensional Fault Systems.
    Through dating methods, these authors showed that the BBFS was already active 649 ± 21 ka BP and that the ISFS was active at least 476 ± 10 ka BP, whereas the activity of the CLPBFS started later than 253 ± 22 ka BP, and very probably as recently as ca. 28 ka BP. The authors conclude that such ages, which they combined with structural data, indicate that the inception and development of the CLPBFS could be related to the stress changes caused by earthquakes occurring on the BBFS (like the 26 Jul 1805, Mw 6.6 event).
