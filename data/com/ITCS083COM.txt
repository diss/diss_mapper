**COMMENTS**
    
This Composite Source is an extensional belt that straddles the region near the Serchio R. valley, within the Ligurian and Tosco-Emilian Apennines. This Source can be seen as the possible northwestern prolongation of the NE-dipping Etrurian Fault System, marking the northwestern extensional border of the Northern Apennines.
    
Historical and instrumental catalogues (Boschi et al., 2000; Gruppo di Lavoro CPTI, 2004; Pondrelli et al., 2006; Guidoboni et al., 2007) show a dense intermediate (4.5 < Mw 5.0) to damaging seismicity within the area, besides the key damaging earthquakes of 11 April 1837 (Mw 5.6, Alpi Apuane), and the destructive 7 September 1920 (Mw 6.5) event.
    
This source includes a set of faults that possibly mimick the inherited structures that border the extensional basins, and that have been in part coseismically reactivated during the earthquakes we report. This would particularly fit the southern sector of the area, where seismic moment release has been larger.
    
Some segments of this Source have been associated with the key earthquakes of this region. For an in-depth analysis of seismogenesis in this region, the reader can refer to the Individual Sources in this Database.
    
The strike of the Source was based on that of the mapped structures. The dip was based on subsurface data and geometrical considerations. The rake represents pure extension, based on geological observations. The minimum and maximum depth were based on subsurface data and on geometrical considerations. The slip rate was inferred from geological observations in adjacent structures that share the same tectonic environment with this Source.
