    **COMMENTS**
    
    This Source is a S-dipping blind thrust fault. The strike, dip and rake of the fault were chosen following the focal solution of the 23 December 2008, Mw 5.4 earthquake contained in the Centroid Moment Tensor Catalog, available at the web site http://www.bo.ingv.it/RCMT/searchRCMT.html.
    
    The main shock occured at 23 km of depth, and the most of the shocks are located below 20 km depth (data from ISIDE, Italian Seismic Bulletin, Istituto Nazionale di Geofisica e Vulcanologia, available online at http://iside.rm.ingv.it/iside/standard/index.jsp). 
    
    The earthquake was felt over a wide area due to its depth.
    
	We tentatively propose that the causative source of this earthquake is located beneath the interface between the Apennines chain in the upper plate and the Adria lithosphere in the lower plate.

    
    **OPEN QUESTIONS**
    
    1) Is this Source in a structural position similar to that of the Source responsible for the 14 September 2003 earthquake, located near Bologna, about 80 km to the SE?
