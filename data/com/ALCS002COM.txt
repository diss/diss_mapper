**COMMENTS**
    
An over 1,000 km-long contractional belt runs along the eastern margin of the Adriatic Sea from the Alpine domain (to the North) to the Kefallonia-Lefkada Fault (to the south). The various part of this fold-and-thrust belt are known, from north to south, as Dinarides, Albanides and Hellenides and result from the continental collision between the European and African plates. Along this margin the SW-NE convergence between the two plates exhibits a strain rate of ca. 30 nanostrain/y (Hollenstein et al., 2003; Serpelloni et al., 2005).
    
Seismicity mostly concentrates in the Albanides and Hellenides. The outer portion of these chains is partly located offshore and characterized by a number of thrust fronts that all appear to be currently active. The available focal mechanisms indicate a SW-NE maximum horizontal stress axis (Papazachos et al., 1999); reverse faulting dominates, even if some smaller strike-slip events are occasionally recorded (Vannucci et al., 2004; Pondrelli et al., 2006).
    
We propose that this Source is on one of the outermost thrust fronts that characterize the southernmost portion of the continental collision between the African and European plates.
    
This Composite Source is presently characterized by shallow seismicity with many 4 < M < 5 events. Several large events (up to M=6.8) are also known from historical catalogues. Its average strike and rake computed by Papazachos et al. (1999) from focal mechanisms are consistent with plate motion vectors derived from GPS data and with the orientation of the thrust front reported by many investigators. Its dip was inferred from focal mechanisms (Global CMT catalogue) and from the average dip of the thrust faults detected on seismic profiles acquired for hydrocarbon exploration (e.g., Graham Wall et al., 2006).
We used GPS data to estimate the slip rate of this Composite Source.
