**COMMENTS**
    
This source is an E-W striking, regional strike-slip fault, showing right-lateral motion and straddling a short sector offshore of the central-southern Adriatic Sea. It can be seen as a part of a major E-W striking, active strike-slip fault system covering the Italian south-eastern sector of the Adriatic foreland.
    
The Gondola Fault Zone is a ca. 60 km long, E-W striking poly-phased structure that dissects the southern Adriatic continental shelf and slope. Although its activity can be traced back to the Mesozoic (De Alteriis, 1987; De’ Dominicis and Mazzoldi, 1995; Morelli, 2002, and references therein), it has long been considered a tectonically inactive structure, also due to the mild and sparse seismicity on and surrounding the region (Gruppo di Lavoro CPTI, 2004).
    
Although the Gondola Fault Zone shows only feeble instrumental seismicity, a number of moderate to intermediate earthquakes (5 < M < 6) have occurred in the Gargano Promontory, as recorded by the historical and instrumental catalogs; some of these events have been associated with segments of the Mattinata Fault, an onshore well-studied crustal structure, showing a tectonic history remarkably similar to that of the Gondola Fault Zone (Di Bucci et al., 2006). However, Molin (1985) and Tinti et al. (1995) suggest that, for instance, the 10 August 1893, Mw 5.4 earthquake may have taken place offshore, in particular in view of anomalous, temporary sea-level features and turbulence on the sea surface that were documented by contemporary references.
    
Finally, recent studies (Ridente et al., 2008) that employed very high resolution seismic data (Chirp sonar lines) have unveiled that the Gondola Fault Zone shows clear evidence of activity at least since the Middle Pleistocene and, locally, up to recent times. In some cases, active fault segments, as long as 10-20 km, rupture up to the Holocene and the seabed, accompanied by several gas escape features.
Therefore, we believe that the shallow section of the Gondola Fault Zone behaves as an actively deforming system, with fault segments that locally rupture the seabed, strongly suggesting a right-lateral strike-slip motion.
    
We obtained strike, dip and rake from seismotectonic data in Ridente et al. (2008), Di Bucci et al. (2009), and Fracassi et al., (2012).
