﻿    **COMMENTS**
    
    The Crati Valley fault system is located between the Catena Costiera and the Sila Massif. The southern sector of the Crati Valley is oriented N-S, about 50 km long, and delimited on both sides by normal fault systems. Some of these are recognized as active in the literature (Tortorici et al., 1995; Galadini et al., 2000; Tansi et al., 2005a, 2005b).
    
    We propose the existence of this Source based on data regarding the recent tectonic activity of the southern sector of the Crati Valley constrained by the occurrence of the 12 october 1835 earthquake (Mw 5.9).
    
    We propose that this Source is located along the eastern side of the valley, in the southernmost portion of it.
    
    The geometry of this source is constrained by geological and geomorphological observations, in particular the drainage pattern and the distribution of the radon anomalies in the area. The drainage pattern shows that the Crati River flows closer to the eastern side of the Valley, thus suggesting a possible active subsidence of that side of the valley. The orientation of the N-S trending normal fault shows a clear correlation with the geometry of radon anomalies in soil gas (according to Tansi et al., 2005b).
    The location of this Source is constrained to the north by the occurrence of another Mw 5.8 earthquake on 14 July 1767 (CPTI, 2004), and to the south by the presence of a transverse structure known in literature as Vette Line.
    This lineament was the locus of a series of earthquakes with magnitude between 4 and 5 that occurred in the nineteenth and twentieth centuries (CPTI, 2004).
    Another event having a Mw 5.2 (CPTI, 2004) took place in 1556 along the Vette Line.
    
    Maximum macroseismic intensity was X and macroseismic epicenter is located immediately NE of Cosenza (DBMI04, Stucchi et al., 2007). Macroseismic field of the 1835 event is consistent with the geometry of the proposed shallow source.
    
    We propose the following geometrical parameters for the source:
    - the strike is chosen according to that of Crati Valley fault system;
    - the fault dips towards the West, according to geological and geomorphological data;
    - the rake is assumed to be pure normal faulting, based on geodynamic considerations and focal mechanisms of the area;
    - the length and the down-dip width are constrained by scaling relationships with the 1835 earthquake magnitude.
    
    
    **OPEN QUESTIONS**
    
    1) What is the average return period of this Source?
    
    2) What is the relationship between the 1835 and the 1767 earthquakes? Is the ca.
    50 years time span sufficiently short to postulate a triggering?
    
    3) Does this part of the Crati Valley fault system rupture in repeated events with M around 5.9 or can it rupture also in larger events?
    
    
    **SUMMARIES**
    
    Carobene and Damiani (1985)
    They carried out a detailed geological survey on the lower Pleistocene, mainly marine, deposits outcropping in the Crati valley, around the Luzzi village. The deposits, characterized by a cyclic sequence consisting of rudite, arenite and lutite, transgressively overly the crystalline basement of the western slope of the Sila Massif. A geological longitudinal section drawn by these authors show that both Pleistocene deposits both basement are deformed and strongly controlled by tectonics.
    
    Tortorici et al. (1995)
    These workers carried out a structural analysis in the Catena Costiera mountain range and the Crati Valley. They recognize and describe a Crati Valley fault system, represented by three main normal fault segments, with the fault planes dip steeply towards the east and develop at the base of the Catena Costiera mountain range. This system extends along a N-S direction for about 40 km. Their results suggest that the Crati Valley fault system was active during the Pleistocene, and a long-term minimum slip rate of about 0.8-1.0 mm/y during the last 700 ky.
    
    Molin et al. (2004)
    The authors examine the tectonic geomorphology of La Sila with map, DEM, and field data.
    They describe the entire Sila Massif separated from Catena Costiera by the Crati River Valley, and investigate some streams in the western and northern portion of Sila Massif, including Mucone and Gidora rivers. They show the major lineaments in the context of these rivers. These workers also show the stream long profiles of Mucone and Gidora rivers, the major tributaries of the Crati River. They observe that these rivers are underlain by uniform rock type, and the knickpoints on the Gidora and Mucone rivers are close to mapped N-S lineaments.
    
    Tansi et al. (2005)
    These workers measure radon concentrations in soil gas in the south-eastern part of the Crati Valley. Their results show a clear correlation between the N-S geometry of radon anomalies and the orientation of main fault systems. They observe that epicentral zones of instrumental and historical earthquakes correspond to the highest values of radon concentrations, in particular high radon values occur in the zones struck by earthquakes in 1835, 1854, and 1870.
