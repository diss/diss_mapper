﻿    **COMMENTS**
    
    This is the Source of the 23 June 1542 earthquake, based on geomorphologic observations and the geological previous studies. This Source is a N-dipping pure normal fault. This segment would be a part of the Etrurian Fault System proposed by Boncio et al (2000).
    
    The macroseismic data of the 1542 earthquake suggest a fault orientation quite different respected to the source we proposed, but we can reasonably suppose a important role of the site effects due to the shape of the basin itself and the high contrast between the sediments and the bedrock bellow.
    
    According to the tectonics and geological profiles (Benvenuti, 2003; GE.MI.NA, 1963; Sanesi, 1965) the geometry of the Plio-Quaternary sediment filling seems to indicate a synsedimentary normal faulting. The two episodes of sedimentation separated by a discontinuity are interpreted by Benvenuti and colleagues as the consequences of episodes of thrusting located some kilometres north-eastward. Another possibility could be an extensive tectonics with a movement on the Etrurian Fault System, which include the 1542 source. The several episodes of sedimentation and erosion could be explained by eustatic movement. 
    
    
    **OPEN QUESTIONS**
    
    1) Is there any surface faulting expression associated with the 1542 earthquake?
    
    2) Is the Mugello a piggy-back basin or is it due to the activity of a normal fault?
    
    3) Is the tectonics responsible of the formation of the basin yet the same?
    
    
    **SUMMARIES**
    
    GE.MI.NA. (1963)
    This paper presents a detailed study of the Mugello Basin planned to reconstruct the geometry of some lignite layers. The interpretation of the stratigraphic log of several boreholes, principally located along the Sieve River, has been used to draw a map of the isobaths of the top surface of the prelacustrine formation.
    
    Sanesi (1965)
    The author exposes some results regarding the geology and geomorphology of the Mugello basin.
    From Upper Pliocene, a lake was locate in the basin and produce at the maximum 600 m deep of sediments.
    Actually, according to the author, it is possible to recognize 5 levels of climatic and alluvial terraces. Sediments on the terraces show different weathering and different soils. The author uses these soils for the datation and the correlation of the different terraces and for climatic deductions. 
    
    Boncio et al. (2000)
    The authors principally describe a structural model for earthquake faulting in the Umbria-Marche Apennines by integrated analysis of geological, geophysical and seismological data to depict the geometry and the possibly seismogenic role of the Altotiberina Fault (AF), a low-angle normal fault in Central Italy. The AF belongs to a regional NE dipping low-angle en echelon normal fault system (Etrurian Fault System (EFS)) which extends for about 350 km from northwestern Tuscany (Lunigiana) to Southern Umbria (Terni). Some considerations make the authors to suggest that the EFS may play an important role in controlling active extension and related seismicity in northern central Italy.
    The link between the different EFS segments are not always clearly understood because of the lack of surface and subsurface data. In particular the link between the Mugello and the Garfagnana basin looks to be hard. The authors consider the presence of a E-W transfer zone, possibly reactivating a left-lateral crustal discontinuity inherited from the Miocene tectonics. The EFS appears also to control the seismicity at depth.
    
    Benvenuti (2003)
    This article is the latest one of a series regarding the sedimentology of the Mugello Basin. The author describes the Plio-Pleistocene alluvial and lacustrine deposit succession. He uses and explains the concept of "fan-delta" and "hyperconcentrated flow" corresponding to the two major episodes of the sedimentological history of the Basin. The lacustrine deposits are subdivided in two systems: the upper fan delta and the lower fan delta. They are separated by an angular unconformity. They show an alternation of alluvial fan, fan delta front, lacustrine and floodplain facies association. The fan deltas are of the type Gilbert and shoal water, indicating variable water depths at the lake margin. Each of the successions consists of fining-and coarsening-upward cyclothems of medium and small scale. The different scale cycles are interpreted by the author as the sedimentary response to pulses of a compressional deformation of the basin margin at variable frequency, related to the contemporary Northern Apennine active thrusts. The episodes of compression downwarped the basin margin alluvium, made the central lake shift periodically toward the margin, and created progressive unconformities in the sedimentary succession.
    According to the author, the immediate effects of a compressional pulse included lake transgression and accentuation of the structural hinge of the basin margin, causing a decline of sediment supply from the catchments. As the hinge relief was subsequently reduced by denudation, the alluvial fans prograded and fan deltas were formed in normal conditions of graben subsidence.
    
    Delle Donne et al. (2003)
    The authors present their results during the EGS annual meeting. They regard the active tectonics of the Mugello Basin. They individuated and mapped recent and active dip-slip normal faults, which cut lacustrine deposits, Pleistocene paleo-surfaces, and Middle/Late Pleistocene terrace deposits.
    According to the authors, these active faults can be subdivided into two sets. A WNW-ESE oriented group at the two opposite part of the Basin, close to the Ronta locality and close to Sieve River. The second set is oriented NE-SW and mainly present on the eastern part of the basin and interfered with the other set. Structural and seismological data collected suggest an active extensional regime at a regional scale with NE-SW oriented sigma3. The authors tried a first evaluation of maximum expected magnitude, calculated using empirical relations from the outcropping length of the various active faults individuated. The magnitude expected is about 6.2-6.4. Finally, they also compared tectonic data with geophysical analysis based on statistical processing of macroseismic information, which suggest the possible correlation between 1919 earthquake and a NE-SW oriented structure present in the south-eastern sector of the basin.
