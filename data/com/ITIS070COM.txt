﻿    **COMMENTS**
    
    We locate the source for the 1943 Offida earthquake on the thrust fault which generates the buried anticline immediately eastward M.gna dei Fiori structure. This fault plane is singled out in several seismic reflection profiles (Bally et al., 1986; Calamita et al., 1991; Scisciani et al., 2002).
    The thrust plane affects the whole Meso-Cenozoic succession, and its basal decollement is the Triassic evaporites level. On the basis of these considerations, we estimate that the minimum and maximum depths of the fault coincide with the top of the Marne con Cerrogna/Gessoso Solfifera formation (i.e. with the base of the sin-orogenic sequences) and with the top of Triassic evaporites respectively.
    
    The fault strike is chosen according with the orientation of the mapped buried anticline (Bigi et al., 1990). The fault dips toward the SW in agreement with the data from seismic reflection profiles. The rake is assumed to be pure thrusting based on general geodynamic consideration, and as the axis of the buried structure does not show important bending and lateral ramps.
    The structural style of the Offida area is characterised by the presence of different scale structures. In particular we identify different wave length anticlines controlled by the decollement plane depth. The deeper is the decollement plane the bigger are the associated anticlines. In particular the main faults started at the base of the Meso-Cenozoic succession or inside the shallower portion of the metamorphic basement and interest all the carbonates producing major wave length anticlines the secondary faults generates at the Marne a Fucoidi level, or at shallower levels into the Scaglia Variegata formation or into the Gessoso Solfifera formation.
    
    
    **OPEN QUESTIONS**
    
    1) Is the activity of this Source related to a present activity of the Apennine accretionary prism? If this is the case, is the activated thrust a first order structure detached at the base of the sedimentary cover and is its activity correlatable to other main thrust fault in the Northern Apennines?
    
    2) What are the relationships of this source with the sources in the coastal northern sector of the Marche region?
    
    3) Is the observed present instrumental seismicity in this area associated to the activity of the Offida fault?
    
    4) Are there geomorphic features related with the present activity of this thrust?
    And if they are, is it possible to use these elements to quantify the rate of the deformation and evaluate the recurrence interval of the main seismic shocks?
    
    
    **SUMMARIES**
    
    Calamita et al. (1991)
    They propose and discuss three balanced cross sections through the Umbria-Marche thrust system. They built and constraint these sections both by surface geological data and subsurface data from seismic reflection profiles and exploration wells. They assume a thin skin tectonic style and a blind thrusting kinematic model to model the structural setting at depth, the timing of the eastward thrust migration, the shortening rates and the slip rates. In particular they infer: a) shortening of km 52.5, km 55.1 and km 32.8 for the M. Utero/ Tortoreto Lido (Section A-A'), M. Faeto/Taccoli (B-B') and M. S. Vicino/Conero (C-C') sections respectively; b) structuring of the external portion of the thrust belt between Upper Messinian (6 Myr) and Upper Pliocene (2 Myr); c) activity of each thrust in the range of about 1 Myr; d) average slip rates of 15 mm/y for the section A-A' and of 8 mm/y for the section C-C'.
    
    Lavecchia et al. (2003)
    They analyse the spatial and kinematics relationships among the deep Apennine seismicity, the upper crust seismicity at the outer front of the Apennine thrust belt and the major crust-scale tectonic discontinuities. They indicate the presence of a Plio-Quaternary SW-dipping thrust zone, named Adriatic Thrust (AT), cutting the entire Adriatic crust and emerging at the outer front of the Apennine thrust system. On the basis of active tectonics and seismicity they divide the studied sector in three main zones, from west to west these zones are: Apennine zone, Foothill zone and Coastal-Adriatic zone.
    In order to verify any possible seismogenic link between the AT with the shallow and deep seismicity in the Central Apennines they plot well located seismological data of Porto S. Giorgio sequence (1987) and Faenza sequence (2000) along interpretative geological section. They found that the Porto S. Giorgio sequence is confined within the uppermost 6 km and it is associated with the shallow crust AT segment; the Faenza hypocenters are located beneath the Romagna foothills area and confined between 60 and 20 km depths within the AT hanging wall. In both cases main shock and after shock mechanisms show reverse solutions.
    They reach similar conclusions for the Reggio Emilia/Ferrara region struck by the 1983 Parma earthquake and the 1996 Reggio Emilia event.
    These earthquakes present reverse solutions with a strike slip component. The Reggio Emilia main shock (Mw = 5.4) is located at 15 km depth.
    In the investigator opinion the Po Plain/Adriatic front of the Apennine thrust system is still active, the shallow Coastal Adriatic seismicity and the deep Apennine seismicity are kinematically compatible with an active SW-NE compression and associated with the AT lithospheric discontinuity.
