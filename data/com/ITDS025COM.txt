﻿This seismogenic source was first proposed by Monaco and Tortorici (2000) based on geological and geomorphological evidence.
