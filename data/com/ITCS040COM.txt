**COMMENTS**
    
This Composite Source straddles along the backbone of the central Apennines, from the Assergi and Bazzano localities (to the north-west) to the higher Pescara R. valley (to the south-east). This Source is part of the SW-dipping, Abruzzo normal fault system, marking the eastern extensional border of the central Apennines.

Historical and instrumental catalogues (Boschi et al., 2000; Gruppo di Lavoro CPTI, 2004; Pondrelli et al., 2006; Guidoboni et al., 2007) show a sparse intermediate (4.5 < Mw < 5.0) to damaging seismicity within the area. Furthermore, this source has been hit by numerous, complex destructive earthquakes; from north to south they are: 26 November 1461 (Mw 6.5, Aquilano), 6 October 1762 (Mw 5.9, Aquilano), and 3 December 1315 (Mw 6.0, Italia centrale) events. To the southeast of this source, the 3 November 1706 (Mw 6.6, Maiella) earthquake has occurred.

This Source can be seen as the eastern portion of the whole SW-dipping extensional system of the central Apennines. The NW zone, in particular, has shown signs of tectonic activity soon after the destructive 6 April 2009 (Mw 6.3, L'Aquila) earthquake, both with Mw ca. 5 events and the ongoing sequence. To the south, the Mt. Morrone fault is thought to be responsible for the 1315 earthquake.

Some segments of this Source have been recognised based on their seismogenic potential. For an in-depth analysis of seismogenesis in this region, the reader can refer to the Individual Sources in this Database.

The strike of this Source was based on that of the mapped structures. The dip was based on geological geometrical considerations. The rake represents pure extension, based on geological observations. The minimum and maximum depth were based on geological data and geometrical considerations. The slip rate was inferred from geological observations in adjacent structures that share the same tectonic environment with this Source.
