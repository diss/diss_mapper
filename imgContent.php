<?php
if(isset($_GET['img']) && isset($_GET['sType'])){
	$idSource = $_GET['idSource'];
	$img = $_GET['img'];
	$sType = $_GET['sType'];

	//caption and titleImage
	$fileContent = file_get_contents("data/MainTable.txt");
	$lines = explode("\n", $fileContent);
	foreach ($lines as $line) {
		$fields = explode("\t", $line);
		if (count($fields) >= 3) {
			$code = $fields[0];
			if ($code == $img) {
				$titleField = $fields[1];
				$captionField = $fields[2];
			}
		}
	}

	$resultArray = [];

	//evaluate number of picture in folder
	// $cartella = "data/pict/";
	// if (is_dir($cartella)) {
	// 	$numFile = count(scandir($cartella)) - 2;
	// }
	// //evaluate next picture
	// $numero = intval(substr($img, 1, 5));
	// $numero++;
	// $numeroFormattato = sprintf('%05d', $numero);
	// $nextFileName = "P" . $numeroFormattato;

	// //evaluate previous picture
	// $numero = intval(substr($img, 1, 5));
	// $numero--;
	// $numeroFormattato = sprintf('%05d', $numero);
	// $previousFileName = "P" . $numeroFormattato;

	//get file content : LinkTable.txt
;	$percorsoFile = "data/LinkTable.txt";
	$contenuto = file_get_contents($percorsoFile);
	$righe = explode("\n", $contenuto);
	//find picture for idSource
	foreach ($righe as $riga) {
		$colonne = explode("\t", $riga);
		$column1 = trim($colonne[0]);
		$column2 = trim($colonne[1]);
		if ($column2 === $idSource) {
			array_push($resultArray, $column1);
		}
	}

	//check nextImage of same source type
	$finded = false;
	$pictureType = null;

	for($i=0; $i < count($resultArray); $i++){

		if($img === $resultArray[$i]){
			
			
			
			if (($i+1) < count($resultArray)) {
				$nextFileName = $resultArray[$i + 1];
			} else {
				$nextFileName = $resultArray[count($resultArray)-1];
			}

			if($i>0){
				$previousFileName = $resultArray[$i - 1];
			}else{
				$previousFileName = $resultArray[0];
			}

			break;

		}
		
	}

	echo "<div style=\"margin-bottom:5px;margin-top: 5px;\">";
	echo " <p style=\"width:30px;display:inline;margin-right:50px;\" onclick=\"previousImage('" . $idSource . "','" . $previousFileName . "', '" . $sType . "')\"> <button class=\"pointer\" style=\"box-shadow: 2px 2px #888888;\"> << </button></p>";
	echo " <p style=\"width:30px;display:inline;margin-right:50px;\" onclick=\"closeDiv('pictureContent')\"><button class=\"pointer\" style=\"box-shadow: 2px 2px #888888;\"> x </button></p>";
	echo " <p style=\"width:30px;display:inline;margin-right:50px;\" onclick=\"nextImage('" . $idSource . "','" . $nextFileName . "', '". $sType . "')\"> <button class=\"pointer\" style=\"box-shadow: 2px 2px #888888;\"> >> </button> </p>";
	echo "</div>";

	echo "<p style=\"width:30px;display:inline;\">". "Source : " . $idSource ."</p>";
	echo "<br>";
	
	echo "<p style=\"width:30px;display:inline;\">". $titleField ."</p>";
	echo "<img id='pictureImg' style=\"max-width:600px;\" src=\"data/pict/" . $img . ".jpg\">";
	echo "<div>";
	echo " <p style=\"width:30px;display:inline;\">" . $captionField . "</p>";
	echo "</div>";

	

}

?>
