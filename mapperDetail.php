<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <title>DISS - Data Tables </title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="icon" type="image/png" href="img/ingv.ico">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/s_js.js" charset="utf-8" />
    </script>
    <script type="text/javascript" src='js/tablesort.js'></script>

    <!--  codice per Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-421006-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-421006-2');
    </script>
</head>

<body>
    <div id="all">


        <div class="principale">
            <div class="TITLE" id="TITLE">
                <tr>
                    <td>
                        <a onclick="scrollToDiv('tab1')"><span>Information</span></a>
                    </td>
                    <td>
                        <a onclick="scrollToDiv('titleTab2')"><span>Commentary</span></a>
                    </td>
                    <td>
                        <a onclick="scrollToDiv('titleTab3')"><span>Pictures</span></a>
                    </td>

                    <td>
                        <a onclick="scrollToDiv('titleTab4')"><span>References</span></a>
                    </td>
                </tr>
            </div>
            <div class="TITLE_A" id="INFO_ICON">

            </div>
            <div class="mainDIV" id="mainDIV">
                <div id="ID_S"></div>
                <div id="tabcont1" class="tabpanel">

                    <div id="tab1" class="tab">

                        <table class="MAINTABLE">
                            <tbody>
                                <tr class="SEPARATOR" id="CONTAINERELEMENT_sep-labe">
                                    <td colspan="2" class="LABEL"><strong>General information</strong></td>
                                </tr>
                                <tr class="ODD" id="CONTAINERELEMENT_IDSource">
                                    <td scope="row" class="ODD">
                                        <strong><label for="VALUEELEMENT_IDSource">DISS-ID</label></strong>
                                    </td>
                                    <td id="VALUEELEMENT_IDSource" class="ODD">
                                    </td>
                                </tr>
                                <tr class="EVEN" id="CONTAINERELEMENT_SourceName">
                                    <td scope="row" class="EVEN">
                                        <strong><label for="VALUEELEMENT_SourceName">Name</label></strong>
                                    </td>

                                    <td id="VALUEELEMENT_SourceName" class="EVEN">
                                    </td>
                                </tr>
                                <tr class="ODD" id="CONTAINERELEMENT_compilers">
                                    <td scope="row" class="ODD">
                                        <strong><label for="VALUEELEMENT_compilers">Compiler(s)</label></strong>
                                    </td>

                                    <td id="VALUEELEMENT_compilers" class="ODD">
                                    </td>
                                </tr>
                                <tr class="EVEN" id="CONTAINERELEMENT_contributors">
                                    <td scope="row" class="EVEN">
                                        <strong><label for="VALUEELEMENT_contributors">Contributor(s)</label></strong>
                                    </td>

                                    <td id="VALUEELEMENT_contributors" class="EVEN">
                                    </td>
                                </tr>
                                <tr class="ODD" id="CONTAINERELEMENT_affiliations">
                                    <td scope="row" class="ODD">
                                        <strong><label for="VALUEELEMENT_affiliations">Affiliation(s)</label></strong>
                                    </td>

                                    <td id="VALUEELEMENT_affiliations" class="ODD">
                                    </td>
                                </tr>
                                <tr class="EVEN" id="CONTAINERELEMENT_created">
                                    <td scope="row" class="EVEN">
                                        <strong><label for="VALUEELEMENT_created">Created</label></strong>
                                    </td>

                                    <td id="VALUEELEMENT_created" class="EVEN">
                                    </td>
                                </tr>
                                <tr class="ODD" id="CONTAINERELEMENT_latestupdate">
                                    <td scope="row" class="ODD">
                                        <strong><label for="VALUEELEMENT_latestupdate">Updated</label></strong>
                                    </td>

                                    <td id="VALUEELEMENT_latestupdate" class="ODD">
                                    </td>
                                </tr>
                                <tr class="EVEN" id="CONTAINERELEMENT_latestupdate">
                                    <td scope="row" class="EVEN">
                                        <strong><label for="VALUEELEMENT_display">Display map</label></strong>
                                    </td>


                                    <td id="VALUEELEMENT_display" class="EVEN">
                                    </td>
                                </tr>


                                <?php
                                $pageUrl = $_SERVER['REQUEST_URI'];
                                $parsedUrl = parse_url($pageUrl, PHP_URL_QUERY);
                                $Type = substr($parsedUrl, 2, 2);
                                ?>



                                <?php
                                // $Type = substr($pageUrl, 31, 2);
                                // echo $Type;
                                include("html/" . $Type . ".html");
                                if ($Type == 'IS' || $Type == 'CS' || $Type == 'SD') {
                                    include("html/AFF.html");
                                }
                                ?>

                        </table>

                    </div>

                    <br>
                    <div id="titleTab2"><b>Commentary:</b></div>
                    <br>
                    <div id="tab2" class="tab"></div>
                    <br>
                    <div id="titleTab3"><b>Pictures:</b></div>
                    <div id="tab3" class="tab"></div>
                    <br>
                    <div id="titleTab4"><b>References:</b></div>
                    <div id="tab4" class="tab"></div>
                </div>


            </div>
        </div>

    </div>
</body>

</html>