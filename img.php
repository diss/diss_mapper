<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
	<title>DISS - Image </title>
	<link rel="icon" type="image/png" href="img/ingv.ico">
	<script type="text/javascript" src="js/p_js.js"></script>
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" HREF="css/imgstyle.css" TYPE="text/css">
	<link rel="stylesheet" HREF="css/style.css" TYPE="text/css">

	<!--  codice per Google Analytics -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-421006-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-421006-2');
	</script>

</head>

<body onload="pict_query()">

	<table class="MAIN" BORDER=0 CELLPADDING=0 CELLSPACING=0 style="heigth:100%">
		<?php include("html/header.html"); ?>



		<table class="MAINTABLE">

			<tr class="EVEN" id="CONTAINERELEMENT_Title">



				<td id="VALUEELEMENT_Title" class="TITOLO">-
				</td>

			</tr>
			<tr class="ODD" id="CONTAINERELEMENT_Image">


				<td id="VALUEELEMENT_Image" class="IMAGE">
					<div class="IMAGEDIV">--</div>
				</td>
			</tr>
			<tr class="EVEN" id="CONTAINERELEMENT_Caption">


				<td id="VALUEELEMENT_Caption" class="CAPTION">
					-

				</td>
			</tr>
		</table>


	</table>

</body>

</html>



