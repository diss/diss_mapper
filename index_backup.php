<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
	<title>DISS - Data Tables </title>
	<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
	<link rel="icon" type="image/png" href="img/ingv.ico">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/js.js" charset="utf-8"/></script>

	<!--  codice per Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-421006-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-421006-2');
</script>

</head>

<body onload = "test_type()">
	<div id="all">
		<?php include("html/header.html"); ?>
		<div class="right-column">

			<table BORDER=0 CELLPADDING=0 CELLSPACING=0 >
				<?php include("html/leftmenu.html"); ?>
			</table>
		</div>
	
		<div class="principale">
			<div class="TITLE" id="TITLE">

			</div>
			<div class="TITLE_A" id="INFO_ICON">
				
			</div>
			<div class="mainDIV" id="mainDIV">
				<table class="infoList">
					<tr>
						<td>
							Number of sources: <b class="num"></b>
						</td>
					</tr>
				</table>
				<p class="clear">&nbsp;</p>
				<div class = "TABCONT" id="TABCONT">
				</div>
			</div>
		</div>
	</div>	
</body>
</html>
